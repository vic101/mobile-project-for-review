import React, {useEffect, useState} from 'react';
import { AuthProvider } from './context/AuthProvider';
import Screens from "./navigation/Screens";
import { StripeProvider } from '@stripe/stripe-react-native';
import { fetchPublishableKey } from "./helper";
import { ModalPortal } from 'react-native-modals';
import AppStatusBar from './components/AppStatusBar';
import {SafeAreaProvider} from "react-native-safe-area-context/src/SafeAreaContext";
import Theme from "./constants/Theme";

import { Provider } from 'react-redux';
import store from './redux/store'

export default function App() {
    const [publishableKey, setPublishableKey] = useState('');

    const fetchKey = async () => {
        const key = await fetchPublishableKey()
        if (key) {
            setPublishableKey(key)
        }
    };

    useEffect(() => {
        fetchKey();
    }, []);

    return (
        <Provider store={store}>
            <SafeAreaProvider>
                <StripeProvider publishableKey={publishableKey}>
                    <AuthProvider>
                        <AppStatusBar backgroundColor={Theme.COLORS.THEME_COLOR}/>
                        <Screens />
                        <ModalPortal />
                    </AuthProvider>
                </StripeProvider>
            </SafeAreaProvider>
        </Provider>
    );
}
