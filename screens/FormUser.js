import React, {useContext, useEffect, useRef, useState} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    ActivityIndicator,
    Alert, ScrollView, Dimensions, SafeAreaView,
} from 'react-native';
import { AuthContext } from '../context/AuthProvider';
import { Images, argonTheme } from "../constants";
import axiosConfig from '../helpers/axiosConfig';
import {Block, theme} from "galio-framework";
import {Button, Input, Select} from "../components";
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MultiSelect from 'react-native-multiple-select';

export default function FormTeamLocation({ route, navigation }) {
    const [formData, setFormData] = useState(route.params.type == 'update'?route.params.item.name:'');

    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [languages, setLanguages] = useState('');
    const [selectedLanguage, setSelectedLanguage] = useState('');
    const [gender, setGender] = useState('');
    const [selectedGender, setSelectedGender] = useState('');

    const [locations, setLocations] = useState([]);
    const [selectedLocation, setSelectedLocation] = useState([]);
    const [teams, setTeams] = useState([]);
    const [selectedTeam, setSelectedTeam] = useState([]);

    const [type, setType] = useState('');
    const [word, setWord] = useState('');

    const [isLoading, setIsLoading] = useState(false);
    const { user } = useContext(AuthContext);
    const [errors, setError] = useState([]);
    const { width } = Dimensions.get("screen");

    const onSelectedTeamChange = (selectedTeam) => {
        // Set Selected Items
        setSelectedTeam(selectedTeam);
    };

    const onSelectedLocationChange = (selectedLocation) => {
        // Set Selected Items
        setSelectedLocation(selectedLocation);
    };

    useEffect(() => {
        setWord(route.params.action == 'update'?'Update':'Add');
        setType(route.params.type == 'employee'?'employee':'manager')

        if (route.params.action == 'update') {
            let data = route.params.item
            setFirstName(data.first_name)
            setLastName(data.last_name)
            setEmail(data.email)
            setSelectedGender(data.gender)
            setSelectedLanguage(data.language)


            if (route.params.type == 'employee') {
                const fetchedTeams     = data.team_employee.map(team => team.team_id)
                const fetchedLocations = data.location_employee.map(location => location.location_id)
                setSelectedTeam(fetchedTeams)
                setSelectedLocation(fetchedLocations)
            } else {
                const fetchedTeams     = data.team_manager.map(team => team.team_id)
                const fetchedLocations = data.location_manager.map(location => location.location_id)
                setSelectedTeam(fetchedTeams)
                setSelectedLocation(fetchedLocations)
            }
        }

        setTimeout(() => {
            setGender([
                {gender: 'Male'},
                {gender: 'Female'}
            ])
        }, 1000);
    },[]);

    useEffect(() => {
        fetchLanguages();
        fetchTeamLocation();
    },[])

    function fetchTeamLocation() {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attributes = {
            active: true
        }

        axiosConfig.get(`/team-location`, {params: attributes}).then(response => {
            let data = response.data;
            setLocations(data.locations)
            setTeams(data.teams)
        })
            .catch(error => {
                console.log(error);
            });
    }
    function fetchLanguages() {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attributes = {
            active: true
        }

        axiosConfig.get(`/api-ui-translations/languages`, {params: attributes}).then(response => {
            let data = response.data;
            setLanguages(data.languages)
        })
            .catch(error => {
                console.log(error);
            });
    }
    function sendForm() {
        // if (formData.length === 0) {
        //     Alert.alert('Please enter a ' + route.params.form + ' name');
        //     return;
        // }

        setIsLoading(true);
        let attribute = null
        let attributes = null
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        if (route.params.action == 'update') {
            attributes = {
                first_name: first_name,
                last_name: last_name,
                email: email,
                language: selectedLanguage,
                gender: selectedGender,
                locations: selectedLocation,
                teams: selectedTeam,
                type: type
            }
        } else {
            attributes = {
                first_name: first_name,
                last_name: last_name,
                email: email,
                language: selectedLanguage,
                gender: selectedGender,
                locations: selectedLocation,
                teams: selectedTeam,
                type: type
            }
        }

        if (route.params.action == 'update') {
            axiosConfig.patch(setUrl(route.params), attributes).then(response => {
                type == 'employee' ? (
                    navigation.navigate('Manage Employees', {
                        newDataAdded: response.data,
                        type: response.data.user.email_verified_at ? 'bottom' : 'pending'
                    })
                ) : (
                    navigation.navigate('Managers', {
                        newDataAdded: response.data,
                        type: response.data.user.email_verified_at ? 'bottom' : 'pending'
                    })
                )

                setIsLoading(false);
            })
                .catch(error => {
                    const key = Object.keys(error.response.data.errors)[0];
                    setError(error.response.data.errors[key][0]);
                    setIsLoading(false);
                });
        } else {
            axiosConfig.post(setUrl(route.params), attributes).then(response => {
                type == 'employee' ? (
                    navigation.navigate('Manage Employees', {
                        newDataAdded: response.data,
                        type: response.data?.user?.email_verified_at ? 'bottom' : 'pending'
                    })
                    ) : (
                    navigation.navigate('Managers', {
                        newDataAdded: response.data,
                        type: response.data?.user?.email_verified_at ? 'bottom' : 'pending'
                    })
                )

                setIsLoading(false);
            })
                .catch(error => {
                    console.log('errors: ', error.response.data.errors)

                    const key = Object.keys(error.response.data.errors)[0];
                    // setError(error.response.data.errors[key][0]);

                    setError(error.response.data.errors)

                    setIsLoading(false);

                    // console.log('errors: ', errors)
                });
        }
    }
    function setUrl(parameter) {
        if (parameter.action == 'update') {
            return `/${route.params.form}-api-post/${parameter.item.id}`
        } else {
            return `/${route.params.form}-api-post`
        }
    }

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block style={styles.container}>
                <View style={styles.createTeamButtonContainer}>
                    <Text style={styles.textGray}>
                        {word} {type}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {isLoading && (
                            <ActivityIndicator
                                size="small"
                                color="gray"
                                style={{ marginRight: 8 }}
                            />
                        )}
                        <TouchableOpacity
                            style={styles.createTeamButton}
                            onPress={() => sendForm()}
                            disabled={isLoading}
                        >
                            <Text style={styles.createTeamButtonText}>{word} {type}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <Block style={{ flexDirection: 'row'}}>
                    <Block style={{ flex: 6}}>
                        <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                            <Input error={errors.first_name ? true : false} onChangeText={setFirstName} value={first_name} right placeholder={'First name'} iconContent={<Block />} />
                            <Text style={{color: '#f5365c'}}>{errors.first_name}</Text>
                        </Block>
                    </Block>
                    <Block style={{ flex: 6}}>
                        <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                            <Input error={errors.last_name ? true : false} onChangeText={setLastName} value={last_name} right placeholder={'Last name'} iconContent={<Block />} />
                            <Text style={{color: '#f5365c'}}>{errors.last_name}</Text>
                        </Block>
                    </Block>
                </Block>

                <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                    <Input error={errors.email ? true : false} onChangeText={setEmail} value={email} right placeholder={'Email address'} iconContent={<Block />} />
                    <Text style={{color: '#f5365c'}}>{errors.email}</Text>
                </Block>

                {type=='manager'? (
                    <View style={styles.dropdownsRow}>
                        <SelectDropdown
                            data={languages}
                            onSelect={(selectedItem, index) => {
                                setSelectedLanguage(selectedItem.language)
                            }}
                            defaultButtonText={'Language'}
                            buttonTextAfterSelection={(selectedItem, index) => {
                                return selectedItem.name;
                            }}
                            rowTextForSelection={(item, index) => {
                                return item.name;
                            }}
                            buttonStyle={styles.dropdown1BtnStyle}
                            buttonTextStyle={styles.dropdown1BtnTxtStyle}
                            renderDropdownIcon={isOpened => {
                                return <FontAwesome name={isOpened ? 'sort-up' : 'sort-down'} color={'#8898aa'} size={20} />;
                            }}
                            dropdownIconPosition={'right'}
                            dropdownStyle={styles.dropdown1DropdownStyle}
                            rowStyle={styles.dropdown1RowStyle}
                            rowTextStyle={styles.dropdown1RowTxtStyle}
                        />
                    </View>
                ) : (
                        <View style={styles.dropdownsRow}>
                            <SelectDropdown
                                data={gender}
                                onSelect={(selectedItem, index) => {
                                    setSelectedGender(selectedItem.gender)
                                }}
                                defaultButtonText={'Gender'}
                                buttonTextAfterSelection={(selectedItem, index) => {
                                    return selectedItem.gender;
                                }}
                                rowTextForSelection={(item, index) => {
                                    return item.gender;
                                }}
                                buttonStyle={styles.dropdown1BtnStyle}
                                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                renderDropdownIcon={isOpened => {
                                    return <FontAwesome name={isOpened ? 'sort-up' : 'sort-down'} color={'#8898aa'} size={18} />;
                                }}
                                dropdownIconPosition={'right'}
                                dropdownStyle={styles.dropdown1DropdownStyle}
                                rowStyle={styles.dropdown1RowStyle}
                                rowTextStyle={styles.dropdown1RowTxtStyle}
                            />

                            <Block style={styles.divider} />

                            <SelectDropdown
                                data={languages}
                                onSelect={(selectedItem, index) => {
                                    setSelectedLanguage(selectedItem.language)
                                }}
                                defaultButtonText={'Language'}
                                buttonTextAfterSelection={(selectedItem, index) => {
                                    return selectedItem.name;
                                }}
                                rowTextForSelection={(item, index) => {
                                    return item.name;
                                }}
                                buttonStyle={styles.dropdown1BtnStyle}
                                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                renderDropdownIcon={isOpened => {
                                    return <FontAwesome name={isOpened ? 'sort-up' : 'sort-down'} color={'#8898aa'} size={18} />;
                                }}
                                dropdownIconPosition={'right'}
                                dropdownStyle={styles.dropdown1DropdownStyle}
                                rowStyle={styles.dropdown1RowStyle}
                                rowTextStyle={styles.dropdown1RowTxtStyle}
                            />
                        </View>
                    )
                }

                {type=='manager' ? (
                        <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                            <Text>Access to</Text>
                        </Block>
                ):(<Text></Text>)}

                <Block style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: "5%" }}>
                    <MultiSelect
                        hideTags
                        items={teams}
                        uniqueKey="sid"
                        onSelectedItemsChange={onSelectedTeamChange}
                        selectedItems={selectedTeam}
                        selectText=" all teams"
                        searchInputPlaceholderText="Search Items..."
                        // onChangeInput={ (text)=> console.log(text)}
                        tagRemoveIconColor="#CCC"
                        tagBorderColor="#CCC"
                        tagTextColor="#CCC"
                        selectedItemTextColor="#CCC"
                        selectedItemIconColor="#CCC"
                        itemTextColor="#000"
                        displayKey="name"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#32325d"
                        submitButtonText="Select"
                    />
                </Block>

                <Block style={{ paddingHorizontal: theme.SIZES.BASE, marginTop: '5%' }}>
                    <MultiSelect
                        hideTags
                        items={locations}
                        uniqueKey="sid"
                        onSelectedItemsChange={onSelectedLocationChange}
                        selectedItems={selectedLocation}
                        selectText=" all locations"
                        searchInputPlaceholderText="Search Items..."
                        // onChangeInput={ (text)=> console.log(text)}
                        tagRemoveIconColor="#CCC"
                        tagBorderColor="#CCC"
                        tagTextColor="#CCC"
                        selectedItemTextColor="#CCC"
                        selectedItemIconColor="#CCC"
                        itemTextColor="#000"
                        displayKey="name"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#32325d"
                        submitButtonText="Select"
                    />
                </Block>
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    dropdownsRowLanguage: {flexDirection: 'row', width: '50%', paddingHorizontal: '5%', paddingTop: '2%', paddingBottom: '2%'},
    dropdownsRowGender: {flexDirection: 'row', width: '50%', paddingHorizontal: '5%', paddingTop: '2%', paddingBottom: '2%'},
    dropdownsRow: {flexDirection: 'row', width: '100%', paddingHorizontal: '4%', paddingTop: '2%', paddingBottom: '2%'},
    dropdown2BtnStyle: {
        flex: 1,
        height: 40,
        backgroundColor: '#FFF',
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#444',
    },
    dropdown2BtnTxtStyle: {color: '#444', textAlign: 'left'},
    dropdown2DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown2RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown2RowTxtStyle: {color: '#444', textAlign: 'left'},

    divider: {width: 30},

    dropdown1BtnStyle: {
        flex: 1,
        height: 45,
        backgroundColor: '#FFF',
        // borderRadius: 8,
        // borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 14},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left'},

    optionsButton: {
        width: "auto",
        height: 34,
        paddingHorizontal: theme.SIZES.BASE,
        paddingVertical: 10
    },
    textGray: {
        color: 'gray',
    },
    textRed: {
        color: 'red',
    },
    ml4: {
        marginLeft: 16,
    },
    container: {
        // flex: 1,
        backgroundColor: argonTheme.COLORS.BACKGROUND,
        // paddingVertical: 12,
        // paddingHorizontal: 10,
    },
    createTeamButtonContainer: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    createTeamButton: {
        backgroundColor: '#32325d',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 24,
    },
    createTeamButtonText: {
        color: 'white',
        fontWeight: 'bold',
    },
    tweetBoxContainer: {
        flexDirection: 'row',
        paddingTop: 10,
    },
    avatar: {
        width: 42,
        height: 42,
        marginRight: 8,
        marginTop: 10,
        borderRadius: 21,
    },
    input: {
        flex: 1,
        fontSize: 18,
        lineHeight: 28,
        padding: 10,
    },
});
