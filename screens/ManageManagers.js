import React, {useContext, useEffect, useRef, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Platform, ActivityIndicator, Alert, TouchableOpacity, RefreshControl
} from "react-native";
import { Card } from '../components';
import {Block, Text, theme} from "galio-framework";

import { Button, Icon } from "../components";
import { Images, argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import Ionicons from '@expo/vector-icons/Ionicons';
import ManageManageEmployeesLinks from "../components/links/ManageEmployeesLinks";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import {Menu, MenuItem} from "react-native-material-menu";
import RenderHtml from 'react-native-render-html';
import SelectDropdown from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";
import {fetchEmployees} from "../components/manage/Employees";
import { sort } from "../components/manage/sort"
import options from "../components/manage/sortOptions"

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function ManageManagers({route, navigation}) {
    const modalizeRef = useRef(null);
    const [isLoading, setIsLoading] = useState(true);
    const [loadVerified, setLoadVerified] = useState(true);
    const [pending, setPending]     = useState([]);
    const [verified, setVerified]   = useState([]);
    const [page, setPage]           = useState(1);
    const flatListRef               = useRef();
    const [isRefreshing, setIsRefreshing] = useState(false);
    const { user }                        = useContext(AuthContext);
    const [isAtEndOfScrolling, setIsAtEndOfScrolling] = useState(false);

    const [activeIndex, setActiveIndex] = useState(undefined);
    const [activeVerifiedIndex, setActiveVerifiedIndex] = useState(undefined);

    const [pendingShowHideIndex, setPendingShowHideIndex] = useState(undefined);
    const [verifiedShowHideIndex, setVerifiedShowHideIndex] = useState(undefined);
    const [refreshing, setRefreshing] = React.useState(false);

    const filter = (sortType, fetchType) => {
        sort(sortType, fetchType, fetchPendingVerifiedEmployees)
    }
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);

        wait(2000).then(
            () => {
                setRefreshing(false)
                fetchAllList()
            }
        );
    }, []);

    useEffect(() => {
        fetchAllList()
    }, [page]);
    useEffect(() => {
        if (route.params?.newDataAdded) {
            fetchPendingVerifiedEmployees(null,null, route.params?.type)
        }
    }, [route.params?.newDataAdded]);

    const fetchPendingVerifiedEmployees = async (type, order, fetchType) => {
        const data = await fetchEmployees(
            user,
            'manager',
            fetchType,
            type??'first_name',
            order??'desc',
            10,
            'teamManager',
            'locationManager'
        )

        if (data) {
            if (fetchType == 'pending') {
                setPending(data);
                setIsLoading(false);
            } else {
                setVerified(data);
                setLoadVerified(false);
            }
        }
    };
    function gotoNewManager(item, type, action) {
        navigation.navigate('FormUser', {form: 'user', item: item, type: type, action: action});
    }

    function handleRefresh(state) {
        setPage(1);
        setIsAtEndOfScrolling(false);
        setIsRefreshing(true);

        fetchPendingVerifiedEmployees(null,null, state)
    }
    function fetchAllList() {
        fetchPendingVerifiedEmployees(null,null, 'pending')
        fetchPendingVerifiedEmployees(null,null, 'bottom')
    }
    function deleteManager(id, state) {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        axiosConfig.delete(`/user-api/${id}`).then(response => {
            Alert.alert('Manager was deleted.');
            handleRefresh(state)
        })
            .catch(error => {
                console.log(error.response);
            });
    }
    function showAlert(id, state) {
        Alert.alert('Delete this manager?', null, [
            {
                text: 'Cancel',
                onPress: () => modalizeRef.current?.close(),
                style: 'cancel',
            },
            {
                text: 'OK',
                onPress: () => deleteManager(id, state),
                style: 'default',
            },
        ]);

        state == 'pending' ? setActiveIndex(undefined) : setActiveVerifiedIndex(undefined);
    }

    const renderAssignedLink = (manager, index, showPending) => {
        const source = {html: `<span style="color: gray;">Assigned to: </span>`+ manager.assigned};
        return (
            <TouchableOpacity
                onPress={() => showHideList(index, showPending)}
            >
                <RenderHtml contentWidth={width} source={source}/>
            </TouchableOpacity>
        )
    }
    const showHideList = (index, showPending) => {
        showPending ? (
            pendingShowHideIndex !== index ? setPendingShowHideIndex(index) : setPendingShowHideIndex(undefined)
        ) : (
            verifiedShowHideIndex !== index ? setVerifiedShowHideIndex(index) : setVerifiedShowHideIndex(undefined)
        )
    }
    function renderAssignments(manager, index, showPending) {
        return (
            <Block>
                <Text onPress={()=>showHideList(index, showPending)} size={12} muted style={{textDecorationLine: 'underline'}}>Hide list</Text>
                {manager.team_manager.map((teamManager, index) => {
                    return (
                        <Block key={index}>
                            <Text size={13} style={{ color: '#32325d'}}>
                                {teamManager.team?.name}
                                <Text muted> in </Text>
                                <Text>{manager.locations}</Text>
                            </Text>
                        </Block>
                    )
                })
                }
                <Text onPress={()=>showHideList(index, showPending)} size={12} muted style={{textDecorationLine: 'underline'}}>Hide list</Text>
            </Block>
        )
    }

    function renderPendingManagers() {
      return (
          <Block flex>
              {isLoading ? (
                  <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
              ) : (
                  <Block flex style={styles.listContainer}>
                      <Block style={{flexDirection: 'row'}}>
                          <Block style={{ flex: 10}}>
                              <Text muted style={{ fontWeight: 'bold', marginBottom: 5 }} >{pending?.user?.total} pending invites</Text>
                          </Block>
                          <Block style={{ flex: 20}}>
                              { renderDropdown('pending') }
                          </Block>
                      </Block>

                      <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                          <Block style={styles.divider} />
                      </Block>
                      {
                          pending?.user?.data.map((manager, index) => {
                              return (
                                  <Block key={index}>
                                      <Block style={{ flexDirection: 'row' }}>
                                          <Block style={{ flex: 12, marginRight: 2 }}>
                                              <Text size={13}>
                                                  {
                                                      manager.first_name + ' '
                                                  }
                                                  <Text style={{ fontWeight: 'bold' }}>
                                                      { manager.last_name + ' ' }
                                                      <Text muted>{manager.email}</Text>
                                                  </Text>
                                              </Text>

                                              {!isLoading && renderAssignedLink(manager, index, true) }
                                              {pendingShowHideIndex == index && renderAssignments(manager, index, true) }
                                          </Block>
                                          <Block>
                                              <Ionicons
                                                  onPress={() => activeIndex !== index ? setActiveIndex(index) : setActiveIndex(undefined)}
                                                  name="ellipsis-vertical" size={14} color="#8898aa"
                                              />
                                              <Menu
                                                  visible={activeIndex === index} onRequestClose={() => setActiveIndex(undefined)}
                                                  row middle space="between"
                                              >
                                                  <MenuItem onPress={()=> gotoNewManager(item, 'manager', 'update')}>Edit</MenuItem>
                                                  <MenuItem onPress={()=> showAlert(item.id, 'pending')}>Delete</MenuItem>
                                              </Menu>
                                          </Block>
                                      </Block>

                                      <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                          <Block style={styles.divider} />
                                      </Block>
                                  </Block>
                              )
                          })
                      }
                  </Block>
              )
              }
          </Block>
      )
  }

    function renderDropdown (fetchType) {
        return (
            <Block style={styles.dropdownsRow}>
                <SelectDropdown
                    data={options}
                    onSelect={(selectedItem, index) => {
                        filter(selectedItem.key, fetchType)
                    }}
                    defaultButtonText={options[0].name}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                        return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                        return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                    search
                    searchInputStyle={styles.dropdown1searchInputStyleStyle}
                    searchPlaceHolder={'Search here'}
                    searchPlaceHolderColor={'darkgrey'}
                    renderSearchInputLeftIcon={() => {
                        return <FontAwesome name={'search'} color={'#444'} size={18} />;
                    }}
                />
                <Block style={styles.dividerDropdown} />
            </Block>
        )
    }
    function renderVerifiedManagers() {
        return (
            <Block flex>
                {loadVerified ? (
                    <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                ) : (
                    <Block flex style={styles.listContainer}>
                        <Block style={{flexDirection: 'row'}}>
                            <Block style={{ flex: 10}}>
                                <Text muted style={{ fontWeight: 'bold', marginBottom: 5 }} >{verified?.user?.total} invites</Text>
                            </Block>
                            <Block style={{ flex: 20}}>
                                { renderDropdown('bottom') }
                            </Block>
                        </Block>

                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>
                        {
                            verified?.user?.data.map((manager, index) => {
                                return (
                                    <Block key={index}>
                                        <Block style={{ flexDirection: 'row' }}>
                                            <Block style={{ flex: 12, marginRight: 2 }}>
                                                <Text size={13}>
                                                    {
                                                        manager.first_name + ' '
                                                    }
                                                    <Text style={{ fontWeight: 'bold' }}>
                                                        { manager.last_name + ' ' }
                                                        <Text muted>{manager.email}</Text>
                                                    </Text>
                                                </Text>

                                                {!loadVerified && renderAssignedLink(manager, index, false) }
                                                {verifiedShowHideIndex == index && renderAssignments(manager, index, false) }

                                                <Text muted size={13}>Last login: {manager.last_login}</Text>
                                            </Block>
                                            <Block>
                                                <Ionicons
                                                    onPress={() => activeVerifiedIndex !== index ? setActiveVerifiedIndex(index) : setActiveVerifiedIndex(undefined)}
                                                    name="ellipsis-vertical" size={14} color="#8898aa"
                                                />
                                                <Menu
                                                    visible={activeVerifiedIndex === index} onRequestClose={() => setActiveVerifiedIndex(undefined)}
                                                    row middle space="between"
                                                >
                                                    <MenuItem onPress={()=> gotoNewManager(item, 'manager', 'update')}>Edit</MenuItem>
                                                    <MenuItem onPress={()=> showAlert(item.id, 'verified')}>Delete</MenuItem>
                                                </Menu>
                                            </Block>
                                        </Block>

                                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                            <Block style={styles.divider} />
                                        </Block>
                                    </Block>
                                )
                            })
                        }

                    </Block>
                )
                }
            </Block>
        )
    }
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                }
            >
                <Block flex style={styles.descriptionsContainer}>
                    <Block flex>
                        <Block style={styles.details}>
                            <ManageManageEmployeesLinks
                                route={route}
                                title={route.name}
                            />
                        </Block>
                    </Block>
                </Block>
                <Block flex style={styles.descriptionsContainer}>
                    <Block flex>
                        <Block style={styles.details}>
                            <Text bold size={13} color="#36d79a" style={{ marginBottom: 10}}>
                                About employees
                            </Text>
                            <Text size={13} color="#32325d" style={styles.aboutText}>
                                Invite your employees to let them participate in your regular surveys, and send you valuable feedback.
                            </Text>

                            <Text size={13} color="#32325d" style={styles.aboutText}>
                                This way, you can increase their happiness, engagement, and loyalty towards your organization.
                            </Text>

                            <Text size={13} muted>
                                By the way, you never pay for pending invites.
                            </Text>
                        </Block>
                    </Block>
                </Block>

                <Block>
                    <Button onPress={()=>gotoNewManager(null,'create')} color="default" style={styles.button}>
                        Add manager
                    </Button>
                </Block>

                { renderPendingManagers() }
                { renderVerifiedManagers() }
            </ScrollView>
        </Block>
    );
}

const styles = StyleSheet.create({
    dropdownsRow: {
        flexDirection: 'row',
        width: '60%',
        alignSelf: 'flex-end',
        marginRight: -5
    },
    dropdown1BtnStyle: {
        flex: 1,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 4,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 12},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 12},
    dividerDropdown: {width: 12},
    button: {
        width: width - theme.SIZES.BASE * 16,
        marginLeft: 16,
        marginTop: 16
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: width,
        height: height,
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    descriptionsContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    details: {
        marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    aboutText: {
        lineHeight: 20,
        marginBottom: 15
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});
