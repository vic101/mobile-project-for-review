import React from "react";
import Details from "../components/messages/Details";

export default function messages({ navigation, route }) {
    return (
        <Details
            navigation={navigation}
            route={route}
        />
    )
}
