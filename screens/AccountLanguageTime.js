import React, {useContext, useEffect, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Platform, View, ActivityIndicator
} from "react-native";

import { Block, Text, theme, Radio } from "galio-framework";
import { argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import AccountLinks from "../components/links/AccountLinks";
import {Select} from "../components";
import {AuthContext} from "../context/AuthProvider";
import axiosConfig from "../helpers/axiosConfig";
import {func} from "prop-types";
import SelectDropdownMenu from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";
import SelectDropdown from "react-native-select-dropdown";
import LanguageTime from "../components/accounts/LanguageTime";
const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function Account ({route, navigation}) {
    const { user } = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(true);
    const [isLoadingManager, setIsLoadingManager] = useState(true);
    const [isLoadingEmployee, setIsLoadingEmployee] = useState(true);

    const [isFetchingLanguage, setFetchLanguage] = useState(true);

    const [managerLanguage, setManagerLanguage] = useState(true);
    const [employeeLanguage, setEmployeeLanguage] = useState(true);
    const [employee, setEmployee] = useState(false);
    const [allEmployee, setAllEmployee] = useState(false);
    const [timezones, setTimezones] = useState([]);
    const [selectedTimezone, setSelectedTimezone] = useState('');
    const [timezone, setTimezone] = useState('');
    const [currentTime, setCurrentTime] = useState(true);
    const [lang, setLang] = useState(true);
    const [languages, setLanguages] = useState([]);
    const [language, setLanguage] = useState('');
    const [error, setError] = useState(null);
    const [selectedLanguage, setSelectedLanguage] = useState('');


    const [manager, setManager] = useState(false);
    const [managerAll, setManagerAll] = useState(false);

    const [timeFormat, setTimeFormat] = useState('');
    const [twentyFour, setTwentyFour] = useState(false);
    const [twelve, setTwelve] = useState(false);
    const [langId, setLangId] = useState(false);

    useEffect(() => {
        getSettings()
        getLanguages()
        getTimezones()
    }, [langId]);
    function select(status) {
        setIsLoading(true);

        switch (status) {
            case 'manager':
                saveManagerLanguage(true)
                alert('Successfully changed.')
                break;

            case 'managerAll':
                saveManagerLanguage(false)
                // saveAllManagerLanguage()
                alert('Successfully changed.')
                break;

            case 'employee':
                // setEmployee(true)
                // setAllEmployee(false)
                saveEmployeeLanguage(true)
                //saveAllEmployeeLanguage()
                alert('Successfully changed.')
                break;

            case 'allEmployee':
                // setEmployee(false)
                // setAllEmployee(true)
                saveEmployeeLanguage(false)
                alert('Successfully changed.')
                break;

            case 'twentyFour':
                setTimeFormat('twentyFour')
                setTwentyFour(true)
                setTwelve(false)
                break;

            case 'twelve':
                setTimeFormat('twelve')
                setTwentyFour(false)
                setTwelve(true)
                break;
        }

        setIsLoading(false);
    }
    function changeTimeFormat(format) {
        setIsLoading(true)
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'time_format',
            value: format
        }
        console.log('attribute: ', attribute)
        axiosConfig.post('/account-api', attribute).then(response => {
            setTimeFormat(response.data.account?.time_setting?.time_format ?? timeFormat)
            setIsLoading(false)

            alert('Successfully changed.')
        }).catch(error => {
            console.log(error.response.data);
            setIsLoading(false);
        });
    }
    function saveAllEmployeeLanguage(showNotif = false) {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'set_language_for_all_employee',
            value: employeeLanguage
        }

        axiosConfig.post('/api/account', attribute).then(response => {
            setEmployeeLanguage(response.data.account?.set_language_for_all_employee?.language ?? employeeLanguage)
        })

        // if (showNotif) {
        //     this.notify('Successfully changed.')
        // }
    }
    function saveEmployeeLanguage(isEmployee) {
        setIsLoadingEmployee(true)
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'employee_can_set_own_language',
            value: isEmployee ? true:false
        }

        axiosConfig.post('/account-api', attribute).then(response => {
            let isEmployee = response.data.account.employee_can_set_own_language == 0 ? false: true;
            setEmployee(isEmployee)
            setAllEmployee(!isEmployee)

            setIsLoadingEmployee(false)
        }).catch(error => {
            console.log(error.response.data);
        });
    }
    function saveManagerLanguage(managerCanSetOwnLanguage) {
        setIsLoadingManager(true)
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'manager_can_set_own_language',
            value: managerCanSetOwnLanguage ? true:false
        }

        axiosConfig.post('/account-api', attribute).then(response => {
            const isManager = response.data.account.manager_can_set_own_language == 0 ? false:true
            setManager(isManager)
            setManagerAll(!isManager)

            setIsLoadingManager(false)
        }).catch(error => {
            console.log(error.response.data);
        });
    }
    function saveAllManagerLanguage(showNotif = false) {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'set_language_for_all_manager',
            value: managerLanguage
        }

        axiosConfig.post('/account-api', attribute).then(response => {
            setManagerLanguage(response.data.account?.set_language_for_all_manager?.language ?? managerLanguage)
        }).catch(error => {
            console.log(error.response.data);
        });
    }
    function getSettings() {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attributes = {
            setting: 'language_time'
        }

        axiosConfig.get(`/account/get-settings-api?`, {params: attributes }).then(function (response) {
            let isManager  = response.data.account?.manager_can_set_own_language == 0 ? false:true;
            setManager(isManager)
            setManagerAll(!isManager)

            let isEmployee = response.data.account?.employee_can_set_own_language == 0 ? false:true;
            setEmployee(isEmployee)
            setAllEmployee(!isEmployee)


            // this.managerLanguage  = response.data.account?.set_language_for_all_manager?.language ?? this.managerLanguage
            setManagerLanguage(response.data.account?.set_language_for_all_manager?.language ?? managerLanguage)

            // this.employeeLanguage = response.data.account?.set_language_for_all_employee?.language ?? this.employeeLanguage
            setEmployeeLanguage(response.data.account?.set_language_for_all_employee?.language ?? employeeLanguage)
            // this.employee         = response.data.account?.employee_can_set_own_language

            let defaultTimeZone = response.data.account?.time_setting?.timezone ?? timezone
            setTimezone(defaultTimeZone)

            // this.timeFormat       = response.data.account?.time_setting?.time_format ?? this.timeFormat
            setTimeFormat(response.data.account?.time_setting?.time_format ?? timeFormat)

            // this.current_time     = response.data?.current_time
            setCurrentTime(response.data?.current_time)

            setLangId(response.data.account?.language_setting);

            setIsLoading(false);
            setIsLoadingEmployee(false)
            setIsLoadingManager(false)
            console.log('done fetched settings...')
        }).catch(error => {
            console.log(error);
            setIsLoading(false);
        });
    }
    function changeTimezone(selected) {
        setIsLoading(true)
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'timezone',
            value: selected
        }

        axiosConfig.post('/account-api', attribute).then(response => {
            setTimezone(response.data.account?.time_setting?.timezone ?? timezone)
            setCurrentTime(response.data.account.current_time)

            setIsLoading(false)
            alert('Successfully changed.')
        }).catch(error => {
            console.log(error.response.data);
        });
    }
    function getLanguages() {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attributes = {
            active: true
        }

        axiosConfig.get(`/api-ui-translations/languages`, {params: attributes}).then(response => {
            let data = response.data;
            setLanguages(data.languages)

            const setLang = response.data.languages.find(lang => lang.id === langId)
            setLanguage(setLang.name);
        })
            .catch(error => {
                console.log(error);
            });
    }
    function getTimezones() {
        axiosConfig.get(`/timezones-api`).then(response => {
            setTimezones(response.data.timezones)
        }).catch(error => {
            console.log('error: ', error.response.data)
            // const key = Object.keys(error.response.data.errors)[0];
            // setError(error.response.data.errors[key][0]);
            setIsLoading(false);
        });
    }
    function renderAccount () {
        return (
            <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Block flex style={styles.accountContainer}>
                        <Block flex>
                            <Block style={styles.accountDetails}>
                                <AccountLinks title={route.name} role={user.role}/>
                            </Block>
                        </Block>
                    </Block>

                    <Block flex style={styles.listContainer}>
                        <Block>
                            <Text bold size={13} color="#32325D" style={styles.text}>
                                Language settings
                            </Text>
                            <Text size={13}>
                                Show surveys and vibestrive's user interface in
                            </Text>
                            {isLoading ? (
                                <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                            ):(
                                <Block flex left style={{marginTop: 8}}>
                                    <LanguageTime
                                        which={'language'}
                                        languages={languages}
                                        language={language}
                                    />
                                </Block>
                            )}

                            <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>

                            {/*Admin*/}
                            {user.role === 'admin' &&
                                <Block>
                                    <Block style={{ marginTop: 5}}>
                                        {isLoadingManager ? (
                                            <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                                        ):(
                                            <Radio label="Managers can set their own language" color="default" onChange={()=>select('manager')} initialValue={manager} radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                        )}
                                    </Block>
                                    <Block style={{ marginTop: 10}}>
                                        {isLoadingManager ? (
                                            <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                                        ):(
                                            <Radio label="Set language for all managers to" color="default" onChange={()=>select('managerAll')} initialValue={managerAll} radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                        )}
                                    </Block>
                                    <Block flex left style={{marginTop: 8}}>
                                        <LanguageTime
                                            which={'language'}
                                            languages={languages}
                                            language={language}
                                        />
                                    </Block>
                                    <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                        <Block style={styles.divider} />
                                    </Block>
                                    <Block>
                                        {(isLoadingEmployee) ? (
                                            <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                                        ) : (
                                            <Block style={{ marginTop: 5}}>
                                                <Radio label="Employees can set their own language" color="default" onChange={()=>select('employee')} initialValue={employee} radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                            </Block>
                                        )}
                                    </Block>
                                    <Block>
                                        {isLoadingEmployee ? (
                                            <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                                        ) : (
                                            <Block style={{ marginTop: 10}}>
                                                <Radio label="Set language for all employees to" color="default" onChange={()=>select('allEmployee')} initialValue={allEmployee} radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                            </Block>
                                        )}
                                    </Block>
                                    <Block flex left style={{marginTop: 8}}>
                                        <LanguageTime
                                            which={'language'}
                                            languages={languages}
                                            language={language}
                                        />
                                    </Block>

                                    <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                        <Block style={styles.divider} />
                                    </Block>
                                </Block>
                            }
                            {/*End admin*/}

                            <Text bold size={13} color="#32325D" style={styles.text}>
                                Time settings
                            </Text>
                            <LanguageTime
                                which={'timezone'}
                                timezones={timezones}
                                timezone={timezone}
                            />
                            <Block style={{ marginTop: 5}}>
                                {isLoading ? (
                                    <ActivityIndicator
                                        size="small"
                                        color="gray"
                                        style={{ marginRight: 8 }}
                                    />
                                ) : (
                                    <Radio label={'24 hour format '+ currentTime.twenty_four}
                                           labelStyle={ timeFormat === 'twentyFour' ? styles.bold : '' }
                                           color="default"
                                           initialValue={timeFormat === 'twentyFour' ? true:false}
                                           radioInnerStyle={{ backgroundColor: "#32325d" }}
                                           onChange={()=>changeTimeFormat('twentyFour')}
                                    />
                                )}
                            </Block>
                            <Block style={{ marginTop: 10}}>
                                {isLoading ? (
                                    <ActivityIndicator
                                        size="small"
                                        color="gray"
                                        style={{ marginRight: 8 }}
                                    />
                                ) : (
                                    <Radio label={'12 hour format '+ currentTime.twelve}
                                           labelStyle={ timeFormat === 'twelve' ? styles.bold : '' }
                                           color="default"
                                           radioInnerStyle={{ backgroundColor: "#32325d" }}
                                           initialValue={timeFormat === 'twelve' ? true:false}
                                           onChange={()=>changeTimeFormat('twelve')}
                                    />
                                )}
                            </Block>
                        </Block>
                    </Block>
                </ScrollView>
            </Block>
        )
    }

    return (
        <Block flex>
            <Block flex>
                { renderAccount() }
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    bold: {
        fontWeight: 'bold'
    },
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    accountContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    accountDetails: {
        // marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    divider: {
        width: "100%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    }
});
