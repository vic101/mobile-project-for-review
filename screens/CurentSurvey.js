import React, {useContext, useEffect, useRef, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView, Alert, TouchableOpacity, ActivityIndicator, RefreshControl,
} from "react-native";
import { Block, Text, theme } from "galio-framework";
import Spinner from 'react-native-loading-spinner-overlay';

import { argonTheme } from "../constants";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";

import TrialBanner from "../components/TrialBanner";
import SurveyDetails from "../components/surveys/CurrentPast/SurveyDetails";
import BarGraph from "../components/surveys/CurrentPast/BarGraph";
import ResponseFilter from "../components/surveys/CurrentPast/ResponseFilter";
import Responses from "../components/surveys/CurrentPast/Responses";
import ResponseActions from "../components/surveys/CurrentPast/ResponseActions";
import CustomResponseActions from "../components/surveys/CurrentPast/CustomResponseActions";
import OutsideView from 'react-native-detect-press-outside';
import CheckBox from "@react-native-community/checkbox";
import SelectDropdown from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function CurrentSurvey({ route, navigation}) {
    const childRef = useRef();

    const { user } = useContext(AuthContext);
    const [loading, setLoading] = useState(true)
    const [loadCustom, setLoadCustom] = useState(false)
    const [loadResponse, setLoadResponse] = useState(false)
    const [selectedLocation, setSelectedLocation] = useState(null)
    const [selectedTeam, setSelectedTeam] = useState(null)
    const [selectedCategory, setSelectedCategory] = useState(null)
    const [locations, setLocations] = useState([])
    const [teams, setTeams] = useState([])
    const [firstSurvey, setFirstSurvey] = useState('')

    const [questions, setQuestions] = useState([])
    const [customQuestions, setCustomQuestions] = useState([])
    const [userAnswerCustoms, setUserAnswerCustoms] = useState([])
    const [customAnswersCount, setCustomAnswersCount] = useState('')

    const [answersStatusCount, setAnswersStatusCount] = useState([])
    const [allAnswerStatusCount, setAllAnswerStatusCount] = useState([])
    const [messageCount, setMessageCount] = useState('')
    const [answerPercentage, setAnswerPercentage] = useState('')
    const [graphPercentage, setGraphPercentage] = useState('')
    const [categories, setCategories] = useState([])
    const [surveyCompleted, setSurveyCompleted] = useState('')
    const [surveyDetails, setSurveyDetails] = useState([])
    const [surveyTotal, setSurveyTotal] = useState('')
    const [surveyStatus, setSurveyStatus] = useState('')

    const [showNew, setShowNew] = useState(true);
    const [showReplied, setShowReplied] = useState(true);
    const [showResolved, setShowResolved] = useState(true);

    const [showNewCustom, setShowNewCustom] = useState(true);
    const [showRepliedCustom, setShowRepliedCustom] = useState(true);
    const [showResolvedCustom, setShowResolvedCustom] = useState(true);

    const [filter, setFilter] = useState('desc');
    const [filterCustom, setFilterCustom] = useState('desc');
    const [answers, setAnswers] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
    const [hasCheckBoxAction, setHasCheckBoxAction] = useState(false);

    const [isOpen, setIsOpen] = useState(false)
    const [individual, setIndividual] = useState(false)
    const [anyId, setId] = useState('')
    const [anyType, setType] = useState('')
    const [questionIndex, setQuestionIndex] = useState(undefined);
    const [showActions, setShowActions] = useState(false);

    const [ansIndex, setAnsIndex] = useState(undefined);
    const [blur, setBlur] = useState(false);

    const [cusResponseIndex, setCusResponseIndex] = useState(undefined);
    const [cusBlur, setCusBlur] = useState(false);
    const [token, setToken] = useState('');

    const filterOption = [
        {
            key: 'desc',
            name: 'Newest first'
        },
        {
            key: 'asc',
            name: 'Oldest first'
        }
    ]

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        fetchCurrentSurvey()
    }, []);

    useEffect(()=> {
        fetchCurrentSurvey()
        setToken(user.token)
    },[token])

    useEffect(() => {
        if (customQuestions && surveyDetails) {
            setLoadCustom(true);
            let attributes = {
                surveyId: surveyDetails.surveyId,
                questionId: customQuestions[0]?.id,
                filter: filterCustom,
                showNew: showNewCustom?1:0,
                showReplied: showRepliedCustom?1:0,
                showResolved: showResolvedCustom?1:0
            }

            fetchCustomQuestionsAnswers(attributes)
        }
    },[customQuestions, showNewCustom, showRepliedCustom, showResolvedCustom, filterCustom])

    useEffect(()=> {
        if (hasCheckBoxAction) {
            loadResponses()
        }
    },[showNew, showResolved, showReplied, hasCheckBoxAction, loadResponse])


    useEffect(()=> {
        if (isOpen) {
            loadResponses()
        }
    },[isOpen, questionIndex, filter, anyId])

    useEffect(()=> {
        if (individual) {
            loadResponses()
        }
    },[individual])

    const showHide = (index, id, type) => {
        if (questionIndex !== index) {
            setQuestionIndex(index)
            setIsOpen(true)
        } else {
            setQuestionIndex(undefined)
            setIsOpen(false)
        }

        setId(id)
        setType(type)
    }
    const fetchIndividualAnswers = (id, type, index) => {
        if (questionIndex !== index) {
            setQuestionIndex(index)
            setIsOpen(true)
        }

        setId(id)
        setType(type)
        setIndividual(true)
    }
    const loadResponses = async () => {
        await fetchResponses();

        setHasCheckBoxAction(false)
        setIndividual(false)
    }
    const fetchResponses = async () => {
        setLoadResponse(true)

        let attribute = {
            answerId: anyType == 'answer' ? anyId : null,
            questionId: anyType == 'question' ? anyId : null,
            showReplied: showReplied?1:0,
            showResolved: showResolved?1:0,
            showNew: showNew?1:0,
            filter: filter,
            // location: selectedLocation,
            // team: selectedTeam
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.get('/user-answer-api?', { params: attribute}).then(response => {
            setAnswers(response.data.answer)
            setAnswersStatusCount(response.data.answersStatusCount)
            setLoadResponse(false)

            setBlur(false)
            setAnsIndex(undefined)
        }).catch(Error => {
            setLoadResponse(false)
            setBlur(false)
            setAnsIndex(undefined)
            console.log('Error: ', Error.response.data)
        })
    }
    function fetchCustomQuestionsAnswers (attributes) {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.get('/user-answer-custom-api?', { params: attributes} ).then(function (response) {
            setUserAnswerCustoms(response.data.answer)
            setCustomAnswersCount(response.data.answer.length)
            setLoadCustom(false)
        }).catch(error => {
            console.log('Error: ', error.response.data)
        })
    }
    function handleReloadCustomQuestions () {
        let attributes = {
            surveyId: surveyDetails.surveyId,
            questionId: customQuestions[0]?.id,
            filter: filterCustom,
            showNew: showNewCustom?1:0,
            showReplied: showRepliedCustom?1:0,
            showResolved: showResolvedCustom?1:0
        }

        axiosConfig.get('/user-answer-custom-api?', { params: attributes} ).then(function (response) {
            setUserAnswerCustoms(response.data.answer)
            setCustomAnswersCount(response.data.answer.length)
            setLoadCustom(false)

            setCusBlur(false)
            setCusResponseIndex(undefined)
        }).catch(error => {
            setCusBlur(false)
            setCusResponseIndex(undefined)
            console.log('Error: ', error.response.data)
        })
    }
    function fetchCurrentSurvey() {
        try {
            let attributes = {
                location: selectedLocation,
                team: selectedTeam,
                category: selectedCategory,
            }

            axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
            axiosConfig.get('/current-survey-api?', { params: attributes }).then(response => {
                if (response.data.surveyTotal !== 0) {
                    setLocations(response.data.locations)
                    setTeams(response.data.teams)
                    setFirstSurvey(response.data.firstSurvey)
                    setQuestions(response.data.questions.data)
                    setCustomQuestions(response.data.customQuestions)
                    setAnswersStatusCount(response.data.answersStatusCount)
                    setAllAnswerStatusCount(response.data.allAnswerStatusCount)
                    setMessageCount(response.data.messageCount)
                    setAnswerPercentage(response.data.answerPercentage)
                    setGraphPercentage(response.data.graphPercentage)
                    setCategories(response.data.categories)
                    setLoading(false)
                    setRefreshing(false);
                } else {
                    setLoading(false)
                    setRefreshing(false);
                    setQuestions([])

                    if (selectedCategory == 'custom') {
                        setCustomQuestions(response.data.customQuestions)
                    }

                    if (selectedCategory == 'happiness') {
                        setCustomQuestions([])
                    }
                }

                setSurveyStatus(response.data.status)
                setSurveyTotal(response.data.surveyTotal)
                setSurveyDetails(response.data.surveyDetails)

                if (selectedCategory == null && selectedTeam == null && selectedLocation == null) {
                    setSurveyCompleted(surveyDetails?.surveyCompleted)
                }
            }).catch(error => {
                setLoading(false)
                setRefreshing(false);
                console.log('error: ', error.response.data)
            })
        } catch (error) {
            setLoading(false)
            setRefreshing(false);
            console.error(error)
        }
    }
    function category(question) {
        if (question?.sub_category_id != null && question.company_id == null) {
            return <Text>Category: {question.sub_category?.category?.name}</Text>
        }

        if (question?.sub_category_id == null && question.company_id == null) {
            return <Text>Category: Happiness</Text>
        }
        if (question.custom) {
            return <Text>Category: Custom</Text>
        }
    }

    const checkBoxAction =  async (status, index, checkBoxStatus) => {
        setHasCheckBoxAction(true)

        if(status === 'new') {
            setShowNew(checkBoxStatus)
        }
        else if (status === 'replied') {
            setShowReplied(checkBoxStatus)
        }
        else {
            setShowResolved(checkBoxStatus)
        }
    }
    const checkBoxActionCustom =  async (status, checkBoxStatus) => {
        if(status === 'new') {
            setShowNewCustom(checkBoxStatus)
        }
        else if (status === 'replied') {
            setShowRepliedCustom(checkBoxStatus)
        }
        else {
            setShowResolvedCustom(checkBoxStatus)
        }
    }
    function customQuestionResponses (response, index) {
        return (
            <Block style={[styles.container, (cusBlur && (index !== cusResponseIndex)) && styles.blur]} key={index}>
                <Block style={[styles.responseContainer, (response.status == 'resolved' && styles.messageResolved)]}>
                    <Block style={{ flex: 6 }}>
                        <Text size={12} style={textColor(response.status)}>
                            {response.message_entry}
                        </Text>
                    </Block>
                    <Block style={{ flex: 1, flexDirection: 'row', marginTop: 10, marginBottom: -10}}>
                        <Block style={{ flex: 1}}>
                            <Text size={10} muted style={textColor(response.status)}>
                                Location:
                                {
                                    response?.user?.location_employee?.map((location, index) => {
                                        return (
                                            (index ? ', ' : '') + location?.location?.name
                                        )
                                    })
                                }
                            </Text>
                        </Block>
                        <Block style={{ flex: 1}}>
                            <Text size={10} muted style={[textColor(response.status), {textAlign: 'right'}]}>
                                {response?.user?.team_employee?.length > 0 && <Text>Team: </Text>}
                                {
                                    response?.user?.team_employee?.map((team, index) => {
                                        return (
                                            (index ? ', ' : '') + team?.team?.name
                                        )
                                    })
                                }
                            </Text>
                            {
                                response.status !== 'new' ? (
                                    <Text bold size={10} style={[true ? ({color: '#36d79a'}):({color: 'white'}), {
                                        textAlign: 'right'
                                    }]}>{response.status}</Text>
                                ) : (
                                    <TouchableOpacity>
                                        <Text
                                            onPress={()=> actions(index)}
                                            size={10} muted bold style={{textAlign: 'right', color: '#32325d'}}
                                        >click to reply/resolve</Text>
                                    </TouchableOpacity>
                                )
                            }
                        </Block>
                    </Block>
                </Block>
                {index === cusResponseIndex &&
                    <CustomResponseActions
                        isMessaging={false}
                        token={user.token}
                        userAnswerCustomId={response.id}
                        questionConversationId={response.question_conversation_id}
                        userId={response.user_id}
                        questionId={response.question_id}
                        handleResponseReload={handleReloadCustomQuestions}
                    />
                }
            </Block>
        )
    }
    const actions = (index) => {
        if (index !== cusResponseIndex) {
            setCusResponseIndex(index)
            setCusBlur(true)
        } else {
            setCusBlur(true)
            setCusResponseIndex(undefined)
        }
    }
    function renderCustomQuestions(custom, cusIndex) {
        return (
            <Block style={styles.surveyDetailsContainer} key={cusIndex}>
                <Block style={{marginBottom: 20}}>
                    <Text size={13}>{custom.text} ({customAnswersCount} replies)</Text>
                </Block>

                <Block style={styles.wrapper}>
                    <CheckBox
                        tintColors={{ true: '#32325d', false: 'black' }}
                        value={showNewCustom}
                        onChange={()=> checkBoxActionCustom('new',showNewCustom?false:true)}
                    />
                    <Text style={styles.text}>
                        Show new
                    </Text>

                    <CheckBox
                        tintColors={{ true: '#32325d', false: 'black' }}
                        value={showRepliedCustom}
                        onChange={()=> checkBoxActionCustom('replied',showRepliedCustom?false:true)}
                    />
                    <Text style={styles.text}>
                        Show replied
                    </Text>

                    <CheckBox
                        tintColors={{ true: '#32325d', false: 'black' }}
                        value={showResolvedCustom}
                        onChange={()=> checkBoxActionCustom('resolved',showResolvedCustom?false:true)}
                    />
                    <Text style={styles.text}>
                        Show resolved
                    </Text>
                </Block>

                <Block>
                    <Block style={styles.dropdownsRow}>
                        <SelectDropdown
                            data={filterOption}
                            onSelect={(selectedItem, index) => {
                                setFilterCustom(selectedItem.key)
                            }}
                            defaultButtonText={filterOption[0].name}
                            buttonTextAfterSelection={(selectedItem, index) => {
                                return selectedItem.name;
                            }}
                            rowTextForSelection={(item, index) => {
                                return item.name;
                            }}
                            buttonStyle={styles.dropdown1BtnStyle}
                            buttonTextStyle={styles.dropdown1BtnTxtStyle}
                            renderDropdownIcon={isOpened => {
                                return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                            }}
                            dropdownIconPosition={'right'}
                            dropdownStyle={styles.dropdown1DropdownStyle}
                            rowStyle={styles.dropdown1RowStyle}
                            rowTextStyle={styles.dropdown1RowTxtStyle}
                            search
                            searchInputStyle={styles.dropdown1searchInputStyleStyle}
                            searchPlaceHolder={'Search here'}
                            searchPlaceHolderColor={'darkgrey'}
                            renderSearchInputLeftIcon={() => {
                                return <FontAwesome name={'search'} color={'#444'} size={18} />;
                            }}
                        />
                    </Block>
                </Block>

                {userAnswerCustoms.map((response, index) => {
                    return (
                        <Block key={index}>

                            {response.status !== 'new' ?
                                <TouchableOpacity
                                    onPress={()=>navigation.navigate('Messaging', {response, token, type: 'custom'})}
                                >
                                    {customQuestionResponses(response, index)}
                                </TouchableOpacity>
                                :
                                    customQuestionResponses(response, index)

                            }
                        </Block>
                    )
                })}
            </Block>
        )
    }

    const textColor = (status) => {
        if (status == 'resolved') {
            return styles.white
        } else if (status == 'replied') {
            return styles.gray
        }
    }

    function renderQuestions () {
        return (
            questions.map((question, index) => {
                return (
                    <Block style={styles.surveyDetailsContainer} key={index}>
                        <Block style={{marginBottom: 20}}>
                            <Text muted>{ category(question) }</Text>
                            <Text size={13}>{question.text + ' (' + allAnswerStatusCount[question.id]?.messageCount} replies)</Text>
                        </Block>
                        {
                            question.answers.map((answer, answerIndex) => {
                                return (
                                    <BarGraph
                                        iWhich={'current'}
                                        key={answerIndex}
                                        answer={answer}
                                        answerText={answer.text}
                                        answerPercentage={answerPercentage[answer.id]}
                                        fetchIndividualAnswers={fetchIndividualAnswers}
                                        index={index}
                                    />
                                )
                            })
                        }

                        <Block style={{flexDirection: 'row'}}>
                            <Block style={{backgroundColor: '#edeff2', height: 1, flex: 1, alignSelf: 'center'}} />
                            <TouchableOpacity
                                onPress={() => showHide(index, question.id, 'question')}
                            >
                                <Text size={10} muted style={{textDecorationLine: "underline"}}>
                                    <Text style={{ textDecorationLine: 'underline', alignSelf:'center', paddingHorizontal:5, fontSize: 12, color: '#8898aa' }}>
                                        {questionIndex === index ? 'Hide' : 'Show'} messages
                                    </Text>
                                </Text>
                            </TouchableOpacity>
                            <Block style={{backgroundColor: '#edeff2', height: 1, flex: 1, alignSelf: 'center'}} />
                        </Block>

                            {(questionIndex == index || (hasCheckBoxAction && questionIndex == index)) && (
                                <Block>
                                    <ResponseFilter
                                        index={index}
                                        questionIndex={questionIndex}
                                        showNew={showNew}
                                        showReplied={showReplied}
                                        showResolved={showResolved}
                                        checkBoxAction={checkBoxAction}
                                        setFilter={setFilter}
                                        questionId={question.id}
                                        allAnswerStatusCount={allAnswerStatusCount}
                                        answersStatusCount={answersStatusCount}
                                        answers={question?.answers}
                                        setId={setId}
                                        setType={setType}
                                    />

                                    {loadResponse ? (
                                        <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                                    ) : (
                                        answers.map((answer, aIndex) => {
                                            return (
                                                <Block key={aIndex}>
                                                    {answer.status != 'new' ? (
                                                        <TouchableOpacity
                                                            onPress={()=>navigation.navigate('Messaging', {answer, token})}
                                                        >
                                                            <Responses
                                                                answer={answer}
                                                                setActions={setShowActions}
                                                                showActions={showActions}
                                                                aIndex={aIndex}
                                                                setIndex={setAnsIndex}
                                                                ansIndex={ansIndex}
                                                                setBlur={setBlur}
                                                                blur={blur}
                                                            />
                                                        </TouchableOpacity>
                                                    ) : (
                                                            <Responses
                                                                answer={answer}
                                                                setActions={setShowActions}
                                                                showActions={showActions}
                                                                aIndex={aIndex}
                                                                setIndex={setAnsIndex}
                                                                ansIndex={ansIndex}
                                                                setBlur={setBlur}
                                                                blur={blur}
                                                            />
                                                        )
                                                    }
                                                    {
                                                        (aIndex == ansIndex) &&
                                                        <ResponseActions
                                                            isMessaging={false}
                                                            answer={answer}
                                                            token={user.token}
                                                            state={'fixed-question'}
                                                            questionConversationId={answer.question_conversation_id}
                                                            userAnswerId={answer.id}
                                                            userId={answer?.user_id}
                                                            questionId={question?.id}
                                                            answerId={answer?.answer_id}
                                                            handleResponseReload={fetchResponses}
                                                        />
                                                    }
                                                </Block>
                                            )
                                        })
                                      )
                                    }
                                </Block>
                            )
                            }
                    </Block>
                )
            })
        )
    }
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block flex>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }
                >
                    <TrialBanner navigation={navigation} />
                    {loading ?
                        <Spinner
                            visible={loading}
                            textContent={'Please Wait...'}
                            textStyle={styles.spinnerTextStyle}
                        />
                        :
                        <Block>
                            {
                                (surveyTotal < 1 && surveyStatus=='no-survey' &&  !loading) &&
                                <Block style={styles.noSurvey}>
                                    <Text style={{textAlign: 'center'}} muted>You have no surveys at the moment</Text>
                                </Block>
                            }
                            {
                                ((surveyTotal > 0 || surveyStatus=='has-survey') && !loading) &&
                                <Block>
                                        <Block flex style={styles.surveyDetailsContainer}>
                                            <Block flex>
                                                <Block style={styles.surveyDetails}>
                                                    {!loading &&
                                                        <SurveyDetails
                                                            surveyDetails={surveyDetails}
                                                            messageCount={messageCount}
                                                        />
                                                    }
                                                </Block>
                                            </Block>
                                        </Block>
                                    {
                                        surveyDetails?.surveyCompleted < 5 &&
                                        <Block style={styles.noSurvey}>
                                            <Text muted size={12}>We're still collecting data. We will send survey result as soon as receive 5 employees' reply</Text>
                                        </Block>
                                    }
                                    {
                                        surveyDetails?.surveyCompleted >= 5 &&
                                        <Block>
                                            <OutsideView
                                                childRef={childRef}
                                                onPressOutside={() => {
                                                    setBlur(false)
                                                    setAnsIndex(undefined)
                                                    setCusBlur(false)
                                                    setCusResponseIndex(undefined)
                                                }}
                                            >
                                                {renderQuestions()}
                                                {loadCustom ?

                                                    <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />

                                                    :

                                                    customQuestions.map((custom, index)=> {
                                                        return renderCustomQuestions(custom, index)
                                                    })
                                                }
                                            </OutsideView>
                                        </Block>
                                    }
                                </Block>
                            }
                        </Block>
                    }
                </ScrollView>
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    blur: {
        opacity: 0.4
    },
    messageResolved: {
        textAlign: 'right',
        backgroundColor: '#36d79a'
    },
    white: {
        color: 'white'
    },
    gray: {
        color: '#8898aa'
    },
    responseContainer: {
        padding: theme.SIZES.BASE,
        marginTop: 15,
        backgroundColor: '#edeff2',
        borderRadius: 5,
        maxWidth: 400,
        width: width - theme.SIZES.BASE * 4
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        // paddingVertical: 15,
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    surveyDetailsContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        maxWidth: 500
    },
    noSurvey: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
    },
    surveyDetails: {
        marginTop: 10
    },
    text: {
        lineHeight: 30,
        fontSize: 12
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    },
    dropdownsRow: {
        flexDirection: 'row',
        width: '100%',
        alignSelf: 'flex-end'
    },
    dropdown1BtnStyle: {
        flex: 1,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 4,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 12},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 12},
});
