import React, {useContext, useState} from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView, Image, ActivityIndicator
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import { FontAwesome } from '@expo/vector-icons';

import { Button, Icon, Input } from "../../components";
import { Images, argonTheme } from '../../constants';
import {AuthContext} from "../../context/AuthProvider";

const { width, height } = Dimensions.get("screen");

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { login, error, isLoading } = useContext(AuthContext);

  return (
      <Block style={styles.mainContainer} flex middle>
          <StatusBar/>

          <Block middle style={styles.avatarContainer}>
            <Image
                source={Images.LoginLogo}
                style={styles.avatar}
            />
          </Block>
            {/*<Block safe flex middle>*/}
              <Block style={styles.loginContainer}>
                {/*<Block flex>*/}
                  <Block flex={0.17} middle>
                    <Text color="#8898AA" size={16}>
                      Login to your account
                    </Text>
                  </Block>
                  <Block flex center>
                    <KeyboardAvoidingView
                        style={{ flex: 1 }}
                        behavior="padding"
                        enabled
                    >
                      <Block width={width * 0.8} style={{ marginBottom: 15 }}>
                        {error && <Text style={{ color: 'red' }}>{error}</Text>}
                        <Input
                            borderless
                            placeholder="Email"
                            onChangeText={setEmail}
                            value={email}
                            iconContent={
                              <Icon
                                  size={16}
                                  color={argonTheme.COLORS.ICON}
                                  name="ic_mail_24px"
                                  family="ArgonExtra"
                                  style={styles.inputIcons}
                              />
                            }
                        />
                      </Block>
                      <Block width={width * 0.8}>
                        <Input
                            password
                            borderless
                            placeholder="Password"
                            onChangeText={setPassword}
                            value={password}
                            iconContent={
                              <Icon
                                  size={16}
                                  color={argonTheme.COLORS.ICON}
                                  name="padlock-unlocked"
                                  family="ArgonExtra"
                                  style={styles.inputIcons}
                              />
                            }
                        />
                      </Block>

                      <Block middle>
                        <Button color="success" style={styles.createButton}
                                onPress={() => login(email, password)}
                        >
                          <Block flex style={{ flexDirection: 'row', marginTop: 10}}>
                            <Block>
                              {isLoading && (
                                  <ActivityIndicator
                                      style={{ marginRight: 18 }}
                                      size="small"
                                      color="white"
                                  />
                              )}
                            </Block>
                            <Block>
                              <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                Login
                              </Text>
                            </Block>
                          </Block>
                        </Button>
                      </Block>
                      <Block middle row style={styles.secureConnection}>
                        <FontAwesome name="lock" size={16} color="black" style={styles.inputIcons} color={argonTheme.COLORS.SUCCESS}/>
                        <Text bold size={12} color={argonTheme.COLORS.SUCCESS}>
                          {" "}
                          secure connection
                        </Text>
                      </Block>
                      <Block middle row style={styles.forgotPassword}>
                        <TouchableOpacity
                            onPress={()=> navigation.navigate('Password Reset')}
                        >
                          <Text bold size={12} muted style={styles.underline}>
                            {" "}
                            I forgot my password
                          </Text>
                        </TouchableOpacity>
                      </Block>

                    </KeyboardAvoidingView>
                  </Block>
                {/*</Block>*/}
              </Block>
            {/*</Block>*/}

          <Block middle row style={styles.try}>
            <Text bold size={12} muted>
              {" "}
              Not using vibestrive yet? <Text onPress={()=>navigation.navigate(
                "Register Screen"
            )} style={styles.underline}>Try it for free</Text>
            </Text>
          </Block>
        </Block>
  );
}

const styles = StyleSheet.create({
  avatarContainer: {
    position: "relative",
    marginTop: -80
  },
  avatar: {
    marginBottom: 20
    // width: 124,
    // height: 124,
    // borderRadius: 62,
    // borderWidth: 0
  },
  mainContainer: {
    backgroundColor: "#32325d",
  },
  loginContainer: {
    width: width * 0.9,
    height: height * 0.53,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  underline: {textDecorationLine: 'underline'},
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  secureConnection: {
    textAlign: "center",
    paddingBottom: 30
  },
  forgotPassword: {
    textAlign: "center"
  },
  try: {
    marginTop: 20
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});

// export default LoginScreen;
