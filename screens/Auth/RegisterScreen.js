import React, {useEffect, useState, useRef} from "react";
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Image, Alert
} from "react-native";
import { Block,Text, theme } from "galio-framework";
import { FontAwesome } from '@expo/vector-icons';

import { Button, Input } from "../../components";
import { Images, argonTheme } from '../../constants';
import Ionicons from "@expo/vector-icons/Ionicons";
import SelectDropdown from "react-native-select-dropdown";
import axiosConfig from "../../helpers/axiosConfig";
import Spinner from "react-native-loading-spinner-overlay";
import * as Localization from 'expo-localization';

const { width, height } = Dimensions.get("screen");

export default function LoginScreen({ navigation }) {
  const [loading, setLoading] = useState(false);
  const [countries, setCountries] = useState([])
  const [industries, setIndustries] = useState([])
  const [email, setEmail] = useState('');
  const [company, setCompany] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [industry, setIndustry] = useState('');
  const [companySize, setCompanySize] = useState('');
  const [country, setCountry] = useState('');
  const modalizeRef = useRef(null);

  const company_size = [
    {
      key: '4',
      name: 'less than 5 employees'
    },
    {
      key: '20',
      name: '5-20 employees'
    },
    {
      key: '21-50',
      name: '21-50 employees'
    },
    {
      key: '51-250',
      name: '51-250 employees'
    },
    {
      key: 'more than 250',
      name: 'more than 250 employees'
    }
  ]

  useEffect(()=> {
    fetchCountryIndustry()
  },[])

  function fetchCountryIndustry () {
    axiosConfig.get(`/country_industry`).then(response => {
      setCountries(response.data.countries)
      setIndustries(response.data.industries)
    });
  }

  function register() {
    setLoading(true)

    let attributes = {
      company_name: company,
      first_name: firstName,
      last_name: lastName,
      email: email,
      company_size: companySize,
      industry: industry,
      country_id: country,
      timezone: Localization.timezone
    }

    axiosConfig.post('register', attributes).then(response => {

      setLoading(false)
      navigation.navigate('Success Screen', {
        data: email
      })
    }).catch(error => {
      setLoading(false)
      console.log('error: ', error.response.data)

      const key = Object.keys(error.response.data.message)[0];

      console.log(error.response.data.message[key][0]);
      Alert.alert('Error', error.response.data.message[key][0])
    })
  }
  return (
      <Block flex style={{ backgroundColor: argonTheme.COLORS.THEME_COLOR}}>
        <Block flex>
          <ScrollView>
            {loading &&
                <Spinner
                    visible={loading}
                    textContent={'Please Wait...'}
                    textStyle={styles.spinnerTextStyle}
                />
            }
            <Block middle style={styles.avatarContainer}>
              <Image
                  source={Images.LoginLogo}
                  style={styles.avatar}
              />
            </Block>

            <Block style={[styles.details,{backgroundColor: '#36d79a'}]}>
              <Text color="white" bold style={{marginBottom: 20}} size={15}>30 day free trial</Text>
              <Text color="white" bold>
                Increase your employees' happiness, engagement,
                and loyalty with a 30 day trial where you can <Text color="#32325d">use all of our features for free:</Text>
              </Text>

              <Text style={{marginTop: 20, lineHeight: 22}}>
                <Ionicons name="checkmark-outline" size={18} color="#32325d"/>
                <Text color="#32325d" bold>Weekly automated surveys</Text>
                <Text color="white"> – find out if your employees are happy, frustrated, or need support.</Text>
              </Text>

              <Text style={{marginTop: 20, lineHeight: 22}}>
                <Ionicons name="checkmark-outline" size={18} color="#32325d"/>
                <Text color="#32325d" bold>A safe space for honest feedback</Text>
                <Text color="white"> – get to the heart of your employees' needs with anonymous messaging.</Text>
              </Text>

              <Text style={{marginTop: 20, lineHeight: 22}}>
                <Ionicons name="checkmark-outline" size={18} color="#32325d"/>
                <Text color="#32325d" bold>Employee happiness monitor</Text>
                <Text color="white"> – see the current state of your company's health in one actionable dashboard, and take immediate action where needed.</Text>
              </Text>
            </Block>

            <Block style={[styles.details, {backgroundColor: 'white', marginTop: -3}]}>
              <Block row center space="between">
                <Block flex middle left>
                  <Text color="#32325d" bold>
                    Create your free account
                  </Text>
                </Block>
                <Block flex middle right>
                  <Block flex right>
                    <Block row>
                      <Block row style={styles.secureConnection}>
                        <FontAwesome
                            name="lock"
                            size={15}
                            style={{marginRight: 5}}
                            color="#36d79a"
                        />
                        <Text bold size={12} color="#36d79a">secure connection</Text>
                      </Block>
                    </Block>
                  </Block>
                </Block>
              </Block>
              <Block>
                <Input style={styles.input} onChangeText={setCompany} placeholder={'Company'} iconContent={<Block />}/>
              </Block>
              <Block row center space="between">
                <Block flex middle left style={{marginRight: 20}}>
                  <Input style={styles.input} onChangeText={setFirstName}  placeholder={'First name'} iconContent={<Block />}/>
                </Block>
                <Block flex middle right>
                  <Input style={styles.input}  onChangeText={setLastName} placeholder={'Last name'} iconContent={<Block />}/>
                </Block>
              </Block>
              <Block>
                <Input style={styles.input} onChangeText={setEmail} placeholder={'Email address'} iconContent={<Block />}/>
              </Block>

              <Block style={styles.dropdownsRow}>
                <SelectDropdown
                    data={industries}
                    onSelect={(selectedItem, index) => {
                      console.log(selectedItem);
                      setIndustry(selectedItem.id)
                    }}
                    defaultButtonText={'Your industry'}
                    buttonTextAfterSelection={(selectedItem, index) => {
                      return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                      return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                      return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={12} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                />
              </Block>
              <Block style={styles.dropdownsRow}>
                <SelectDropdown
                    data={company_size}
                    onSelect={(selectedItem, index) => {
                      console.log(selectedItem);
                      setCompanySize(selectedItem.key)
                    }}
                    defaultButtonText={'Your company size'}
                    buttonTextAfterSelection={(selectedItem, index) => {
                      return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                      return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                      return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={12} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                />
              </Block>
              <Block style={styles.dropdownsRow}>
                <SelectDropdown
                    data={countries}
                    onSelect={(selectedItem, index) => {
                      console.log(selectedItem);
                      setCountry(selectedItem.id)
                    }}
                    defaultButtonText={'Your country'}
                    buttonTextAfterSelection={(selectedItem, index) => {
                      return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                      return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                      return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={12} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                />
              </Block>

              <Block>
                <Button color="success" onPress={() => register()} style={styles.button}>Start my free 30 day trial </Button>
                <Text color={argonTheme.COLORS.SUCCESS}>Cancel anytime - no credit card required</Text>
              </Block>
            </Block>

            <Block style={[styles.details, {marginTop: -10}]}>
              <Block row center space="between">
                <Block flex middle center>
                  <Text color="white">
                    By creating your account, you agree to our
                    <Text style={styles.underline} color="white" onPress={()=> navigation.navigate('Terms Screen')} > terms </Text>
                    and
                    <Text style={styles.underline} color="white" onPress={()=> navigation.navigate('Privacy Policy Screen')}> privacy policy. </Text>
                     Already have an account?
                    <Text color="white" onPress={()=> navigation.navigate('Login Screen')}> Login</Text>
                  </Text>
                </Block>
              </Block>
            </Block>
          </ScrollView>
        </Block>
      </Block>
  );
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  button: {
    marginLeft: 0,
    width: width - theme.SIZES.BASE * 10
  },
  divider: {width: 12},
  dropdownsRow: {flexDirection: 'row', width: '100%', paddingTop: '2%', paddingBottom: '2%'},
  dropdown1BtnStyle: {
    flex: 1,
    height: 40,
    backgroundColor: '#edeff2',
    borderRadius: 8,
    borderColor: '#444',
  },
  dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 14},
  dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
  dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
  dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 14},
  input: {
    backgroundColor: '#edeff2',
    borderColor: '#edeff2'
  },
  details: {
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 10,
    marginBottom: 20,
    borderRadius: 6
  },
  mainContainer: {
    backgroundColor: "#32325d"
  },
  accountContainer: {
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 10,
    marginBottom: 20,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
    maxWidth: 500
  },
  avatarContainer: {
    position: "relative",
    marginTop: 80
  },
  registerContainer: {
    width: width * 0.9,
    height: height * 0.53,
    backgroundColor: "#F4F5F7",
    borderRadius: 4
  },
  underline: {
    textDecorationLine: 'underline'
  },
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  secureConnection: {
    textAlign: "center"
  },
  forgotPassword: {
    textAlign: "center"
  },
  try: {
    marginTop: 20
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25
  }
});
