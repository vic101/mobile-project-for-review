import {argonTheme, Images} from "../../constants";
import {Block, Text, theme} from "galio-framework";
import {Alert, Dimensions, Image, ScrollView, StyleSheet, TouchableOpacity} from "react-native";
import React, {useState} from "react";
import {Button} from "../../components";

const { width, height } = Dimensions.get("screen");
export default function Terms({navigation}) {
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.THEME_COLOR}}>
            <Block flex>
                <ScrollView>
                    <Block middle style={styles.avatarContainer}>
                        <Image
                            source={Images.LoginLogo}
                            style={styles.avatar}
                        />
                    </Block>


                    <Block style={[styles.details, {backgroundColor: 'white'}]}>
                        <Block>
                            <Button
                                small
                                center
                                color="default"
                                style={styles.optionsButton}
                                onPress={()=> navigation.navigate('Register Screen')}
                            >
                                Continue Registration
                            </Button>
                        </Block>

                        <Text size={16} bold>
                            Website Terms and Conditions of Use
                        </Text>
                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>1. Terms</Text>
                            <Text size={12}>
                                By accessing this Website, accessible from https://app.vibestrive.com/, you are agreeing to be bound by these Website Terms and Conditions of Use and agree that you are responsible for the agreement with any applicable local laws. If you disagree with any of these terms, you are prohibited from accessing this site. The materials contained in this Website are protected by copyright and trade mark law.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>2. Use License</Text>
                            <Text size={12}>Permission is granted to temporarily download one copy of the materials on THEDIGITAL OÜ's Website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:</Text>
                            <Text style={{marginLeft: 14}}>
                                modify or copy the materials;
                                use the materials for any commercial purpose or for any public display;
                                attempt to reverse engineer any software contained on THEDIGITAL OÜ's Website;
                                remove any copyright or other proprietary notations from the materials; or
                                transferring the materials to another person or "mirror" the materials on any other server.
                            </Text>
                            <Text size={12}>
                                This will let THEDIGITAL OÜ to terminate upon violations of any of these restrictions. Upon termination, your viewing right will also be terminated and you should destroy any downloaded materials in your possession whether it is printed or electronic format. These Terms of Service has been created with the help of the Terms Of Service Generator.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>3. Disclaimer</Text>
                            <Text size={12}>
                                All the materials on THEDIGITAL OÜ’s Website are provided "as is". THEDIGITAL OÜ makes no warranties, may it be expressed or implied, therefore negates all other warranties. Furthermore, THEDIGITAL OÜ does not make any representations concerning the accuracy or reliability of the use of the materials on its Website or otherwise relating to such materials or any sites linked to this Website.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>4. Limitations</Text>
                            <Text size={12}>
                                THEDIGITAL OÜ or its suppliers will not be hold accountable for any damages that will arise with the use or inability to use the materials on THEDIGITAL OÜ’s Website, even if THEDIGITAL OÜ or an authorize representative of this Website has been notified, orally or written, of the possibility of such damage. Some jurisdiction does not allow limitations on implied warranties or limitations of liability for incidental damages, these limitations may not apply to you.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>5. Revisions and Errata</Text>
                            <Text size={12}>
                                The materials appearing on THEDIGITAL OÜ’s Website may include technical, typographical, or photographic errors. THEDIGITAL OÜ will not promise that any of the materials in this Website are accurate, complete, or current. THEDIGITAL OÜ may change the materials contained on its Website at any time without notice. THEDIGITAL OÜ does not make any commitment to update the materials.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>6. Links</Text>
                            <Text size={12}>
                                THEDIGITAL OÜ has not reviewed all of the sites linked to its Website and is not responsible for the contents of any such linked site. The presence of any link does not imply endorsement by THEDIGITAL OÜ of the site. The use of any linked website is at the user’s own risk.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>7. Site Terms of Use Modifications</Text>
                            <Text size={12}>
                                THEDIGITAL OÜ may revise these Terms of Use for its Website at any time without prior notice. By using this Website, you are agreeing to be bound by the current version of these Terms and Conditions of Use.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>8. Your Privacy</Text>
                            <Text size={12}>
                                Please read our Privacy Policy.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>9. Governing Law</Text>
                            <Text size={12}>
                                Any claim related to THEDIGITAL OÜ's Website shall be governed by the laws of ee without regards to its conflict of law provisions.
                            </Text>
                        </Block>
                        <Block>
                            <Button
                                small
                                center
                                color="default"
                                style={styles.optionsButton}
                                onPress={()=> navigation.navigate('Register Screen')}
                            >
                                Continue Registration
                            </Button>
                        </Block>
                    </Block>
                </ScrollView>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    optionsButton: {
        width: "auto",
        height: 34,
        paddingHorizontal: theme.SIZES.BASE,
        paddingVertical: 10
    },
    text: {
        lineHeight: 30
    },
    wrapper: {
        marginTop: 20,
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
    },
    details: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6
    },
    mainContainer: {
        backgroundColor: "#32325d"
    },
    accountContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        maxWidth: 500
    },
    avatarContainer: {
        position: "relative",
        marginTop: 80
    },
    registerContainer: {
        width: width * 0.9,
        height: height * 0.53,
        backgroundColor: "#F4F5F7",
        borderRadius: 4
    },
});
