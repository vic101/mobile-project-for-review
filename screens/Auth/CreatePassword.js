import {argonTheme, Images} from "../../constants";
import {Block, Text, theme} from "galio-framework";
import React, {useContext, useEffect, useState} from "react";
import {Dimensions, Image, StyleSheet, TouchableOpacity, ScrollView, Alert} from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import {Button, Input} from "../../components";
import axiosConfig from "../../helpers/axiosConfig";
import {AuthContext} from "../../context/AuthProvider";
import * as SecureStore from "expo-secure-store";

const { width, height } = Dimensions.get("screen");

const win = Dimensions.get('window');
const ratio = width/523;

export default function CreatePassword({navigation}) {
    const { user } = useContext(AuthContext);
    const [loading, setLoading] = useState(false);
    const [isValid, setIsValid] = useState(false);
    const [statePass, setStatePass] = useState('');
    const [stateConfirm, setStateConfirm] = useState('');
    const [testing, setTesting] = useState('testing');
    const [hasLetter, setHasLetter] = useState(false);
    const [hasNumber, setHasNumber] = useState(false);
    const [moreThanSix, setMoreThanSix] = useState(false);
    const [passCheck, setPassCheck] = useState(false);
    const [password, setPassword] = useState(undefined);
    const [confirm_password, setConfirm_password] = useState(undefined);

    let match       = null;
    let matchAll    = false;
    let not_match   = '';

    useEffect(()=> {
        SecureStore.getItemAsync("step").then(step => {
            console.log('step: ', step)
        })
    },[])

    const savePassword = () => {
        setIsValid(true)
        setLoading(true)

        let attribute = {
            'device': 'mobile',
            'password': password
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.post('/update-password-api', attribute).then(response => {
            navigation.navigate('SetupLocations')
        }).catch(function (error) {
            console.log('Error: ', error.response.data)

            setLoading(false)
            setIsValid(false)
        })
    }
    const hasLetters= (textEntry) => {
        let letterCount = 0;
        let string = textEntry;
        let i = 0;

        for (i=0; i<textEntry.length; i++) {
            if (/^[a-zA-Z]+$/.test(string.charAt(i))) {
                letterCount += 1;
            }
        }
        if(letterCount > 0) {
            // hasLetter = true;
            setHasLetter(true)
        } else {
            // hasLetter = false;
            setHasLetter(false)
        }
    }
    const hasNumbers = (textEntry) => {
        let isNumber = /\d/;
        isNumber.test(textEntry) ? setHasNumber(true) : setHasNumber(false)
    }
    const sixToTwelve = (textEntry) => {
        if(textEntry.length > 5 && textEntry.length < 13) {
            setMoreThanSix(true)
        } else {
            setMoreThanSix(false)
        }
    }

    const passwordMatch = (txtEntryPassword, txtEntryConfirmPassword) => {
        if (txtEntryPassword === '') {
            if ( (confirm_password !== '' && statePass !== '')) {
                console.log('both fields not empty.')

                if (confirm_password === statePass) {
                    console.log('Password match.')

                    match = 1;
                    not_match = '';

                    if (passCheck) {
                        matchAll = true;
                        setIsValid(true);

                    } else {
                        matchAll = false;
                    }
                } else {
                    match = 2;
                    not_match = 'not match';
                    matchAll = false;

                    console.log('Wala nag match ang password!!!!')
                    setIsValid(false);
                }
            }
        }

        if (txtEntryConfirmPassword === '') {
            if ( (stateConfirm !== '' && txtEntryPassword !== '')) {
                console.log('both fields not empty.')

                if (stateConfirm === txtEntryPassword) {
                    console.log('Password match.')

                    match = 1;
                    not_match = '';

                    if (passCheck) {
                        matchAll = true;
                        setIsValid(true);

                    } else {
                        matchAll = false;
                    }
                } else {
                    match = 2;
                    not_match = 'not match';
                    matchAll = false;

                    console.log('Wala nag match ang password!!!!')
                    setIsValid(false);
                }
            }
        }
    }
    const matchPassword = (confirm) => {
        setStateConfirm(confirm);
        setConfirm_password(confirm);

        if(passCheck) {
            if(password === confirm) {
                matchAll = true;
            }  else {
                matchAll = false;
            }
        }

        passwordMatch(password, confirm)
    }
    const validateEntry = (textEntryPassword) => {
        setPassword(textEntryPassword);

        hasLetters(textEntryPassword);
        hasNumbers(textEntryPassword);
        sixToTwelve(textEntryPassword);

        if(hasLetter && hasNumber && moreThanSix) {
            setPassCheck(true);
        } else {
            setPassCheck(false);
        }

        passwordMatch(textEntryPassword, confirm_password)
    }

    const alertMessage = () => {
        Alert.alert('Error', 'All password criteria must be met');
    }

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.THEME_COLOR}}>
            <ScrollView>
                <Block flex>
                    <Block middle style={styles.avatarContainer}>
                        <Image
                            source={Images.LoginLogo}
                            style={styles.avatar}
                        />
                    </Block>

                    <Block middle style={styles.avatarContainer}>
                        <Image
                            source={Images.setupPassword}
                            style={styles.imageStyle}
                        />
                    </Block>
                    <Block row center space="between" style={{marginTop: 20}}>
                        <Block flex middle center>
                            <Block row style={{textAlign: 'center'}}>
                                <Text color="#36d79a">
                                    Your account setup is finished in about 2 minutes
                                </Text>
                            </Block>
                        </Block>
                    </Block>
                    <Block style={[styles.details, {backgroundColor: 'white', marginTop: 30}]}>
                        <Block row center space="between" style={{marginTop: 20}}>
                            <Block flex middle center>
                                <Block row style={{textAlign: 'center'}}>
                                    <Text color="#32325d" bold>
                                        Please create your account password.
                                    </Text>
                                </Block>
                                <Block row style={{textAlign: 'center'}}>
                                    <Text color="#36d79a" bold>
                                        Your connection is secure.
                                    </Text>
                                </Block>
                            </Block>
                        </Block>

                        <Block row center space="between" style={{marginTop: 20}}>
                            <Block style={styles.container}>
                                <Block flex middle>
                                    <Input
                                        style={styles.input}
                                        onChangeText={(textEntry) => validateEntry(textEntry)}
                                        placeholder={'New password'}
                                        iconContent={<Block />}
                                    />

                                    <Input
                                        style={styles.input}
                                        onChangeText={(confirm) => matchPassword(confirm)}
                                        placeholder={'Confirm password'}
                                        iconContent={<Block />}
                                    />

                                    {((password !== undefined && confirm_password !== undefined) && (password !== confirm_password)) ?
                                        <Text size={12} color="red">Password does not match</Text>
                                        :
                                        null
                                    }
                                </Block>
                            </Block>
                        </Block>

                        <Block row center space="between" >
                            <Block style={styles.message}>
                                {moreThanSix ?
                                    <Block>
                                        <Text size={12}>
                                            <Ionicons name="checkmark-outline" size={18} color="#36d79a"/>
                                            <Text color="#36d79a">6 - 12 characters</Text>
                                        </Text>
                                    </Block>
                                    :
                                    <Text size={12} muted>
                                        <Ionicons name="checkmark-outline" size={18} color="white"/>
                                        <Text>6 - 12 characters {moreThanSix}</Text>
                                    </Text>
                                }
                                {hasLetter ?
                                    <Block>
                                        <Text size={12}>
                                            <Ionicons name="checkmark-outline" size={18} color="#36d79a"/>
                                            <Text color="#36d79a">at least 1 letter {String(hasLetter)}</Text>
                                        </Text>
                                    </Block>
                                    :
                                    <Text size={12} muted>
                                        <Ionicons name="checkmark-outline" size={18} color="white"/>
                                        <Text>at least 1 letter</Text>
                                    </Text>
                                }
                                {hasNumber ?
                                    <Block>
                                        <Text size={12}>
                                            <Ionicons name="checkmark-outline" size={18} color="#36d79a"/>
                                            <Text color="#36d79a">at least 1 number</Text>
                                        </Text>
                                    </Block>:
                                    <Text size={12} muted>
                                        <Ionicons name="checkmark-outline" size={18} color="white"/>
                                        <Text>at least 1 number</Text>
                                    </Text>
                                }
                            </Block>
                        </Block>

                        <Block flex middle center style={{marginTop: 20, marginBottom: 70}}>
                            <Block>
                                {((moreThanSix && hasLetter && hasNumber) && (password === confirm_password)) ?
                                    <Button
                                        color="success"
                                        onPress={() => savePassword()}
                                        style={styles.buttonEnabled}
                                    >
                                        Set my password
                                    </Button>
                                    :
                                    <Button
                                        color="success"
                                        onPress={()=> alertMessage()}
                                        style={styles.buttonDisabled}
                                    >
                                        Set my password
                                    </Button>
                                }

                            </Block>
                        </Block>
                    </Block>
                </Block>
            </ScrollView>
        </Block>
    )
}

export const styles = StyleSheet.create({
    buttonEnabled: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 10,
        borderRadius: 6
    },
    buttonDisabled: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 10,
        borderRadius: 6,
        backgroundColor: '#76dfb2'
    },
    imageStyle: {
        width: width - theme.SIZES.BASE * 2,
        height: 30 * ratio
    },
    black: {
        color: 'black'
    },
    container: {
        flex: 1,
        flexDirection: "row",
        alignContent: "space-between",
        justifyContent: "center",
        maxWidth: 500,
        marginBottom: 10
    },
    barGraph: {
        flexDirection: 'row',
        padding: 10,
        borderRadius: 5,
        maxWidth: 400,
    },
    message: {
        minWidth: 5,
        justifyContent: 'center',
        marginLeft: -40,
        marginRight: 15
    },

    details: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6
    },
    avatarContainer: {
        position: "relative",
        marginTop: 80
    },
    input: {
        backgroundColor: '#edeff2',
        borderColor: '#edeff2',
        width: width - theme.SIZES.BASE * 15
    },
})
