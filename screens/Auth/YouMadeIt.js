import {argonTheme, Images} from "../../constants";
import {Block, Text, theme} from "galio-framework";
import React, {useContext, useState} from "react";
import {Dimensions, Image, StyleSheet} from "react-native";
import {Button} from "../../components";
import {AuthContext} from "../../context/AuthProvider";

const { width, height } = Dimensions.get("screen");
export default function YouMadeIt({navigation}) {
    const { user } = useContext(AuthContext);

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.THEME_COLOR}}>
            <Block flex>
                <Block middle style={styles.avatarContainer}>
                    <Image
                        source={Images.LoginLogo}
                        style={styles.avatar}
                    />
                </Block>

                <Block style={[styles.details, {backgroundColor: 'white', marginTop: 40}]}>
                    <Block row center space="between" style={{marginTop: 20}}>
                        <Block flex middle center>
                            <Block row style={{textAlign: 'center'}}>
                                <Text color="#36d79a" bold>
                                    You made it, {user.first_name}!
                                </Text>
                            </Block>
                        </Block>
                    </Block>

                    <Block row center space="between" style={{marginTop: 20}}>
                        <Block flex middle center>
                            <Block row style={{textAlign: 'center'}}>
                                <Text muted bold color="#32325d">
                                    Let's start with a few quick steps
                                </Text>
                            </Block>
                            <Block row style={{textAlign: 'center'}}>
                                <Text muted bold color="#32325d">
                                    to make your start as smooth as possible.
                                </Text>
                            </Block>
                        </Block>
                    </Block>

                    <Block flex middle center style={{marginTop: 40, marginBottom: 70}}>
                        <Block>
                            <Button color="success" onPress={() => navigation.navigate('CreatePassword')} style={styles.button}>Awesome, continue.</Button>
                        </Block>
                    </Block>
                </Block>
            </Block>
        </Block>
    )
}

export const styles = StyleSheet.create({
    details: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6
    },
    avatarContainer: {
        position: "relative",
        marginTop: 80
    },
    button: {
        marginLeft: 0,
        width: width - theme.SIZES.BASE * 10
    },
})
