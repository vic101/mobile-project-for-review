import {argonTheme, Images} from "../../constants";
import {Block, Text, theme} from "galio-framework";
import React from "react";
import {Image, StyleSheet} from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";

export default function SuccessScreen({navigation, route}) {

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.THEME_COLOR}}>
            <Block flex>
                <Block middle style={styles.avatarContainer}>
                    <Image
                        source={Images.LoginLogo}
                        style={styles.avatar}
                    />
                </Block>

                <Block style={[styles.details, {backgroundColor: 'white', marginTop: 40}]}>
                    <Block row center space="between" style={{marginTop: 20}}>
                        <Block flex middle center>
                            <Block row style={{textAlign: 'center'}}>
                                <Ionicons name="checkmark-circle" size={20} color="#36d79a"/>
                                <Text color="#36d79a" bold>
                                    Your account is ready.
                                </Text>
                            </Block>
                        </Block>
                    </Block>

                    <Block row middle center style={{marginTop: 20}}>
                        <Text size={14} bold>We emailed your login link to {route.params.data}</Text>
                    </Block>

                    <Block flex middle center style={{marginTop: 60, marginBottom: 40}}>
                        <Text muted size={12}>
                            If our email didn't arrive within the next 2 minutes,
                        </Text>
                        <Text muted size={12}>please check your spam folder.</Text>

                        <Text muted size={12}
                              onPress={()=> navigation.navigate('Login Screen')}
                              style={{marginTop: 10, textDecorationLine: 'underline', marginBottom: 10}}>
                            Login now to continue
                        </Text>
                    </Block>
                </Block>
            </Block>
        </Block>
    )
}

export const styles = StyleSheet.create({
    details: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6
    },
    avatarContainer: {
        position: "relative",
        marginTop: 80
    },
})
