import {argonTheme, Images} from "../../constants";
import {Block, Text, theme} from "galio-framework";
import React, {useContext, useEffect, useState} from "react";
import {Dimensions, Image, StyleSheet, ScrollView, TouchableOpacity, Alert} from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import {Button, Input} from "../../components";
import Spinner from "react-native-loading-spinner-overlay";
import axiosConfig from "../../helpers/axiosConfig";
import {AuthContext} from "../../context/AuthProvider";

const { width, height } = Dimensions.get("screen");
const ratio = width/553;

export default function SetupLocations({navigation}) {
    const { user } = useContext(AuthContext);
    const [textInput, setTextInput] = useState([]);
    const [inputData, setInputData] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(()=> {
        addTextInput(textInput.length)
    },[])

    function addTextInput (index) {
        setLoading(true);
        textInput.push(
            <Block row center space="between" style={index === 0 ? {marginTop: 20} : null} key={index}>
                <Block center>
                    <Input
                        onChangeText={(location) => addValues(location, index)}
                        style={styles.input} placeholder={'Your city office name'} iconContent={<Block />}
                    />
                </Block>

                <Block right style={{marginLeft: 8}}>
                    <Ionicons
                        disabled={index === 0 ? true : false}
                        name="remove-circle"
                        size={18} color={index === 0 ? '#FFF' : '#8898aa'}
                        onPress={()=> removeTextInput()}
                    />
                </Block>
            </Block>
        );

        setTextInput(textInput);

        setTimeout(()=> {
            setLoading(false);
        }, 500)
    }

    const addValues = (location, index) => {
        let dataArray = inputData;
        let checkBool = false;

        if (dataArray.length !== 0){
            dataArray.forEach(element => {
                if (element.index === index ){
                    element.location = location;
                    checkBool = true;
                }
            });
        }

        if (checkBool){
            setInputData(dataArray)
        }

        else {
            dataArray.push({'location':location,'index':index});
            setInputData(dataArray)
        }
    }

    const removeTextInput = () => {
        setLoading(true);

        textInput.pop();
        inputData.pop();

        setTextInput(textInput)
        setInputData(inputData)

        setTimeout(()=> {
            setLoading(false);
        }, 500)
    }
    const alertMessage = () => {
        Alert.alert('Error', 'Please check empty fields')
    }
    const saveLocations = () => {
        let attribute = {
            locations: inputData
        }

        if (inputData.length !== textInput.length) {
            alertMessage();
            return;
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.post('location-api', attribute).then(response => {
            navigation.navigate('SetupTeams')
        }).catch(error => {
            console.log('error: ', error.response.data)
        })
    }
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.THEME_COLOR}}>
            <Block flex>
                <ScrollView>
                    <Block middle style={styles.avatarContainer}>
                        <Image
                            source={Images.LoginLogo}
                            style={styles.avatar}
                        />
                    </Block>

                    <Block middle style={styles.avatarContainer}>
                        <Image
                            source={Images.setupLocations}
                            style={styles.imageStyle}
                        />
                    </Block>

                    <Block row center space="between" style={{marginTop: 20}}>
                        <Block flex middle center>
                            <Block row style={{textAlign: 'center'}}>
                                <Text color="#36d79a">
                                    Your account setup is finished in about 2 minutes
                                </Text>
                            </Block>
                        </Block>
                    </Block>

                    <Block style={[styles.details, {backgroundColor: 'white', marginTop: 40}]}>
                        <Block row center space="between" style={{marginTop: 20}}>
                            <Block flex middle center>
                                <Block row style={{textAlign: 'center'}}>
                                    <Text color="#32325d" bold>
                                        Set up your locations.
                                    </Text>
                                </Block>
                                <Block row style={{textAlign: 'center'}}>
                                    <Text color="#36d79a" bold>
                                        You can filter your employee feedback by location.
                                    </Text>
                                </Block>
                            </Block>
                        </Block>

                        {
                            textInput.map((value) => {
                            return value
                        })}

                        <Block row center space="between" style={{marginTop: 20}}>
                            <Block center>
                                <TouchableOpacity
                                    onPress={ () => addTextInput(textInput.length)}
                                >
                                    <Ionicons
                                        name="add-circle"
                                        size={18} color="#32325d"
                                    />
                                </TouchableOpacity>
                            </Block>
                            <Block right style={{marginLeft: 8}}>
                                <TouchableOpacity
                                    onPress={ () => addTextInput(textInput.length)}
                                >
                                    <Text muted>Add another location</Text>
                                </TouchableOpacity>
                            </Block>
                        </Block>

                        <Block flex middle center style={{marginTop: 20, marginBottom: 70}}>
                            <Block>
                                <Button color="success" onPress={ () => saveLocations() } style={styles.button}>Save my locations</Button>
                            </Block>
                            <Block>
                                <Text color="#36d79a" size={12}>You can always edit your locations later</Text>
                            </Block>
                        </Block>
                    </Block>
                </ScrollView>
            </Block>
        </Block>
    )
}

export const styles = StyleSheet.create({
    imageStyle: {
        width: width - theme.SIZES.BASE * 2,
        height: 39 * ratio
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    black: {
        color: 'black'
    },
    container: {
        flex: 1,
        flexDirection: "row",
        alignContent: "space-between",
        justifyContent: "center",
        maxWidth: 500,
        marginBottom: 10
    },
    barGraph: {
        flexDirection: 'row',
        padding: 10,
        borderRadius: 5,
        maxWidth: 400,
    },
    message: {
        minWidth: 5,
        justifyContent: 'center',
        // marginLeft: -40,
        // marginRight: 15
    },

    details: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6
    },
    avatarContainer: {
        position: "relative",
        marginTop: 80
    },
    button: {
        marginLeft: 0,
        width: width - theme.SIZES.BASE * 10
    },
    input: {
        backgroundColor: '#edeff2',
        borderColor: '#edeff2',
        width: width - theme.SIZES.BASE * 14
    },
})
