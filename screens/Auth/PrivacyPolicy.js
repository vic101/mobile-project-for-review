import {argonTheme, Images} from "../../constants";
import {Block, Text, theme} from "galio-framework";
import {Dimensions, Image, ScrollView, StyleSheet} from "react-native";
import React from "react";
import {Button} from "../../components";

const { width, height } = Dimensions.get("screen");
export default function PrivacyPolicy({navigation}) {
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.THEME_COLOR}}>
            <Block flex>
                <ScrollView>
                    <Block middle style={styles.avatarContainer}>
                        <Image
                            source={Images.LoginLogo}
                            style={styles.avatar}
                        />
                    </Block>


                    <Block style={[styles.details, {backgroundColor: 'white'}]}>
                        <Block>
                            <Button
                                small
                                center
                                color="default"
                                style={styles.optionsButton}
                                onPress={()=> navigation.navigate('Register Screen')}
                            >
                                Continue Registration
                            </Button>
                        </Block>

                        <Text size={16} bold>
                            Privacy Policy of THEDIGITAL OÜ {'\n'}
                        </Text>
                        <Block>
                            <Text>
                                THEDIGITAL OÜ operates the https://app.vibestrive.com/ website, which provides the SERVICE.
                                {'\n'}
                            </Text>

                            <Text>
                                This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service, the vibestrive website.
                                {'\n'}
                            </Text>
                            <Text>
                                If you choose to use our Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that we collect are used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy. Our Privacy Policy was created with the help of the Privacy Policy Template Generator.
                                {'\n'}
                            </Text>
                            <Text>
                                The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at https://app.vibestrive.com/, unless otherwise defined in this Privacy Policy.
                                {'\n'}
                            </Text>
                        </Block>
                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Information Collection and Use</Text>
                            <Text size={12}>
                                For a better experience while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to your name, phone number, and postal address. The information that we collect will be used to contact or identify you.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Log Data</Text>
                            <Text size={12}>
                                We want to inform you that whenever you visit our Service, we collect information that your browser sends to us that is called Log Data. This Log Data may include information such as your computer’s Internet Protocol ("IP") address, browser version, pages of our Service that you visit, the time and date of your visit, the time spent on those pages, and other statistics.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Cookies</Text>
                            <Text size={12}>
                                Cookies are files with small amount of data that is commonly used an anonymous unique identifier. These are sent to your browser from the website that you visit and are stored on your computer’s hard drive. {'\n'}
                                {'\n'}
                                Our website uses these "cookies" to collection information and to improve our Service. You have the option to either accept or refuse these cookies, and know when a cookie is being sent to your computer. If you choose to refuse our cookies, you may not be able to use some portions of our Service.

                                {'\n'}{'\n'}
                                For more general information on cookies, please read "Cookies" article from the Privacy Policy Generator.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Service Providers</Text>
                            <Text size={12}>
                                We may employ third-party companies and individuals due to the following reasons:
                            </Text>

                            <Block style={{marginTop: 10, marginLeft: 10, marginBottom: 10}}>
                                <Text>To facilitate our Service;</Text>
                                <Text>To provide the Service on our behalf;</Text>
                                <Text>To perform Service-related services; or</Text>
                                <Text>To assist us in analyzing how our Service is used.</Text>
                            </Block>
                            <Text>
                                We want to inform our Service users that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Security</Text>
                            <Text size={12}>
                                We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Links to Other Sites</Text>
                            <Text size={12}>
                                Our Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over, and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.
                                {'\n'}
                                {'\n'}
                                Children's Privacy
                                {'\n'}
                                {'\n'}
                                Our Services do not address anyone under the age of 13. We do not knowingly collect personal identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Changes to This Privacy Policy</Text>
                            <Text size={12}>
                                We may update our Privacy Policy from time to time. Thus, we advise you to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page.
                            </Text>
                        </Block>

                        <Block>
                            <Text size={14} style={{marginTop: 20}} bold>Contact Us</Text>
                            <Text size={12}>
                                If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.
                            </Text>
                        </Block>
                        <Block>
                            <Button
                                small
                                center
                                color="default"
                                style={styles.optionsButton}
                                onPress={()=> navigation.navigate('Register Screen')}
                            >
                                Continue Registration
                            </Button>
                        </Block>
                    </Block>
                </ScrollView>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    optionsButton: {
        width: "auto",
        height: 34,
        paddingHorizontal: theme.SIZES.BASE,
        paddingVertical: 10
    },
    text: {
        lineHeight: 30
    },
    wrapper: {
        marginTop: 20,
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
    },
    details: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6
    },
    mainContainer: {
        backgroundColor: "#32325d"
    },
    accountContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        maxWidth: 500
    },
    avatarContainer: {
        position: "relative",
        marginTop: 80
    },
    registerContainer: {
        width: width * 0.9,
        height: height * 0.53,
        backgroundColor: "#F4F5F7",
        borderRadius: 4
    },
});
