import React, {useContext, useState} from "react";
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  KeyboardAvoidingView, Image, ActivityIndicator, Alert
} from "react-native";
import { Block, Checkbox, Text, theme } from "galio-framework";
import { FontAwesome } from '@expo/vector-icons';

import { Button, Icon, Input } from "../../components";
import { Images, argonTheme } from '../../constants';
import {AuthContext} from "../../context/AuthProvider";
import axiosConfig from "../../helpers/axiosConfig";

const { width, height } = Dimensions.get("screen");

export default function PasswordResetScreen({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { login, error } = useContext(AuthContext);
  const [isUpdate, setIsUpdate] = useState(false);
  const [errors, setError] = useState([]);

  function updatePassword () {
    if (email === '') {
      Alert.alert('Error', 'Please enter your email')
      return;
    }

    setIsUpdate(true)

    let attribute = {
      email: email
    }

    axiosConfig.post('/password/email', attribute).then(response => {

      Alert.alert('Success', response.data.message)
      setIsUpdate(false)
      setError('')
    }).catch(error => {
      setError(error.response.data.errors)
      setIsUpdate(false)
    })
  }

  return (
      <Block style={styles.mainContainer} flex middle>
          <StatusBar/>

          <Block middle style={styles.avatarContainer}>
            <Image
                source={Images.LoginLogo}
                style={styles.avatar}
            />
          </Block>
              <Block style={styles.loginContainer}>
                  <Block flex={0.17} middle style={{marginTop: 10}}>
                    <Text color="#212529" size={14}>
                      Reset Password
                    </Text>
                  </Block>
                  <Block flex center>
                    <KeyboardAvoidingView
                        style={{ flex: 1 }}
                        behavior="padding"
                        enabled
                    >
                      <Block width={width * 0.8} >
                        <Input
                            error={errors.email ? true : false}
                            placeholder="E-Mail Address"
                            onChangeText={setEmail}
                            iconContent={
                              <Icon
                                  size={16}
                                  color={argonTheme.COLORS.ICON}
                                  name="ic_mail_24px"
                                  family="ArgonExtra"
                                  style={styles.inputIcons}
                              />
                            }
                        />
                        <Text style={{color: '#f5365c'}}>{errors.email}</Text>
                      </Block>

                      <Block middle>
                        <Button style={styles.sendButton}
                                onPress={() => updatePassword()}
                        >
                          <Block flex style={{ flexDirection: 'row', marginTop: 10}}>
                            <Block>
                              {isUpdate && (
                                  <ActivityIndicator
                                      style={{ marginRight: 16 }}
                                      size="small"
                                      color="white"
                                  />
                              )}
                            </Block>
                            <Block>
                              <Text bold size={14} color={argonTheme.COLORS.WHITE}>
                                Send Password Reset Link
                              </Text>
                            </Block>
                          </Block>
                        </Button>
                      </Block>
                      <Block middle row style={styles.forgotPassword}>
                        <Text size={12} muted>
                          {" "}
                          Remember your password? <Text onPress={()=> navigation.navigate('Login Screen')} style={styles.underline}>Login</Text>
                        </Text>
                      </Block>

                    </KeyboardAvoidingView>
                  </Block>
              </Block>
        </Block>
  );
}

const styles = StyleSheet.create({
  avatarContainer: {
    position: "relative",
    marginTop: -80
  },
  avatar: {
    marginBottom: 20
  },
  mainContainer: {
    backgroundColor: "#32325d",
  },
  loginContainer: {
    width: width * 0.9,
    height: height * 0.3,
    backgroundColor: "#F4F5F7",
    borderRadius: 4,
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: "hidden"
  },
  underline: {textDecorationLine: 'underline'},
  socialConnect: {
    backgroundColor: argonTheme.COLORS.WHITE,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: "#8898AA"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: "#fff",
    shadowColor: argonTheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: argonTheme.COLORS.PRIMARY,
    fontWeight: "800",
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12
  },
  secureConnection: {
    textAlign: "center",
    paddingBottom: 30
  },
  forgotPassword: {
    textAlign: "center"
  },
  try: {
    marginTop: 20
  },
  sendButton: {
    backgroundColor: '#38c172',
    width: width * 0.6
  }
});
