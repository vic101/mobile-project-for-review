import React, {useEffect, useState, useContext} from "react";
import {
    StyleSheet,
    ScrollView,
    Image,
    ImageBackground,
    Platform, TouchableOpacity
} from "react-native";
import { Block, Text, theme } from "galio-framework";

import {Dimensions} from "react-native";
import {argonTheme} from "../../constants";
import Spinner from "react-native-loading-spinner-overlay";

const { width, height } = Dimensions.get("screen");
export default function SurveyTranslations({ navigation }) {
    const [loading, setLoading] = useState(true);

    useEffect(()=> {
        setTimeout(()=> {
            setLoading(false)
        }, 1000)
    },[])

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block flex>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {loading ?
                        <Block style={styles.container}>
                            <Text style={{textAlign: 'center'}} bold muted size={14}>Processing data please wait...</Text>
                            <Spinner
                                visible={true}
                                textContent={'Please Wait...'}
                                textStyle={styles.spinnerTextStyle}
                            />
                        </Block>
                        :
                        <Block style={styles.container}>
                            <Text style={{textAlign: 'center'}} bold muted size={14}>Features Coming soon...</Text>
                        </Block>
                    }
                </ScrollView>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    container: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        maxWidth: 500
    },
});
