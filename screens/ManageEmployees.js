import React, {useContext, useEffect, useRef, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Image,
    ImageBackground,
    Platform, ActivityIndicator, Alert, StatusBar
} from "react-native";
import { Card } from '../components';
import {Block, Text, theme} from "galio-framework";

import { Button, Icon } from "../components";
import { Images, argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import Ionicons from '@expo/vector-icons/Ionicons';
import ManageEmployeesLinks from "../components/links/ManageEmployeesLinks";
import {AuthContext} from "../context/AuthProvider";
import axiosConfig from "../helpers/axiosConfig";
import {Menu, MenuItem} from "react-native-material-menu";
import TrialBanner from "../components/TrialBanner";
import { fetchEmployees } from "../components/manage/Employees";
import { sort } from "../components/manage/sort"
import options from "../components/manage/sortOptions"
import SelectDropdown from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";

import UsersContainer from "../components/usersContainer";

const { width, height } = Dimensions.get("screen");

export default function ManageEmployees({route, navigation}) {
    const modalizeRef = useRef(null);
    const [data, setData] = useState([]);
    const [pending, setPending] = useState([]);
    const [verifiedEmployees, setVerifiedEmployees] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isLoadingVerified, setIsLoadingVerified] = useState(true);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [page, setPage] = useState(1);
    const [isAtEndOfScrolling, setIsAtEndOfScrolling] = useState(false);
    const [activeIndex, setActiveIndex] = useState(undefined);
    const [activeVerifiedIndex, setActiveVerifiedIndex] = useState(undefined);

    const [teamIndex, setTeamIndex] = useState(undefined);
    const [locIndex, setLocIndex] = useState(undefined);

    const [teamPendingIndex, setTeamPendingIndex] = useState(undefined);
    const [locPendingIndex, setLocPendingIndex] = useState(undefined);

    const { user } = useContext(AuthContext);

    useEffect(() => {
        fetchPendingVerifiedEmployees(null,null, 'pending')
        fetchPendingVerifiedEmployees(null,null, 'bottom')
    }, [page]);
    useEffect(() => {
        if (route.params?.newDataAdded) {
            fetchPendingVerifiedEmployees(null,null, route.params?.type)
        }
    }, [route.params?.newDataAdded]);

    const fetchPendingVerifiedEmployees = async (type, order, fetchType) => {
        const data = await fetchEmployees(
            user,
            'employee',
            fetchType,
            type??'first_name',
            order??'desc',
            10,
            'teamEmployee',
            'locationEmployee'
        )

        if (data) {
            if (fetchType == 'pending') {
                setPending(data);
                setIsLoading(false);
            } else {
                setVerifiedEmployees(data);
                setIsLoadingVerified(false);
            }
        }
    };
    const filter = (sortType, fetchType) => {
        sort(sortType, fetchType, fetchPendingVerifiedEmployees)
    }

    function gotoNewEmployee(item, type, action) {
        navigation.navigate('FormUser', {form: 'user', item: item, type: type, action: action});
    }

    function handleRefresh(state) {
        setPage(1);
        setIsAtEndOfScrolling(false);
        setIsRefreshing(true);

        fetchPendingVerifiedEmployees(null,null, state)
    }
    function deleteEmployee(id, state) {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        axiosConfig.delete(`/user-api/${id}`).then(response => {
            Alert.alert('Success', 'Employee was deleted.');
            handleRefresh(state)
        })
            .catch(error => {
                console.log(error.response);
            });
    }
    function showAlert(id, state) {
        console.log('state: ', state)
        Alert.alert('Delete this employee?', 'This can not be reverted!', [
            {
                text: 'Cancel',
                onPress: () => modalizeRef.current?.close(),
                style: 'cancel',
            },
            {
                text: 'OK',
                onPress: () => deleteEmployee(id, state),
                style: 'default',
            },
        ]);

        state == 'pending' ? setActiveIndex(undefined) : setActiveVerifiedIndex(undefined);
    }

    function showHideList(type, index) {
        return (
            <Block>
                {type == 'team' && <Text onPress={()=> teamIndex !== index ? setTeamIndex(index) : setTeamIndex(undefined)} muted size={12} style={styles.underline}>Hide list</Text>}
                {type == 'location' && <Text onPress={()=> locIndex !== index ? setLocIndex(index) : setLocIndex(undefined)} muted size={12} style={styles.underline}>Hide list</Text>}

                {type == 'teamPending' && <Text onPress={()=> teamPendingIndex !== index ? setTeamPendingIndex(index) : setTeamPendingIndex(undefined)} muted size={12} style={styles.underline}>Hide list</Text>}
                {type == 'locationPending' && <Text onPress={()=> locPendingIndex !== index ? setLocPendingIndex(index) : setLocPendingIndex(undefined)} muted size={12} style={styles.underline}>Hide list</Text>}
            </Block>
        )
    }
    function renderList (data, type, index) {
        return (
            <Block>
                {showHideList(type, index)}

                {(type === 'team' || type === 'teamPending') &&
                    data.map((team, index) => {
                        return (
                            <Block key={index}>
                                <Text style={{color: '#32325d'}} size={12}>{team?.team?.name}</Text>
                            </Block>
                        )
                    })
                }

                {(type === 'location' || type === 'locationPending') &&
                    data.map((location, index) => {
                        return (
                            <Block key={index}>
                                <Text style={{color: '#32325d'}} size={12}>{location?.location?.name}</Text>
                            </Block>
                        )
                    })
                }

                {showHideList(type, index)}
            </Block>
        )
    }

    function renderInviteSentTime(employee) {
        if (employee.invite_sent_time == null) {
            return <Text>{pending.note}</Text>
        }
        if(employee.invite_sent_time != null && employee.invite_sent > 0) {
            if (employee.created_ago_day > 0) {
                if (employee.created_ago_day >= 7) {
                    return <Text style={{ color: '#f5365c'}} size={13}>{employee.created_ago_mobile}</Text>;
                }
                else {
                    return <Text size={13}>{employee.created_ago_mobile}</Text>;
                }
            } else {
                return <Text size={13}>Invite was sent today</Text>
            }
        }
    }
    function renderPendingEmployees() {
        return (
            <Block flex>
                {isLoading ? (
                    <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                ) : (
                    <Block flex style={styles.listContainer}>
                        <Block style={{ flexDirection: 'row'}}>
                            <Block style={{ flex: 10 }}>
                                <Text muted style={{ fontWeight: 'bold', marginBottom: 5 }} >{data.user?.total} pending invites</Text>
                            </Block>
                            <Block style={{ flex: 20}}>
                                { renderDropdown('pending') }
                            </Block>
                        </Block>

                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>

                        {
                            pending?.user?.data.map((employee, index) => {
                                return (
                                    <Block key={index}>
                                        <Block style={{ flexDirection: 'row' }}>
                                            <Block style={{ flex: 12, marginRight: 2 }}>
                                                <Text size={13}>
                                                    {
                                                        employee.first_name + ' '
                                                    }
                                                    <Text style={{ fontWeight: 'bold' }}>
                                                        { employee.last_name + ' ' }
                                                        <Text muted>{employee.email}</Text>
                                                    </Text>
                                                </Text>

                                                <Text muted size={13}>
                                                    <Text onPress={()=> teamPendingIndex !== index ? setTeamPendingIndex(index) : setTeamIndex(undefined)}>
                                                        {employee.teams}
                                                    </Text>
                                                    <Text> | </Text>
                                                    <Text onPress={()=> locPendingIndex !== index ? setLocPendingIndex(index) : setLocPendingIndex(undefined)}>
                                                        {employee.locations}
                                                    </Text>
                                                </Text>
                                                {renderInviteSentTime(employee)}
                                                {
                                                    (employee.team_employee.length > 0 &&teamPendingIndex === index) &&
                                                    renderList(employee.team_employee, 'teamPending', index)
                                                }
                                                {
                                                    (employee.location_employee.length > 0 &&locPendingIndex === index) &&
                                                    renderList(employee.location_employee, 'locationPending', index)
                                                }
                                            </Block>
                                            <Block>
                                                <Ionicons
                                                    onPress={() => activeIndex !== index ? setActiveIndex(index) : setActiveIndex(undefined)}
                                                    name="ellipsis-vertical" size={14} color="#8898aa"
                                                />
                                                <Menu
                                                    visible={activeIndex === index} onRequestClose={() => setActiveIndex(undefined)}
                                                    row middle space="between"
                                                >
                                                    <MenuItem onPress={()=> gotoNewEmployee(employee, 'employee', 'update')}>Edit</MenuItem>
                                                    <MenuItem onPress={()=> showAlert(employee.id, 'pending')}>Delete</MenuItem>
                                                </Menu>
                                            </Block>
                                        </Block>

                                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                            <Block style={styles.divider} />
                                        </Block>
                                    </Block>
                                )
                            })
                        }

                        <Block>
                            <Text size={12} muted>To ensure anonymity of your employees, they will be shown as "pending" until they received their first survey.</Text>
                        </Block>
                    </Block>
                )
                }
            </Block>
        )
    }
    function renderVerifiedEmployees() {
        return (
            <Block flex>
                {isLoadingVerified ? (
                    <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                ) : (
                    <Block flex style={styles.listContainer}>
                        <Block style={{ flexDirection: 'row'}}>
                            <Block style={{ flex: 10}}>
                                <Text muted style={{ fontWeight: 'bold', marginBottom: 5 }} >{verifiedEmployees?.user?.total} Employees</Text>
                            </Block>
                            <Block style={{ flex: 20}}>
                                { renderDropdown('bottom') }
                            </Block>
                        </Block>


                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>
                        {
                            verifiedEmployees?.user?.data?.map((employee, index) => {
                                return (
                                    <Block key={index}>
                                        <Block style={{ flexDirection: 'row' }}>
                                            <Block style={{ flex: 12, marginRight: 2 }}>
                                                <Text size={13}>
                                                    {
                                                        employee.first_name + ' '
                                                    }
                                                    <Text style={{ fontWeight: 'bold' }}>
                                                        { employee.last_name + ' ' }
                                                        <Text muted>{employee.email}</Text>
                                                    </Text>
                                                </Text>

                                                <Text muted size={13}>
                                                    <Text onPress={()=> teamIndex !== index ? setTeamIndex(index) : setTeamIndex(undefined)}>
                                                        {employee.teams}
                                                    </Text>
                                                    <Text> | </Text>
                                                    <Text onPress={()=> locIndex !== index ? setLocIndex(index) : setLocIndex(undefined)}>
                                                        {employee.locations}
                                                    </Text>
                                                </Text>

                                                <Text muted size={13}>
                                                    {employee.last_login_ago_day < 30 && employee.last_login_ago}
                                                    {employee.last_login_ago_day > 30 && <Text style={{color: '#f5365c'}}>Last login: more than 30 days ago</Text>}
                                                </Text>
                                                {
                                                    (employee.team_employee.length > 0 &&teamIndex === index) &&
                                                    renderList(employee.team_employee, 'team', index)
                                                }
                                                {
                                                    (employee.location_employee.length > 0 &&locIndex === index) &&
                                                    renderList(employee.location_employee, 'location', index)
                                                }
                                            </Block>
                                            <Block>
                                                <Ionicons
                                                    onPress={() => activeVerifiedIndex !== index ? setActiveVerifiedIndex(index) : setActiveVerifiedIndex(undefined)}
                                                    name="ellipsis-vertical" size={14} color="#8898aa"
                                                />
                                                <Menu
                                                    visible={activeVerifiedIndex === index} onRequestClose={() => setActiveVerifiedIndex(undefined)}
                                                    row middle space="between"
                                                >
                                                    <MenuItem onPress={()=> gotoNewEmployee(employee, 'employee', 'update')}>Edit</MenuItem>
                                                    <MenuItem onPress={()=> showAlert(employee.id, 'bottom')}>Delete</MenuItem>
                                                </Menu>
                                            </Block>
                                        </Block>

                                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                            <Block style={styles.divider} />
                                        </Block>
                                    </Block>
                                )
                            })
                        }
                    </Block>
                )
                }
            </Block>
        )
    }
    function renderDropdown (fetchType) {
        return (
            <Block style={styles.dropdownsRow}>
                <SelectDropdown
                    data={options}
                    onSelect={(selectedItem, index) => {
                        console.log('status: ', selectedItem.key)
                        filter(selectedItem.key, fetchType)
                    }}
                    defaultButtonText={options[0].name}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                        return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                        return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                    search
                    searchInputStyle={styles.dropdown1searchInputStyleStyle}
                    searchPlaceHolder={'Search here'}
                    searchPlaceHolderColor={'darkgrey'}
                    renderSearchInputLeftIcon={() => {
                        return <FontAwesome name={'search'} color={'#444'} size={18} />;
                    }}
                />
                <Block style={styles.dividerDropdown} />
            </Block>
        )
    }
    return (
      <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
          <ScrollView showsVerticalScrollIndicator={false}>
              <TrialBanner navigation={navigation}/>
              <Block flex style={styles.descriptionsContainer}>
                  <Block flex>
                      <Block style={styles.details}>
                          <ManageEmployeesLinks route={route} navigation={navigation}/>
                      </Block>
                  </Block>
              </Block>
              <Block flex style={styles.descriptionsContainer}>
                  <Block flex>
                      <Block style={styles.details}>
                             <Text bold size={13} color="#36d79a" style={{ marginBottom: 10}}>
                              About employees
                          </Text>
                          <Text size={13} color="#32325d" style={styles.aboutText}>
                              Invite your employees to let them participate in your regular surveys, and send you valuable feedback.
                          </Text>

                          <Text size={13} color="#32325d" style={styles.aboutText}>
                              This way, you can increase their happiness, engagement, and loyalty towards your organization.
                          </Text>

                          <Text size={13} muted>
                              By the way, you never pay for pending invites.
                          </Text>
                      </Block>
                  </Block>
              </Block>

              <Block>
                  <Button onPress={()=>gotoNewEmployee(null,'employee', 'create')} color="default" style={styles.button}>
                      Add employee
                  </Button>
              </Block>

              { renderPendingEmployees() }
              { renderVerifiedEmployees() }
          </ScrollView>
      </Block>
    );
}

const styles = StyleSheet.create({
    underline: {
        textDecorationLine: "underline",
        color: '#32325d'
    },
    dropdownsRow: {
        flexDirection: 'row',
        width: '60%',
        alignSelf: 'flex-end',
        marginRight: -5
    },
    dropdown1BtnStyle: {
        flex: 1,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 4,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 12},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 12},
    dividerDropdown: {width: 12},
    button: {
        width: width - theme.SIZES.BASE * 16,
        marginLeft: 16,
        marginTop: 16
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: width,
        height: height,
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    descriptionsContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    details: {
        marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    aboutText: {
        lineHeight: 20,
        marginBottom: 15
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    }
});
