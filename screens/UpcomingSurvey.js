import React, {useContext, useEffect, useState} from "react";
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Platform, Alert
} from "react-native";
import { Block, Text, theme } from "galio-framework";

import {Button, Input, Switch} from "../components";
import { Images, argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import TrialBanner from "../components/TrialBanner";
const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function UpcomingSurvey ({ navigation }) {
  const [isLoading, setIsLoading] = useState(true);
  const [radioCustom, setRadioCustom] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const { user } = useContext(AuthContext);

  const [questions, setQuestions] = useState([]);
  const [language, setLanguage] = useState('')
  const [details, setSurveyDetails] = useState([]);
  const [disabled, setDisabled] = useState(0);
  const [total, setTotal] = useState(0);
  const [customQuestion, setCustomQuestion] = useState([]);

  const [inputValue, setInputValue] = useState('')
  const [timer, setTimer] = useState('')
  const [saving, setSaving] = useState(false);
  const [edited, setEdited] = useState(false);

  useEffect( ()=> {
    fetchSurveys()
  },[]);

  const inputChanged = (newText) => {
    setInputValue(newText);
    setSaving(true)
    clearTimeout(timer)

    const newTimer = setTimeout(() => {
      callingApi()
    }, 2000)

    setTimer(newTimer)
  }
  const settingRadioCustom = (value) => {
    setRadioCustom(value = !value)
    setEdited(false)
    setSaving(false)
  }
  const callingApi = () => {
    saveQuestion()
    setSaving(false)
    setEdited(true)
  }

  function saveQuestion () {

    if (customQuestion?.id) {
      let attributes = {
        text: inputValue,
        language: language?.language?.language ?? '',
        custom: 1,
        status: customQuestion.status
      }
      console.log('attributes: ', attributes)
      axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
      axiosConfig.patch(`/question-api/${customQuestion.id}`, attributes).then(response => {
        console.log('message: ', response.data.message)
        console.log('edited: ', edited)
        console.log('saving: ', saving)
      }).catch(error => {
        console.log(error.response.data)
      })
    } else {

    }
  }
  function setAvailability (question, disabled_questions) {
    let attributes = {
      questionId: question.id,
      surveyId: details.surveyId
    }

    axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;

    if (disabled_questions.length < 1) {
      if (disabled >= total) {
        let message = 'Your upcoming survey must include at least 1 question.';
            Alert.alert(message);
            return;
      } else {
        axiosConfig.post('/set-question-availability-api', attributes).then( function () {})
        fetchSurveys()
      }

    } else {
      let disabledQuestionId = question.disabled_questions[0].id
      axiosConfig.delete('/set-question-availability-api/' + disabledQuestionId).then(function (response) {
        fetchSurveys()
      }).catch(error => {
        console.log('error: ', error.response.data)

        // const key = Object.keys(error.response.data.errors)[0];
        // setError(error.response.data.errors[key][0]);
        // setIsLoading(false);
      });
    }

    // setTimeout(function () {
    //   this.selected   = false
    // }, 2000)
  }
  function fetchSurveys () {
    axiosConfig.defaults.headers.common[
        'Authorization'
        ] = `Bearer ${user.token}`;

    let attributes = {
      'type': 'name',
      'order' : 'asc',
      id: user.id
    }

    axiosConfig.get(`/upcoming-survey-api?`, {params: attributes}).then(response => {
      setQuestions(response.data.survey)
      setSurveyDetails(response.data.surveyDetails)
      setTotal(response.data.total)
      setDisabled(response.data.disabled)
      setLanguage(response.data.language)

      // console.log('survey: ', response.data.survey)
      // this.surveyDetails = response.data.surveyDetails
      //
      // this.total         = response.data.total
      // this.disabled      = response.data.disabled
      // this.language      = response.data.language
      // this.languagesUsed = response.data.languagesUsed
      // this.employeeCountLanguageUsed = response.data.employeeCountLanguageUsed

      const question = response.data.survey.find(question => question.custom === 1)
      if (question) {
        setCustomQuestion({...question})
        setInputValue(question?.text?.en)
      }

      setIsLoading(false);
      setIsRefreshing(false);
    })
        .catch(error => {
          console.log(error);
          setIsLoading(false);
          setIsRefreshing(false);
        });
  }

  function renderAnswers (quest) {
    let answerString = '';

    quest.answers.map((answer, index) => {
      answerString += answer.text.en + (index+1 < quest.answers.length ? ', ':'')
    })

    return(
          <Block >
            {quest.id == 1 ?
                <Text size={12} muted>NPS: about to quit / jumping with joy</Text>
                :
                <Text size={12} muted>Multiple choice : { answerString } </Text>
            }
          </Block>
        )
  }
  function renderUpcomingSurvey () {
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
          <Block flex style={styles.upcomingSurveyContainer}>
            <Block flex>
              <Block style={styles.upcomingDetails}>
                <Text bold size={15} color="#32325D" style={styles.text}>
                  Survey details
                </Text>
                <Text size={15} color="#32325D" style={styles.text}>
                  Start: {details.start} <Text muted size={12}>{details.status})</Text>
                </Text>
                <Text size={15} color="#32325D" style={styles.text}>
                  End: {details.end} <Text muted>soon</Text>
                </Text>
              </Block>
            </Block>
          </Block>

          <Block flex style={styles.upcomingSurveyContainer}>
            {
              questions.map((question, index) => {
                if (!question.custom) {
                  return (
                    <Block key={index}>
                      <Block style={{ flexDirection: 'row' }}>
                          <Block style={{ flex: 1}}>
                            <Switch style={styles.switch}
                                    trackColor={{true: "#36d79a", false: 'grey'}}
                                    value={!question.disabled_questions.length}
                                    thumbColor={{true: "#36d79a", false: 'grey'}}
                                    onValueChange={() => setAvailability(question, question.disabled_questions)}
                            />
                          </Block>
                          <Block style= {{ flex: 7, marginBottom: 20 }}>
                            <Text bold size={13} color="#32325D" style={styles.questionText}>
                              { question.text.en }
                            </Text>
                            { renderAnswers(question) }
                          </Block>
                        </Block>
                      <Block middle style={{ marginBottom: 16 }}>
                      <Block style={styles.divider} />
                      </Block>
                  </Block>
                  )
                }
              })
            }

              <Block style={{ flexDirection: 'row' }}>
                <Block style={{ flex: 1}}>
                  <Switch style={{ color: "#36d79a", marginTop: 15 }}
                          trackColor={{true: "#36d79a", false: 'grey'}}
                          value={radioCustom}
                          thumbColor={{true: "#36d79a", false: 'grey'}}
                          onValueChange={()=> settingRadioCustom(radioCustom)}
                  />
                </Block>
                <Block style={{ flex: 7, marginBottom: 20 }}>
                  <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                    <>
                      {saving &&
                            <Block style={{flex: 1}}>
                              <Text muted size={11} style={{ textAlign: 'right'}}>Saving</Text>
                            </Block>
                      }
                      {(edited && !saving && radioCustom) &&
                          <Block style={{flex: 1}}>
                            <Text muted size={11} style={{textAlign: 'right', color: '#36d79a'}}>saved</Text>
                          </Block>
                      }
                    </>

                    {radioCustom &&
                        <Input
                            onChangeText={newText => inputChanged(newText)}
                            defaultValue={inputValue}
                            right placeholder="Type your custom question here."
                            iconContent={<Block />}
                        />
                    }
                    {!radioCustom &&
                        <Input
                            editable={radioCustom}
                            value={inputValue}
                            right placeholder="Type your custom question here."
                            iconContent={<Block />}
                        />
                    }
                    <Text muted size={12}>Free text | Your employees will see that this is a custom question</Text>
                  </Block>
                </Block>
              </Block>
          </Block>
        </Block>
    )
  }

  return (
      <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
        <Block flex>
          <ScrollView showsVerticalScrollIndicator={false}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <TrialBanner navigation={navigation}/>
              { renderUpcomingSurvey() }
            </ScrollView>
          </ScrollView>
        </Block>
      </Block>
  );
}

const styles = StyleSheet.create({
  divider: {
    width: "90%",
    borderWidth: 0.8,
    borderColor: "#E9ECEF"
  },
  verticalLine: {
    height: '100%',
    width: 1,
    backgroundColor: '#909090',
  },
  profile: {
    marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1
  },
  textCustom: {
    backgroundColor: argonTheme.COLORS.BACKGROUND
  },
  profileContainer: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1
  },
  profileBackground: {
    width: width,
    height: height / 2
  },
  upcomingSurveyContainer: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 10,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2
  },
  info: {
    paddingHorizontal: 40
  },
  avatarContainer: {
    position: "relative",
    marginTop: -80
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0
  },
  upcomingDetails: {
    marginTop: 10
  },
  text: {
    lineHeight: 30
  },
  questionText: {
    lineHeight: 20,
    // flexWrap: 'wrap',
    // flex: 1
  },
  switch: {
    // marginBottom: 40
    marginRight: 3,
    color: "#36d79a"
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: "center",
    width: thumbMeasure,
    height: thumbMeasure
  }
});
