import React, {useContext, useEffect, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    Platform, ActivityIndicator, Alert
} from "react-native";

import { Block, Text, theme, Radio } from "galio-framework";
import { argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import AccountLinks from "../components/links/AccountLinks";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import {Input} from "../components";
import Spinner from "react-native-loading-spinner-overlay";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function Account ({route, navigation}) {
    const { user } = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(true);
    const [isUpdate, setIsUpdate] = useState(false);
    const [inactive, setInactive] = useState(false);
    const [closed, setClosed] = useState(false);
    const [one, setOne] = useState(false);
    const [seven, setSeven] = useState(false);
    const [thirty, setThirty] = useState(false);
    const [never, setNever] = useState(false);


    const [oldEmail, setOldEmail] = useState(false);
    const [automaticLogout, setAutomaticLogout] = useState('');
    const [status, setStatus] = useState(false);
    const [pendingEmail, setPendingEmail] = useState(user.pendingEmail);
    const [update, setUpdate] = useState(false);
    const [updated, setUpdated] = useState(false);
    const [newEmail, setNewEmail] = useState('');
    const [email, setEmail] = useState('');

    useEffect(() => {
        fetchLoginAndSecuritySetting()
        if (status) {
            select('')
        }

        console.log('user: ', user)
    }, [automaticLogout]);

    function select (status) {
        setStatus(true)
        switch (status) {
            case 'never':
                setInactive(false)
                setClosed(false)
                setOne(false)
                setSeven(false)
                setThirty(false)
                setNever(true)
                setAutomaticLogout('never_logged_out_automatically')
                break;
            case 'inactive':
                setInactive(true)
                setClosed(false)
                setOne(false)
                setSeven(false)
                setThirty(false)
                setNever(false)
                setAutomaticLogout('logout_after_15_min_inactive')
                break;

            case 'closed':
                setInactive(false)
                setClosed(true)
                setOne(false)
                setSeven(false)
                setThirty(false)
                setNever(false)
                setAutomaticLogout('logout_after_closing')
                break;

            case 'one':
                setInactive(false)
                setClosed(false)
                setOne(true)
                setSeven(false)
                setThirty(false)
                setNever(false)
                setAutomaticLogout('stay_logged_in_for_1_day')
                break;

            case 'seven':
                setInactive(false)
                setClosed(false)
                setOne(false)
                setSeven(true)
                setThirty(false)
                setNever(false)
                setAutomaticLogout('stay_logged_in_for_7_days')
                break;

            case 'thirty':
                setInactive(false)
                setClosed(false)
                setOne(false)
                setSeven(false)
                setThirty(true)
                setNever(false)
                setAutomaticLogout('stay_logged_in_for_30_days')
                break;
        }

        updateLoginAndSecuritySetting()
    }
    function changeStatus (status) {
        switch (status) {
            case 'never_logged_out_automatically':
                setInactive(false)
                setClosed(false)
                setOne(false)
                setSeven(false)
                setThirty(false)
                setNever(true)
                break;
            case 'logout_after_15_min_inactive':
                setInactive(true)
                setClosed(false)
                setOne(false)
                setSeven(false)
                setThirty(false)
                setNever(false)
                break;
            case 'logout_after_closing':
                setInactive(false)
                setClosed(true)
                setOne(false)
                setSeven(false)
                setThirty(false)
                setNever(false)
                break;

            case 'stay_logged_in_for_1_day':
                setInactive(false)
                setClosed(false)
                setOne(true)
                setSeven(false)
                setThirty(false)
                setNever(false)
                break;

            case 'stay_logged_in_for_7_days':
                setInactive(false)
                setClosed(false)
                setOne(false)
                setSeven(true)
                setThirty(false)
                setNever(false)
                break;

            case 'stay_logged_in_for_30_days':
                setInactive(false)
                setClosed(false)
                setOne(false)
                setSeven(false)
                setThirty(true)
                setNever(false)
                break;
        }
    }
    function fetchLoginAndSecuritySetting () {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'automaticLogout'
        }
        axiosConfig.get('/account-api?', { params: attribute}).then(function (response) {
            let status   = response.data.account.automatic_logout;
            let oldEmail = response.data.account.old_email;

            setNewEmail(user.email)
            setOldEmail(oldEmail)
            changeStatus(status)

            setIsLoading(false);
            console.log('isLoading: ', isLoading)
        }).catch(error => {
            console.log(error);
            setIsLoading(false);
        });
    }
    function updatePassword () {
        setIsUpdate(true)

        let attribute = {
            email: user.email
        }

        axiosConfig.post('/password/email', attribute).then(response => {

            Alert.alert('Success', response.data.message)
            setIsUpdate(false)
        })
    }
    function updateEmail () {
        setIsUpdate(true)

        let attribute = {
            type: 'emailUpdate',
            email: email,
            user_id: user.id,
            old_email: user.email
        }

        try {
            axiosConfig.post('/account-api', attribute).then(response => {
                setNewEmail(response.data.account.new)
                setPendingEmail(response.data.account.pending)

                setUpdate(false)
                setIsUpdate(false)

                Alert.alert('Success', "We have emailed your verification link!")
            }).catch(err => {
                setUpdate(false)

                if (err?.response?.status === 422) {
                    const errors = err.response.data.errors
                    Object.entries(errors).map(values => this.error[values[0]] = values[1][0])

                    console.log(this.error)
                    setIsLoading(false)
                }
            })

        } catch (err) {
            setUpdate(false)
            setIsLoading(false)

            if (err?.response?.status === 422) {
                const errors = err.response.data.errors
                Object.entries(errors).map(values => this.error[values[0]] = values[1][0])

                console.log(this.error)
            }
        }
    }
    function updateLoginAndSecuritySetting() {
        setIsLoading(true);
        try {
            let attribute = {
                type: 'automatic_logout',
                value: automaticLogout,
            }
            console.log('attribute: ', attribute)
            axiosConfig.post('/account-api', attribute).then(function (response) {
                setIsLoading(false);
                alert('Successfully changed.')
            }).catch(error => {
                console.log(error);
                setIsLoading(false);
            });

        } catch (error) {
            console.log(error)
        }
    }
    function renderAccount () {
        return (
            <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {user.role === 'admin' &&
                        <Block flex style={styles.accountContainer}>
                            <Block flex>
                                <Block style={styles.accountDetails}>
                                    <AccountLinks title={route.name} role={user.role}/>
                                </Block>
                            </Block>
                        </Block>
                    }

                    <Block flex style={styles.listContainer}>

                        <Block>
                            {user.role === 'admin' ?
                                <Block>
                                    <Text bold size={13} color="#32325D" style={styles.text}>
                                        Email address
                                    </Text>

                                    {update &&
                                        <Block>
                                            <Input defaultValue={user.email} onChangeText={setEmail} placeholder={'Enter new email address'} iconContent={<Block />}/>
                                            <Text><Text onPress={()=> updateEmail()} style={{textDecorationLine: 'underline'}}>Save</Text> | <Text onPress={()=> update ? setUpdate(false) : setUpdate(true)} style={{textDecorationLine: 'underline'}}>Cancel</Text></Text>
                                        </Block>
                                    }
                                </Block>
                                :
                                <Block>
                                    <Text muted bold size={13} style={styles.text}>
                                        Email address
                                    </Text>
                                    <Text muted size={12}>Your email address is {newEmail}.</Text>
                                    <Text muted size={12}>To change your email, please talk to the Wiley Maynard Com update account owner.</Text>
                                </Block>

                            }
                            {isUpdate &&
                                <Spinner
                                    visible={isUpdate}
                                    textContent={'Please Wait...'}
                                    textStyle={{color: '#FFF'}}
                                />
                            }
                            {user.role === 'admin' &&
                                <Block>
                                    {(!isLoading && !update) &&
                                        <Text size={13}>
                                            {newEmail} <Text onPress={()=> !update ? setUpdate(true) : setUpdate(false)} size={12} style={{textDecorationLine: 'underline'}} muted>change email address</Text>
                                        </Text>
                                    }
                                </Block>
                            }

                            {pendingEmail &&
                                <Block style={{marginTop: 10}}>
                                    <Text style={styles.text} color="#32325D" size={13} muted bold>Change requested:</Text>
                                    <Text color="#36d79a"><Text color="#f5365c">from</Text> <Text style={{textDecorationLine: 'line-through',color:'#f5365c'}}>{user.email}</Text> to {pendingEmail}</Text>
                                    <Text color="#f5365c">new email address not verified <Text onPress={()=> !update ? setUpdate(true) : setUpdate(false)} color="#32325d" size={10} style={{textDecorationLine: 'underline'}}>re-send verification email</Text></Text>
                                </Block>
                            }

                            <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>
                        </Block>

                        <Block>
                            <Text bold size={13} color="#32325D" style={styles.text}>
                                Change password
                            </Text>
                            <TouchableOpacity
                                onPress={()=> updatePassword()}
                            >
                                <Text size={13} style={{ textDecorationLine: 'underline'}}>
                                    Send me an email to set a new password
                                </Text>
                            </TouchableOpacity>

                            <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>
                        </Block>

                        <Block>
                            <Text bold size={13} style={styles.text}>Automatic logout</Text>
                            {isLoading ? (
                                <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                            ) : (
                                <Block>
                                    <Block style={{ marginTop: 5}}>
                                        <Radio onChange={()=>select('inactive')} initialValue={inactive} label="Log me out after 15 minutes of inactivity" color="default" radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                    </Block>
                                    <Block style={{ marginTop: 5}}>
                                        <Radio onChange={()=>select('closed')} initialValue={closed} label="Log me out after closing vibestrive" color="default" radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                    </Block>
                                    <Block style={{ marginTop: 5}}>
                                        <Radio onChange={()=>select('one')} initialValue={one} label="Stay logged in for 1 day" color="default" radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                    </Block>
                                    <Block style={{ marginTop: 5}}>
                                        <Radio onChange={()=>select('seven')} initialValue={seven} label="Stay logged in for 7 days" color="default" radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                    </Block>
                                    <Block style={{ marginTop: 5}}>
                                        <Radio onChange={()=>select('thirty')} initialValue={thirty} label="Stay logged in for 30 days" color="default" radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                    </Block>
                                    <Block style={{ marginTop: 5}}>
                                        <Radio onChange={()=>select('never')} initialValue={never} label="Never log me out automatically" color="default" radioInnerStyle={{ backgroundColor: "#32325d" }} />
                                    </Block>
                                </Block>
                            )}

                        </Block>
                    </Block>
                </ScrollView>
            </Block>
        )
    }

    return (
        <Block flex>
            <Block flex>
                { renderAccount() }
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    profileContainer: {
        width: width,
        height: height,
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    accountContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    accountDetails: {
        // marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    divider: {
        width: "100%",
        borderWidth: 0.59,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});
