import React, {useContext, useEffect, useRef, useState} from "react";
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';

import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Image,
    ImageBackground,
    Platform, ActivityIndicator, Alert, TouchableOpacity, FlatList, View
} from "react-native";
import { Card } from '../components';
import {Block, Text, theme} from "galio-framework";
import {AntDesign, FontAwesome} from '@expo/vector-icons';
import { Button, Icon } from "../components";
import { Images, argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import Ionicons from '@expo/vector-icons/Ionicons';
import ManageEmployeesLinks from "../components/links/ManageEmployeesLinks";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import EmployeeList from "../components/manage/TeamLocation/RenderEmployeeList";
import RenderUnassignedEmployeeList from "../components/manage/TeamLocation/RenderUnassignedEmployeeList";

import SelectDropdown from "react-native-select-dropdown";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function Teams({route, navigation}) {
    const modalizeRef = useRef(null);
    const [data, setData] = useState([]);
    const [unAssignedEmployee, setUnAssignedEmployee] = useState([]);
    const [unAssignedManager, setUnAssignedManager] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isAtEndOfScrolling, setIsAtEndOfScrolling] = useState(false);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [page, setPage] = useState(1);
    const { user } = useContext(AuthContext);
    const flatListRef = useRef();

    const [activeIndex, setActiveIndex] = useState(undefined);
    const [empIndex, setEmpIndex] = useState(undefined);
    const [manIndex, setManIndex] = useState(undefined);
    const [visibleEmp, setVisibleEmp] = useState(false);
    const [visibleMan, setVisibleMan] = useState(false);
    const filterOption = [
        {
            key: 'a-z',
            name: 'Alphabetical (0-9,A-Z)'
        },
        {
            key: 'z-a',
            name: 'Alphabetical (9-0,Z-A)'
        },
        {
            key: 'newest',
            name: 'Newest first'
        }
    ]
    useEffect(() => {
        getAllTeams();
    }, [page]);
    useEffect(() => {
        if (route.params?.newDataAdded) {
            getAllTeams()
        }
    }, [route.params?.newDataAdded]);

    function getAllTeams(type, order) {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attributes = {
            'type': type??'name',
            'order' : order??'asc',
            id: user.id
        }

        axiosConfig.get(`/team-api?page=${page}`, {params: attributes}).then(response => {
            setUnAssignedEmployee(response.data.unAssignedEmployeeTeam)
            setUnAssignedManager(response.data.unAssignedManagerTeam)

            setIsLoading(false);
            if (page === 1) {
                setData(response.data.teams);
            } else {
                setData([...data, ...response.data.teams]);
            }

            if (!response.data.next_page_url) {
                setIsAtEndOfScrolling(true);
            }

            setIsLoading(false);
            setIsRefreshing(false);
            setActiveIndex(undefined);
        })
            .catch(error => {
                console.log(error);
                setIsLoading(false);
                setIsRefreshing(false);
                setActiveIndex(undefined);
            });
    }
    function sort (sortType) {
        let type  = null;
        let order = null;
        switch (sortType) {
            case 'a-z':
                type = 'name'
                order = 'asc'
                break;
            case 'z-a':
                type = 'name'
                order = 'desc'
                break;
            case 'newest':
                type = 'id'
                order = 'desc'
                break;

            default:
                type = 'name'
                order = 'asc'
        }

        getAllTeams(type, order)
    }
    function handleRefresh() {
        setPage(1);
        setIsAtEndOfScrolling(false);
        setIsRefreshing(true);
        getAllTeams();
    }
    function handleEnd() {
        setPage(page + 1);
    }
    function deleteTeam(id) {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;
        let attribute = {
            team_id: id
        }
        axiosConfig.delete(`/team-api/${id}`, { params: attribute}).then(response => {
                Alert.alert('Team was deleted.');
            handleRefresh()
            })
            .catch(error => {
                console.log(error.response);
            });
    }
    function showAlert(id) {
        Alert.alert('Delete this team?', null, [
            {
                text: 'Cancel',
                onPress: () => modalizeRef.current?.close(),
                style: 'cancel',
            },
            {
                text: 'OK',
                onPress: () => deleteTeam(id),
                style: 'default',
            },
        ]);

        setActiveIndex(undefined)
    }
    function showHideEmployeeList(visibility, visibleType) {
        if (visibleType == 'employee') {
            setVisibleEmp(visibility=!visibility)
        } else {
            setVisibleMan(visibility=!visibility)
        }
    }
    function renderTeams() {
        return (
            <Block>
                {isLoading ? (
                        <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                    ) :
                    (
                        <Block style={styles.listContainer}>
                            <Text size={13} muted>Unassigned
                                <Text style={unAssignedEmployee?.length > 0?styles.empUnderline:''} onPress={()=> showHideEmployeeList(visibleEmp, 'employee')}> {unAssignedEmployee?.length} employees</Text> |
                                <Text style={unAssignedManager?.length > 0?styles.empUnderline:''} onPress={()=> showHideEmployeeList(visibleMan, 'manager')}> {unAssignedManager?.length} manager, you</Text>
                            </Text>
                            <Block>
                                {
                                    (unAssignedEmployee.length > 0 && visibleEmp) &&
                                    <RenderUnassignedEmployeeList
                                        data={unAssignedEmployee}
                                        visibleEmp={visibleEmp}
                                        visibleMan={visibleMan}
                                        renderType={'employee'}
                                        showHideEmployeeList={showHideEmployeeList}
                                    />
                                }
                                {
                                    (unAssignedManager.length > 0 && visibleMan) &&
                                    <RenderUnassignedEmployeeList
                                        data={unAssignedManager}
                                        visibleEmp={visibleEmp}
                                        visibleMan={visibleMan}
                                        renderType={'manager'}
                                        showHideEmployeeList={showHideEmployeeList}
                                    />
                                }
                            </Block>
                            <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                <Block style={styles.divider} />
                            </Block>

                            {
                                data.map((team, index) => {
                                    return (
                                        <Block key={index}>
                                            <Block style={{ flexDirection: 'row' }}>
                                                <Block style={{ flex: 12, marginRight: 2 }}>
                                                    <Text size={13}>
                                                        {team.name + ' '}
                                                        <Text muted>
                                                            <Text onPress={()=> empIndex !== index ? setEmpIndex(index) : setEmpIndex(undefined)} style={team.team_employee_count > 0?styles.empUnderline:''}>{team.team_employee_count + ' employees'}</Text>{' | '}
                                                            <Text onPress={()=> manIndex !== index ? setManIndex(index) : setManIndex(undefined)} style={team.team_manager_count > 0?styles.empUnderline:''}>{team.team_manager_count + ' managers, you'}</Text>
                                                        </Text>
                                                    </Text>
                                                </Block>
                                                <Block style={{ flexDirection: 'row'}}>
                                                    <Ionicons
                                                        onPress={() => activeIndex !== index ? setActiveIndex(index) : setActiveIndex(undefined)}
                                                        name="ellipsis-vertical" size={14} color="#8898aa"
                                                    />

                                                    <Block>
                                                        <Menu
                                                            visible={activeIndex === index} onRequestClose={() => setActiveIndex(undefined)}
                                                            row middle space="between"
                                                        >
                                                            <MenuItem onPress={()=> gotoNewTeam(team, 'update')}>Edit</MenuItem>
                                                            <MenuItem onPress={()=> showAlert(team.id)}>Delete</MenuItem>
                                                        </Menu>
                                                    </Block>
                                                </Block>
                                            </Block>
                                            <Block>
                                                {
                                                    (team.team_employee_count > 0 && empIndex === index) &&
                                                    <EmployeeList
                                                        data={team.team_employee}
                                                        index={index}
                                                        empIndex={empIndex}
                                                        manIndex={manIndex}
                                                        setEmpIndex={setEmpIndex}
                                                        setManIndex={setManIndex}
                                                        renderType={'employee'}
                                                    />
                                                }
                                                {
                                                    (team.team_manager_count > 0 && manIndex === index) &&
                                                    <EmployeeList
                                                        data={team.team_manager}
                                                        index={index}
                                                        empIndex={empIndex}
                                                        manIndex={manIndex}
                                                        setEmpIndex={setEmpIndex}
                                                        setManIndex={setManIndex}
                                                        renderType={'manager'}
                                                    />
                                                }
                                            </Block>
                                            <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                                <Block style={styles.divider} />
                                            </Block>
                                        </Block>
                                    )
                                })
                            }

                            <Button onPress={()=> gotoNewTeam(null, 'create')} color="default" style={styles.button}>
                                Create team
                            </Button>
                        </Block>
                    )}
            </Block>
        )
    }
    function gotoNewTeam(item, type) {
        navigation.navigate('FormTeamLocation', {form: 'team', item: item, type: type});
    }
    function renderDropdown () {
        return (
            <Block style={styles.dropdownsRow}>
                <SelectDropdown
                    data={filterOption}
                    onSelect={(selectedItem, index) => {
                        console.log('selectedItem: '+  selectedItem.key + ' index: ' + index);
                        sort(selectedItem.key)
                    }}
                    defaultButtonText={filterOption[0].name}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                        return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                        return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                    search
                    searchInputStyle={styles.dropdown1searchInputStyleStyle}
                    searchPlaceHolder={'Search here'}
                    searchPlaceHolderColor={'darkgrey'}
                    renderSearchInputLeftIcon={() => {
                        return <FontAwesome name={'search'} color={'#444'} size={18} />;
                    }}
                />
                <Block style={styles.dividerDropdown} />
            </Block>
        )
    }
    return (
        <Block  style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Block  style={styles.descriptionsContainer}>
                    <Block >
                        <Block style={styles.details}>
                            <ManageEmployeesLinks
                                route={route}
                                navigation={navigation}
                            />
                        </Block>
                    </Block>
                </Block>
                <Block>
                    { renderDropdown() }
                </Block>
                { renderTeams() }
            </ScrollView>

            {/*<TouchableOpacity*/}
            {/*    style={styles.floatingButton}*/}
            {/*>*/}
            {/*    <AntDesign name="plus" size={26} color="white" />*/}
            {/*</TouchableOpacity>*/}
        </Block>
    );
}

const styles = StyleSheet.create({
    empUnderline: {
        textDecorationLine: "underline"
    },
    dropdownsRow: {
        flexDirection: 'row',
        width: '50%',
        paddingHorizontal: '5%',
        alignSelf: 'flex-end'
    },
    dropdown1BtnStyle: {
        flex: 1,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 4,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 12},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 12},
    dividerDropdown: {width: 12},
    underline: {
        textDecorationLine: 'underline',
        fontSize: 11
    },
    floatingButton: {
        width: 60,
        height: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#32325d',
        position: 'absolute',
        bottom: 20,
        right: 12,
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    button: {
        width: width - theme.SIZES.BASE * 16,
        // marginLeft: 16,
        marginTop: 16
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: width,
        height: height,
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    descriptionsContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    details: {
        marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    aboutText: {
        lineHeight: 20,
        marginBottom: 15
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});
