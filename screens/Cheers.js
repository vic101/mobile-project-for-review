import {Block, Text, theme} from "galio-framework";
import React, {useContext, useEffect, useState} from "react";
import {StyleSheet, ScrollView, FlatList} from "react-native";
import {argonTheme} from "../constants";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import footerStyle from '../screens/Styles/MessagesScreen.style';

export default function Cheers() {
    const { user } = useContext(AuthContext);
    const green = argonTheme.COLORS.BRAND_GREEN;
    const [cheers, setCheers] = useState([]);
    const [isRefreshing, setIsRefreshing] = useState(false);
    axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;

    useEffect(()=> {
        const fetchData = async () => {
            const cheers = await getCheers()

            const data = cheers.filter(cheer => !cheer.read_at);

            if (data && data.length) {
                await multiReadCheer(data);
            }
        }

        fetchData().catch(console.error);
    },[])

    const getCheers = async () => {
        try {
            const response = await axiosConfig.get('/cheer-api')
            setCheers(response.data.cheers)

            return response.data.cheers;

        } catch (error) {
            console.log(error)
        }
    }

    const multiReadCheer = async (cheers) => {
        try {
            const ids = cheers.map((cheer) => cheer.id)

            const response = await axiosConfig.post(`/cheer/multi-read-cheer-api`, {ids})
            console.log('response: ', response.data)
        } catch (error) {
            console.log(error)
        }
    }
    function handleRefresh () {

    }
    function handleEnd () {

    }

    const renderFooter = () => (
        <Block style={footerStyle.footerText}>
            <Text>No more cheers found.</Text>
        </Block>
    )
    const renderCheers = (cheer, index) => (
        <Block style={styles.group}>
            {!cheer.read_at ?
                <Block style={[styles.container, {backgroundColor: green}]}>
                    <Block row middle space="between">
                        <Text bold color="white">
                            {cheer.status}
                            {'\n'}
                            <Text color="white">from {cheer.sender}</Text>
                        </Text>
                        <Text color="white">{cheer.created_ago}</Text>
                    </Block>
                    <Block style={{marginTop: 10}}>
                        <Text color="white">
                            {cheer.comment}
                        </Text>
                    </Block>
                </Block>
                :
                <Block style={[styles.container, {backgroundColor: 'white'}]}>
                    <Block row middle space="between">
                        <Text bold color="#32325d">
                            {cheer.status}
                            {'\n'}
                            <Text muted>{cheer.sender}</Text>
                        </Text>
                        <Text muted>{cheer.created_ago}</Text>
                    </Block>

                    <Block style={{marginTop: 10}}>
                        <Text color="#32325d">{cheer.comment}</Text>
                    </Block>
                </Block>
            }
        </Block>
    )

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <ScrollView>
                <FlatList
                    keyExtractor={item => item.id.toString()}
                    data={cheers}
                    renderItem={({ item, index }) => renderCheers(item, index)}
                    refreshing={isRefreshing}
                    onRefresh={handleRefresh}
                    onEndReached={handleEnd}
                    onEndReachedThreshold={0.2}
                    ListFooterComponent={renderFooter}
                />
            </ScrollView>
        </Block>
    )
}

const styles = StyleSheet.create({
    group: {
        paddingTop: theme.SIZES.BASE * 1
    },
    container: {
        marginTop: 10,
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    spinnerTextStyle: {
        color: '#FFF'
    }
});
