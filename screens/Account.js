import React, {useContext} from "react";
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Platform,
  TouchableOpacity
} from "react-native";

import { Block, Text, theme, Radio } from "galio-framework";
import { argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import AccountLinks from "../components/links/AccountLinks";
import AccountSurveyFrequency from "../components/AccountSurveyFrequency";
import AccountLoginSecurity from "./AccountLoginSecurity";
import TrialBanner from "../components/TrialBanner";
import {AuthContext} from "../context/AuthProvider";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function account({ route, navigation }) {
    const { user, logout } = useContext(AuthContext);

  function renderAccount() {
    return (
          <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                {user.role === 'admin' && <TrialBanner navigation={navigation}/>}

                <Block flex style={styles.accountContainer}>
                    <Block flex>
                        <Block style={styles.accountDetails}>
                            <AccountLinks title={route.name} role={user.role}/>
                        </Block>
                        <TouchableOpacity
                            onPress={logout}
                        >
                            <Text>Logout</Text>
                        </TouchableOpacity>
                    </Block>
                </Block>

                {user.role === 'admin' && <AccountSurveyFrequency />}
                {(user.role === 'employee' || user.role === 'manager') && <AccountLoginSecurity />}
            </ScrollView>
          </Block>
    )
  }

    return (
        <Block flex>
            <Block flex>
                { renderAccount() }
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: width,
        height: height,
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    accountContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    accountDetails: {
        // marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});
