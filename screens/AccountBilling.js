import React, {useContext, useEffect, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Platform, Alert, ActivityIndicator
} from "react-native";

import { Block, Text, theme, Radio } from "galio-framework";
import { argonTheme } from "../constants";
import AccountLinks from "../components/links/AccountLinks";
import AccountHasBilling from "../components/accounts/HasBilling";
import AccountNoBilling from "../components/accounts/NoBilling";
import {AuthContext} from "../context/AuthProvider";
import axiosConfig from "../helpers/axiosConfig";
import {API_URL} from "../Config";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function Account({route, navigation}) {
    const { user } = useContext(AuthContext);
    const [userData, setUser] = useState([]);
    const [loading, setLoading] = useState(true);
    const [hasBilling, setHasBilling] = useState(true);
    const [cardLastFour, setCardLastFour] = useState('')
    const [cardBrand, setCardBrand] = useState('')
    const [billingDetails, setBillingDetails] = useState([])
    const [payments, setPayments] = useState([])
    const [countries, setCountries] = useState([])
    const [intent, setIntent] = useState([])

    useEffect(()=> {
        fetchUserAccountDetails()
    }, [])
    function fetchUserAccountDetails() {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.get(`/billing-details`).then(response => {
            setIntent(response.data.intent)
            setHasBilling(response.data.hasBilling)
            setCountries(response.data.countries)
            if (response.data.hasBilling) {
                setBillingDetails(response.data.billingDetails)
                setPayments(response.data?.payments)
            }

            setLoading(false)
        })
            .catch(error => {
                console.log('Error: ', error.response.data)
                alert('Error: '+ error.response.data)
            });
    }
    function handleReload() {
        fetchUserAccountDetails()
    }
    function renderAccount() {
        return (
            <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Block flex style={styles.accountContainer}>
                        <Block flex>
                            <Block style={styles.accountDetails}>
                                <AccountLinks title={route.name} role={user.role}/>
                            </Block>
                        </Block>
                    </Block>

                    {loading ? (
                        <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                    ):(
                        hasBilling ? (
                            <AccountHasBilling
                                navigation={navigation}
                                intent={intent}
                                billingDetails={billingDetails}
                                payments={payments}
                                countries={countries}
                                reload={handleReload}
                            />
                        ) : (
                            <AccountNoBilling
                                navigation={navigation}
                                reload={handleReload}
                                countries={countries}
                            />
                        )
                    )}

                </ScrollView>
            </Block>
        )
    }

    return (
        <Block flex>
            <Block flex>
                { renderAccount() }
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        // marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    accountContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    accountDetails: {
        // marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    divider: {
        width: "100%",
        borderWidth: 0.59,
        borderColor: "#E9ECEF"
    }
});
