import React, {useContext, useEffect, useRef, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView, TextInput, Alert, TouchableOpacity
} from "react-native";

import { Block, Text, theme, Radio } from "galio-framework";
import { argonTheme } from "../../constants";
import Ionicons from "@expo/vector-icons/Ionicons";
import {Button, Switch} from "../../components";
import CheckBox from "@react-native-community/checkbox";
import axiosConfig from "../../helpers/axiosConfig";
import {AuthContext} from "../../context/AuthProvider";
import Spinner from "react-native-loading-spinner-overlay";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function CancelBilling() {
    const { user, logout } = useContext(AuthContext);
    const modalizeRef = useRef(null);
    const [checkOne, setCheckOne] = useState(false);
    const [checkTwo, setCheckTwo] = useState(false);
    const [checkThree, setCheckThree] = useState(false);

    // const [checkBoxesStatus, setCheckBoxesStatus] = useState(false);
    const [status, setStatus] = useState('');
    const [reason, setReason] = useState('');

    const [onGracePeriod, setOnGracePeriod] = useState('');
    const [endsAt, setEndsAt] = useState('');
    const [onTrial, setOnTrial] = useState('');
    const [resolvedIssues, setResolvedIssues] = useState('');
    const [cheersSent, setCheersSent] = useState('');
    const [greatMoments, setGreatMoments] = useState('');
    const [surveysSent, setSurveysSent] = useState('');
    const [totalAnswers, setTotalAnswers] = useState('');
    const [minHappiness, setMinHappiness] = useState('');
    const [maxHappiness, setMaxHappiness] = useState('');
    const [unresolvedIssues, setUnresolvedIssues] = useState('');
    const [trialEndsAt, setTrialEndsAt] = useState('');
    const [trialGracePeriod, setTrialGracePeriod] = useState('');
    const [company, setCompany] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(()=> {
        const fetchData = async () => {
            await fetchSubscription()
        }

        fetchData().catch(console.error)
    },[]);

    function showAlert(status) {
        let title = "We're sorry to see you go";
        let message = null;

        if (status === 'cancel-immediately') {
            message = "Cancelling immediately won't be able resume anymore";
        }

        if (status === 'cancel') {
            message = "Are you sure you want to cancel?";
        }

        if (status === 'cancel-on-trial-period') {
            message = "Are you sure you want to cancel?";
        }


        Alert.alert(title, message, [
            {
                text: 'Cancel Subscription',
                onPress: () => cancelSubscription(status),
                style: 'cancel',
            },
            {
                text: 'Cancel',
                onPress: () => modalizeRef.current?.close(),
                style: 'default',
            },
        ]);
    }
    const checkBoxesStatus = () => {
        if ( checkOne && checkTwo && checkThree ) {
            return true
        } else {
            return false
        }
    }
    const fetchSubscription = async () => {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig('/subscription-api', {params: {id: user.id}}).then(function (response) {
            setOnGracePeriod(response.data.onGracePeriod)
            setEndsAt(response.data.ends_at)
            setOnTrial(response.data.onTrial)
            setResolvedIssues(response.data.resolvedIssues)
            setCheersSent(response.data.cheersSent)
            setGreatMoments(response.data.greatMoments)
            setSurveysSent(response.data.surveysSent)
            setTotalAnswers(response.data.totalAnswers)
            setMinHappiness(response.data.minHappiness)
            setMaxHappiness(response.data.maxHappiness)
            setUnresolvedIssues(response.data.unresolvedIssues)
            setTrialEndsAt(response.data.trialEndsAt)
            setTrialGracePeriod(response.data.trialGracePeriod)
            setCompany(response.data.companyName)
        })
    }
    const subscriptionAction = async (status) => {
        if (status == 'cancel-immediately' && ! checkBoxesStatus()) {
            Alert.alert('Error', 'Please check all agreement with checkboxes')

            return;
        } else
            try {
                if (status === 'cancel-immediately') {
                    showAlert(status);
                }

                if (status === 'cancel') {
                    showAlert(status);
                }

                if (status === 'cancel-on-trial-period') {
                    showAlert(status);
                }

                if (status === 'resume' || status === 'resume-trial') {
                    await resumeSubscription(status)
                }
            } catch (error) {
                console.log(error)
            }

    }
    const cancelSubscription = async (status) => {
        if ( (status === 'cancel' && checkBoxesStatus())
            || status === 'resume'
            || status === 'cancel-immediately'
            || status === 'cancel-on-trial-period') {

            if (status === 'cancel-immediately') {
                setLoading(true);
            }

            axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
            const response = await axiosConfig.post('/subscription-action-api?', { status: status , reason: reason, id: user.id })
                .catch(error => {
                    console.log('error: ', error.response.data)
                    setLoading(false);
                })

            if (status !== 'cancel-immediately') {
                setTrialEndsAt(response.data.subscription.trialEndsAt)
                setTrialGracePeriod(response.data.subscription.trialGracePeriod)
                setOnGracePeriod(response.data.subscription.onGracePeriod)
                setEndsAt(response.data.subscription.ends_at)

                Alert.alert('Success', 'Successfully updated');
                setLoading(false);
            }

            if (response.data.subscription.cancelNow) {
                logout()
            }
        }
        else {
            Alert.alert('Error', 'Please check all agreement with checkboxes...');
        }
    }
    const resumeSubscription = async (status) => {
        setLoading(true);

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.post('/subscription-action-api?', { status: status , reason: reason}).then(function (response) {
            if (status !== 'cancel-immediately') {

                setTrialEndsAt(response.data.subscription.trialEndsAt)
                setTrialGracePeriod(response.data.subscription.trialGracePeriod)
                setOnGracePeriod(response.data.subscription.onGracePeriod)

                setLoading(false);
                Alert.alert('Success', 'Successfully updated');
            }
        })
    }

    function renderDetails() {
        return (
            <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Block flex style={styles.container}>
                        {loading &&
                            <Spinner
                                visible={loading}
                                textContent={'Please Wait...'}
                                textStyle={styles.spinnerTextStyle}
                            />
                        }
                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 1}}>
                                <Text>😔</Text>
                            </Block>
                            <Block style={{ flex: 12, marginBottom: 10}}>
                                <Text bold color="#32325d">We are sorry that you want to stop using vibestrive.</Text>
                            </Block>
                        </Block>

                        <Block style={[styles.divider, {
                            marginTop: 14, marginBottom: 16
                        }]} />

                        {greatMoments > 0 ?
                            <Text bold color="#32325d">Thank you for <Text color="#36d79a">{greatMoments} great moments</Text> you and your employees had here: {'\n'}</Text>
                            :null
                        }
                        {resolvedIssues > 0 ?
                            <Block style={{ flexDirection: 'row' }}>
                                <Block style={{ flex: 1}}>
                                    <Ionicons name="heart" size={18} color="#36d79a"/>
                                </Block>
                                <Block style={{ flex: 12, marginBottom: 10}}>
                                    <Text>You got to the heart of {resolvedIssues} issues and resolved them.</Text>
                                </Block>
                            </Block>
                            :null
                        }
                        {maxHappiness > 0 ?
                            <Block style={{ flexDirection: 'row', marginBottom: 10 }}>
                                <Block style={{ flex: 1}}>
                                    <Ionicons name="heart" size={18} color="#36d79a"/>
                                </Block>
                                <Block style={{ flex: 12}}>
                                    <Text>Your employee <Text color="#36d79a">happiness</Text> went <Text color="#36d79a">from {minHappiness} to {maxHappiness}.</Text></Text>
                                </Block>
                            </Block>
                            :null
                        }
                        {cheersSent > 0 ?
                            <Block style={{ flexDirection: 'row', marginBottom: 10 }}>
                                <Block style={{ flex: 1}}>
                                    <Ionicons name="heart" size={18} color="#36d79a"/>
                                </Block>
                                <Block style={{ flex: 12}}>
                                    <Text> Your employee sent each other <Text color="#36d79a">{cheersSent} cheers and motivational messages.</Text></Text>
                                </Block>
                            </Block>
                            :null
                        }

                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 1}}>
                                <Ionicons name="heart" size={18} color="#36d79a"/>
                            </Block>
                            <Block style={{ flex: 12}}>
                                <Text>And <Text color="#36d79a">every time an employee opened up</Text> to tell you <Text color="#36d79a">how to make {company} update a better place.</Text></Text>
                            </Block>
                        </Block>

                        <Block style={[styles.divider, {marginTop: 14, marginBottom: 16}]} />

                        {!onGracePeriod ?
                            <Block>
                                <Text bold color="#32325d">What makes you want to stop using vibestrive?</Text>
                                <TextInput
                                    multiline
                                    placeholder="Your honest feedback is highly appreciated ..."
                                    style={styles.feedback}
                                    onChangeText={setReason}
                                />
                                <Block style={{ flexDirection: 'row', marginBottom: 10 }}>
                                    <Block style={{ flex: 1}}>
                                        <CheckBox
                                            tintColors={{ true: '#f5365c', false: 'black' }}
                                            value={checkOne}
                                            onChange={()=> checkOne ? setCheckOne(false) : setCheckOne(true)}
                                        />
                                    </Block>
                                    <Block style={{ flex: 10}}>
                                        <Text>I understand that <Text bold color="#f5365c">I and my employees lose access</Text> to
                                            <TouchableOpacity
                                                style={styles.chatIcon}
                                            >
                                                <Text bold size={12} color="#ffffff">{unresolvedIssues}</Text>
                                            </TouchableOpacity>
                                            unresolved conversations
                                        </Text>
                                    </Block>
                                </Block>
                                <Block style={{ flexDirection: 'row', marginBottom: 10 }}>
                                    <Block style={{ flex: 1}}>
                                        <CheckBox
                                            tintColors={{ true: '#f5365c', false: 'black' }}
                                            value={checkTwo}
                                            onChange={()=> checkTwo ? setCheckTwo(false) : setCheckTwo(true)}
                                        />
                                    </Block>
                                    <Block style={{ flex: 10}}>
                                        <Text>I understand that <Text bold color="#f5365c">I lose access</Text> to <Text bold>{surveysSent} survey results</Text> with a total of <Text bold>{totalAnswers}</Text></Text>
                                    </Block>
                                </Block>
                                <Block style={{ flexDirection: 'row' }}>
                                    <Block style={{ flex: 1}}>
                                        <CheckBox
                                            tintColors={{ true: '#f5365c', false: 'black' }}
                                            value={checkThree}
                                            onChange={()=> checkThree ? setCheckThree(false) : setCheckThree(true)}
                                        />
                                    </Block>
                                    <Block style={{ flex: 10}}>
                                        <Text>I understand that <Text bold color="#f5365c">my employees lose access</Text> to <Text bold>{cheersSent} cheers & motivational messages they sent each other</Text></Text>
                                    </Block>
                                </Block>
                            </Block>
                            :null
                        }

                        <Block style={[styles.divider, {marginTop: 14, marginBottom: 16}]} />

                        {onGracePeriod ?
                            <Block>
                                <Text>Your current billing period ends on {endsAt}.</Text>
                            </Block>
                            :null
                        }

                        {trialGracePeriod ?
                            <Text>Your trial period ends on { trialEndsAt }.</Text>
                            :null
                        }

                        {(!onGracePeriod && !onTrial) ?
                            <Block>
                                <Button
                                    style={styles.buttonEnabled}
                                    variant="primary"
                                    onPress={()=> subscriptionAction('cancel')}
                                >
                                    Cancel my account at the end of my current billing period
                                </Button>
                            </Block>
                            :null
                        }
                        {(onTrial && !trialGracePeriod) ?
                            <Block>
                                <Button
                                    style={styles.buttonEnabled}
                                    variant="primary"
                                    onPress={()=> subscriptionAction('cancel-on-trial-period')}
                                >
                                    Cancel my account at the end of my trial period
                                </Button>

                                <TouchableOpacity
                                    onPress={()=> subscriptionAction('cancel')}
                                >
                                    <Text muted style={{textDecorationLine: 'underline', marginTop: 10}}>Cancel my trial immediately</Text>
                                </TouchableOpacity>
                            </Block>
                            :null
                        }

                        {(onTrial && trialGracePeriod) ?
                            <Block>
                                <Button
                                    style={{marginLeft: 0}}
                                    color="default"
                                    onPress={()=> subscriptionAction('resume-trial')}
                                >
                                    Resume trial
                                </Button>
                            </Block>
                            :null
                        }

                        {(!onGracePeriod && !onTrial) ?
                            <Block style={{marginTop: 10}}>
                                <TouchableOpacity
                                    onPress={()=> subscriptionAction('cancel-immediately')}
                                >
                                    <Text muted style={{textDecorationLine: 'underline'}}>Cancel my account immediately (without refund)</Text>
                                </TouchableOpacity>
                            </Block>
                            :null
                        }
                    </Block>
                </ScrollView>
            </Block>
        )
    }

    return (
        <Block flex>
            <Block flex>
                { renderDetails() }
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    chatIcon: {
        width: 40,
        height: 20,
        borderRadius: 50,
        backgroundColor: '#f5365c',
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonEnabled: {
        color: '#f5365c',
        marginBottom: 2,
        marginLeft: 0,
        width: width - theme.SIZES.BASE * 4,
        borderRadius: 6
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
    },
    text: {
        lineHeight: 30
    },
    feedback: {
        borderRadius: 4,
        shadowOpacity: 0.05,
        backgroundColor: '#edeff2',
        height: 100,
        minHeight: 45,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 10,
        width: "100%",
        textAlignVertical: 'top',
        marginTop: 15,
        marginBottom: 10
    },
    container: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    divider: {
        width: "100%",
        borderWidth: 0.59,
        borderColor: "#E9ECEF"
    }
});
