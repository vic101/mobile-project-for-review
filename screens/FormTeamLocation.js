import React, {useContext, useEffect, useState} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    TextInput,
    ActivityIndicator,
    Alert,
} from 'react-native';
import { AuthContext } from '../context/AuthProvider';
import { Images, argonTheme } from "../constants";
import axiosConfig from '../helpers/axiosConfig';
import {Block, theme} from "galio-framework";
import {Input} from "../components";

export default function FormTeamLocation({ route, navigation }) {
    const [formData, setFormData] = useState(route.params.type == 'update'?route.params.item.name:'');

    const [word, setWord] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const { user } = useContext(AuthContext);
    const [error, setError] = useState(null);

    useEffect(() => {
        setWord(route.params.type == 'update'?'Update':'Create');
    });

    function sendForm() {
        if (formData.length === 0) {
            Alert.alert('Error', 'Please enter a ' + route.params.form + ' name');
            return;
        }

        setIsLoading(true);
        let attribute = null
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        if (route.params.type == 'update') {
            attribute = {
                name: formData
            }
        } else {
            if (route.params.form == 'team') {
                attribute = {
                    teams: [{team: formData}]
                }
            } else {
                attribute = {
                    locations: [{location: formData}]
                }
            }
        }

        if (route.params.type == 'update') {
            axiosConfig.patch(setUrl(route.params), attribute).then(response => {
                if (route.params.form == 'team') {
                    navigation.navigate('Teams', {
                        newDataAdded: response.data,
                    });
                } else {
                    navigation.navigate('Locations', {
                        newDataAdded: response.data,
                    });
                }

                setIsLoading(false);
            })
                .catch(error => {
                    const key = Object.keys(error.response.data.errors)[0];
                    setError(error.response.data.errors[key][0]);
                    setIsLoading(false);
                });
        } else {
            axiosConfig.post(setUrl(route.params), attribute).then(response => {
                if (route.params.form == 'team') {
                    navigation.navigate('Teams', {
                        newDataAdded: response.data,
                    });
                } else {
                    navigation.navigate('Locations', {
                        newDataAdded: response.data,
                    });
                }

                setIsLoading(false);
            })
                .catch(error => {
                    const key = Object.keys(error.response.data.errors)[0];
                    setError(error.response.data.errors[key][0]);
                    setIsLoading(false);
                });
        }
    }
    function setUrl(parameter) {
        if (parameter.type == 'update') {
            return `/${route.params.form}-api/${parameter.item.id}`
        } else {
            return `/${route.params.form}-api`
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.createTeamButtonContainer}>
                <Text style={styles.textGray}>
                    {word} Your {route.params.form.charAt(0).toUpperCase() + route.params.form.slice(1)}
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    {isLoading && (
                        <ActivityIndicator
                            size="small"
                            color="gray"
                            style={{ marginRight: 8 }}
                        />
                    )}
                    <TouchableOpacity
                        style={styles.createTeamButton}
                        onPress={() => sendForm()}
                        disabled={isLoading}
                    >
                        <Text style={styles.createTeamButtonText}>{word} {route.params.form.charAt(0).toUpperCase()+route.params.form.slice(1)}</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                <Input onChangeText={setFormData} value={formData} right placeholder={'Your ' + route.params.form + ' name'} iconContent={<Block />} />
            </Block>
        </View>
    );
}

const styles = StyleSheet.create({
    textGray: {
        color: 'gray',
    },
    textRed: {
        color: 'red',
    },
    ml4: {
        marginLeft: 16,
    },
    container: {
        flex: 1,
        backgroundColor: argonTheme.COLORS.BACKGROUND,
        paddingVertical: 12,
        paddingHorizontal: 10,
    },
    createTeamButtonContainer: {
        paddingVertical: 4,
        paddingHorizontal: 6,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    createTeamButton: {
        backgroundColor: '#32325d',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 24,
    },
    createTeamButtonText: {
        color: 'white',
        fontWeight: 'bold',
    },
    tweetBoxContainer: {
        flexDirection: 'row',
        paddingTop: 10,
    },
    avatar: {
        width: 42,
        height: 42,
        marginRight: 8,
        marginTop: 10,
        borderRadius: 21,
    },
    input: {
        flex: 1,
        fontSize: 18,
        lineHeight: 28,
        padding: 10,
    },
});
