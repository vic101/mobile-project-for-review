import React, {useEffect, useState, useContext} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Image,
    ImageBackground,
    Platform, TouchableOpacity
} from "react-native";
import { Card } from '../components';
import { Block, Text, theme } from "galio-framework";

import { Button } from "../components";
import { Images, argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import TrialBanner from "../components/TrialBanner";
import Ionicons from "@expo/vector-icons/Ionicons";

import {AuthContext} from "../context/AuthProvider";
import axiosConfig from "../helpers/axiosConfig";
import Spinner from "react-native-loading-spinner-overlay";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function vibeMonitor({ navigation }) {
    const [loading, setLoading] = useState(true);
    const { user } = useContext(AuthContext);
    const [selectedIndustry, setSelectedIndustry] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState([]);
    const [selectedLocation, setSelectedLocation] = useState([]);
    const [selectedTeam, setSelectedTeam] = useState([]);

    const [selectedCustomer, setSelectedCustomer] = useState('');
    const [categories, setCategories] = useState([]);
    const [locations, setLocations] = useState([]);
    const [teams, setTeams] = useState([]);
    const [categoryStatus, setCategoryStatus] = useState('');
    const [allCategoryRating, setAllCategoryRating] = useState(0);
    const [allCategoryGraphColor, setAllCategoryGraphColor] = useState([]);
    const [allCategoryGraphPercentage, setAllCategoryGraphPercentage] = useState([]);
    const [categoryRating, setCategoryRating] = useState([]);
    const [subCategoryRating, setSubCategoryRating] = useState([]);
    const [graphPercentage, setGraphPercentage] = useState([]);
    const [graphColor, setGraphColor] = useState([]);
    const [happinessColor, setHappinessColor] = useState('');
    const [categoryMessagesCounters, setCategoryMessagesCounters] = useState([]);
    const [firstQuestionMonthlySurvey, setFirstQuestionMonthlySurvey] = useState([]);


    useEffect(()=> {
        getSurveyMonitor()
    },[])

    function getSurveyMonitor () {
        let attributes = {
            location: selectedLocation,
            team: selectedTeam,
            industry: selectedIndustry,
            country: selectedCountry
        }

        let url = ''
        if (user.role === 'admin' || user.role === 'manager') {
            url = '/monitor-api?'
        } else {
            url = '/monitor-api/'+selectedCustomer
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.get(url, { params: attributes }).then(response => {
            setLocations(response.data.locations)
            setTeams(response.data.teams)
            setCategoryStatus(response.data.rawData.status)

            setAllCategoryRating(response.data.rawData.allCategoryRating)
            setAllCategoryGraphColor(response.data.rawData.allCategoryGraphColor)
            setAllCategoryGraphPercentage(response.data.rawData.allCategoryGraphPercentage)

            setCategories(response.data.rawData.categories)
            setCategoryRating(response.data.rawData.categoryRating)

            setSubCategoryRating(response.data.rawData.subCategoryRating)

            setGraphPercentage(response.data.rawData.graphPercentage)
            setGraphColor(response.data.rawData.graphColor)
            setHappinessColor(response.data.rawData.happinessColor)

            setCategoryMessagesCounters(response.data.rawData.categoryMessagesCounters)
            setFirstQuestionMonthlySurvey(response.data.rawData.firstQuestionMonthlySurvey)

            setLoading(false)
        }).catch(error => {
            setLoading(false)
            console.log(error)
        })

    }

    const backGroundColor = (color) => {
        switch (color) {
            case 'greendeep':
                return styles.greendeep;
                break;

            case 'darkblue':
                return styles.darkblue;
                break;

            case 'red':
                return styles.red;
                break;
            case 'gray':
                return styles.gray;
                break;
            default:
                return styles.gray;
                break;
        }
    }
    function graphPercentWitch (percent) {
        // Formula for getting graph values base from the given percentages
        // 5  = 100%
        // 30 = 0%
        // Y= (120-x)/4, where Y is graph the value and x is the given percentage
        if (percent <= 30) {console.log(percent + ' = ' + (120 - (percent))/4);}

        return (120 - (percent))/4;
    }
  function renderGraph (backColor, percent, txtColor, subCategory) {
        let showInside  = (subCategory.length <= 14 && percent >= 50 ||
                           subCategory.length > 14 && percent > 50) && subCategory;

        let showOutside = (subCategory.length >= 14 && percent <= 49 ||
                           subCategory.length > 14 && percent <= 50  ||
                           subCategory.length <= 14 && percent <= 50) && subCategory;

    return (
          <Block flex>
              <Block style={styles.container}>
                  <Block style={[styles.barGraph, backGroundColor(backColor) , {
                      width: width - theme.SIZES.BASE * graphPercentWitch(percent),
                  }]}>
                      <Block style={{ flex: 6 }}>
                          <Text bold style={[styles.textLeft, {color: txtColor}]}>{showInside} </Text>
                      </Block>
                      <Block style={{ flex: 4}}>
                          <Text bold style={[styles.textRight, {color: txtColor}]}>
                              {backColor === 'gray' ? <Ionicons name="hourglass-outline" size={14} color="#8898aa"/> : percent + '%'}
                          </Text>
                      </Block>
                  </Block>
                  <Block style={styles.message}><Text size={12} style={{color: backColor}}>{showOutside}</Text></Block>
              </Block>
          </Block>
    )
  }

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block flex>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <TrialBanner navigation={navigation}/>
                    {loading ?
                        <Block style={styles.surveyDetailsContainer}>
                            <Text style={{textAlign: 'center'}} bold muted size={14}>Processing data please wait...</Text>
                            <Spinner
                                visible={true}
                                textContent={'Please Wait...'}
                                textStyle={styles.spinnerTextStyle}
                            />
                        </Block>
                        :
                        <Block>
                            <Block flex style={styles.surveyDetailsContainer}>
                                <Block style={{marginBottom: 20}}>
                                    <Text bold color="#32325d">{allCategoryRating}% Current Overall Happiness</Text>
                                </Block>

                                {categories.map((category, index) => {
                                    return (
                                        <Block key={index}>
                                            {renderGraph(
                                                categoryRating[category.id] < 1 ? 'gray' :
                                                    allCategoryGraphColor[category.id],
                                                allCategoryGraphPercentage[category.id],
                                                categoryRating[category.id] < 1 ? '#8898aa' :'white',
                                                category.name
                                            )}
                                        </Block>
                                    )
                                })}

                                {/* note that the base size of 0% must update to 35 so values less than 35 will show for graph changes */}
                                {/*{ renderGraph('red', 35, 'white', 'Category 4') }*/}
                            </Block>

                            {categories.map((category, index) => {
                                return (
                                    <Block flex style={styles.surveyDetailsContainer} key={index}>
                                        <Block style={{marginBottom: 20}}>
                                            <Text bold color="#32325d">{categoryRating[category.id]}% {category.name}</Text>
                                        </Block>

                                        {category.sub_categories.map((subCategory, subCategoryIndex) => {
                                            return (
                                                <Block key={subCategoryIndex}>
                                                    {
                                                        renderGraph(
                                                            subCategoryRating[category.id][subCategory.id]==='in-progress'?'gray':graphColor[subCategory.id],
                                                            subCategoryRating[category.id][subCategory.id]!=='in-progress'?subCategoryRating[category.id][subCategory.id]:100,
                                                            subCategoryRating[category.id][subCategory.id]==='in-progress'?'#8898aa':'white', subCategory.name
                                                        )
                                                    }
                                                </Block>
                                            )
                                        })}

                                        {/*{ renderGraph('darkblue', 77, 'white', 'Komm mit Vorgesetzten') }*/}
                                        {/*{ renderGraph('gray', 100, '#8898aa', 'Sub category 5') }*/}
                                    </Block>
                                )
                            })}
                        </Block>
                    }
                </ScrollView>
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    message: {
        minWidth: 5,
        maxWidth: 200,
        justifyContent: 'center',
        marginLeft: 5,
    },
    surveyDetailsContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        maxWidth: 500
    },
    container: {
        flex: 1,
        flexDirection: "row",
        alignContent: "space-between",
        maxWidth: 500,
        marginBottom: 10
    },
    textLeft: {
        fontSize: 11,
        textAlign: 'left'
    },
    textRight: {
        fontSize: 11,
        textAlign: 'right'
    },
    barGraph: {
        flexDirection: 'row',
        padding: 8,
        borderRadius: 5,
        maxWidth: 400,
    },
    white: {
        color: 'white'
    },
    black: {
        color: '#8898aa'
    },
    greendeep: {
        backgroundColor: '#36d79a'
    },
    darkblue: {
        backgroundColor: '#32325d'
    },
    red: {
        backgroundColor: '#f5365c'
    },
    gray: {
        backgroundColor: '#edeff2'
    },
})
