import React, {useContext, useEffect, useState} from "react";
import {
    StyleSheet,
    Dimensions,
    ScrollView,
    Platform, RefreshControl
} from "react-native";

import {Block, Text, theme} from "galio-framework";
import CheckBox from "@react-native-community/checkbox";
import { argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import AccountLinks from "../components/links/AccountLinks";
import AccountSurveyFrequency from "../components/AccountSurveyFrequency";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import {func} from "prop-types";
import SelectDropdownMenu from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";
import Spinner from "react-native-loading-spinner-overlay";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function notification({route, navigation}){
    const [refreshing, setRefreshing] = React.useState(false);
    const { user } = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(true);
    const [isSurveySent, setSurveySent] = useState(false);
    const [isSurveyResultsAvailable, setSurveyResultsAvailable] = useState(false);
    const [isUpcomingSurveys, setUpcomingSurveys] = useState(false);
    const [isNewMessageChecks, setNewMessageChecks] = useState(false);
    const [newMessage, setNewMessage] = useState('instant');
    const [isNewInvoice, setNewInvoice] = useState(false);
    const [isNewCheers, setNewCheers] = useState(false);
    const [isEndSoonSurveys, setEndSoonSurveys] = useState(false);
    const [isNewSurveys, setNewSurveys] = useState(false);

    const newMessagesOptions = [
        {
            key: 'every_10_minutes',
            name: 'every 10 minutes'
        },
        {
            key: '1x_per_hour',
            name: '1x per hour'
        },
        {
            key: '1x_per_day',
            name: '1x per day'
        }
    ]

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getSettings()

        wait(2000).then(() => {
                setRefreshing(false)
            }
        );
    }, []);

    useEffect(()=> {
        getSettings()
    },[]);

    function getSettings() {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;
        let attributes = {
            setting: 'notification'
        }

        axiosConfig.get(`/account/get-settings-api`, { params: attributes}).then(response => {
            let data = response.data.account.notifications;
            setNewMessage(data?.new_message ? data?.new_message : 'instant')
            setNewMessageChecks(data?.new_message ? true : false)

            switch (user.role) {
                case 'employee':
                    setEmployeeData(data)
                    break;
                case 'manager':
                    setManagerData(data)
                    break;
                case 'admin':
                    setAdminData(data)
                    break;
            }
        }).catch(error => {
            console.log(error.response.data);
        });
    }
    function setEmployeeData(data) {
        setEndSoonSurveys(data?.surveys_end_soon)
        setNewCheers(data?.new_cheer)
        setNewSurveys(data?.new_survey)
    }
    function setManagerData(data) {
        setNewMessageChecks(data?.new_message ? true : false)
        setSurveyResultsAvailable(data?.survey_results_available ?? isSurveyResultsAvailable)
    }
    function setAdminData(data) {
        setSurveySent(data?.survey_sent ?? isSurveySent)
        setSurveyResultsAvailable(data?.survey_results_available ?? isSurveyResultsAvailable)
        setUpcomingSurveys(data?.upcoming_surveys ?? setUpcomingSurveys)
        setNewInvoice(data?.new_invoice ?? isNewInvoice)
    }


    function changeHandler(value, type) {
        switch (user.role) {
            case 'admin':
                switch (type) {
                    case 'survey_sent':
                        setSurveySent(value = !value)
                        break;
                    case 'survey_results_available':
                        setSurveyResultsAvailable(value = !value);
                        break;
                    case 'upcoming_surveys':
                        setUpcomingSurveys(value = !value);
                        break;
                    case 'new_invoice':
                        setNewInvoice(value = !value);
                        break;
                    case 'new_message_checks':
                        setNewMessageChecks(value = !value)
                        type = 'new_message'
                        break;
                }
                break;

            case 'employee':
                switch (type) {
                    case 'new_survey':
                        setNewSurveys(value = !value)
                        break;
                    case 'surveys_end_soon':
                        setEndSoonSurveys(value = !value);
                        break;
                    case 'new_message':
                        setNewMessage(value = !value);
                        break;
                    case 'new_cheer':
                        setNewCheers(value = !value);
                        break;
                }
                break;
            case 'manager':
                switch (type) {
                    case 'new_survey':
                        setNewSurveys(value = !value)
                        break;
                    case 'new_message':
                        setNewMessage(value = !value);
                        break;
                    case 'survey_results_available':
                        setSurveyResultsAvailable(value = !value);
                        break;
                }
                break;
        }

        const attributes = {
            type: type,
            value: value,
        }

        saveNotifications(attributes)
    }
    function saveNotifications(attributes) {
        try {
            axiosConfig.defaults.headers.common[
                'Authorization'
                ] = `Bearer ${user.token}`;

            axiosConfig.post('/account-api', attributes).then(response => {
                getSettings()
                alert('Successfully changed.')
            }).catch(error => {
                console.log(error.response.data);
            });
        } catch (error) {
            console.log(error)
        }
    }

    function renderAdminNotification () {
        return (
            <Block>
                <Block style={styles.wrapper}>
                    <CheckBox
                        disabled={false}
                        value={isSurveySent}
                        onChange={()=> changeHandler(isSurveySent, 'survey_sent')}
                    />

                    <Text style={styles.text}>
                        Survey sent
                    </Text>
                </Block>
                <Block style={styles.wrapper}>
                    <CheckBox
                        value={isSurveyResultsAvailable}
                        onChange={()=> changeHandler(isSurveyResultsAvailable, 'survey_results_available')}
                    />
                    <Text style={styles.text}>
                        Survey results available
                        <Text muted size={10}> (after more than 5 replies)</Text>
                    </Text>
                </Block>
                <Block style={styles.wrapper}>
                    <CheckBox
                        value={isUpcomingSurveys}
                        onChange={()=> changeHandler(isUpcomingSurveys, 'upcoming_surveys')}
                    />
                    <Text style={styles.text}>
                        Upcoming surveys
                    </Text>
                </Block>
                <Block style={styles.wrapper}>
                    <CheckBox
                        value={isNewMessageChecks}
                        onChange={()=> changeHandler(isNewMessageChecks, 'new_message_checks')}
                    />
                    <Text style={styles.text}>
                        New messages
                    </Text>
                    {renderDropDown()}
                </Block>
                <Block style={styles.wrapper}>
                    <CheckBox
                        value={isNewInvoice}
                        onChange={()=> changeHandler(isNewInvoice, 'new_invoice')}
                    />
                    <Text style={styles.text}>
                        New invoices
                    </Text>
                </Block>
            </Block>
        )
    }
    function renderDropDown() {
        return (
            <Block style={styles.dropdownsRow}>
                <SelectDropdownMenu
                    data={newMessagesOptions}
                    onSelect={(selectedItem, index) => {
                        console.log(selectedItem, index);
                        changeHandler(selectedItem.key, 'new_message_checks')
                    }}
                    defaultButtonText={newMessage}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                        return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                        return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                />
            </Block>
        )
    }
    function renderEmployeeNotification() {
        return (
            <Block>
                {user.role === 'manager' &&
                    <Block style={styles.wrapper}>
                        <CheckBox
                            value={isSurveyResultsAvailable}
                            onChange={()=> changeHandler(isSurveyResultsAvailable, 'survey_results_available')}
                        />
                        <Text style={styles.text}>
                            Survey results available
                            <Text muted size={10}> (after more than 5 replies)</Text>
                        </Text>
                    </Block>
                }

                {user.role === 'employee' &&
                    <Block>
                        <Block style={styles.wrapper}>
                            <CheckBox
                                disabled={true}
                                value={isNewSurveys}
                                onChange={() => changeHandler(isNewSurveys, 'new_survey')}
                            />
                            <Text style={styles.text} muted>
                                New surveys <Text size={10}>required</Text>
                            </Text>
                        </Block>
                        <Block style={styles.wrapper}>
                        <CheckBox
                        value={isEndSoonSurveys}
                        onChange={()=> changeHandler(isEndSoonSurveys, 'surveys_end_soon')}
                        />
                        <Text style={styles.text}>
                        Surveys that end soon (and that I haven't finished)
                        </Text>
                        </Block>
                    </Block>
                }

                <Block style={styles.wrapper}>
                    <CheckBox
                        value={isNewMessageChecks}
                        onChange={()=> changeHandler(isNewMessageChecks, 'new_message')}
                    />
                    <Text style={styles.text}>
                        New messages
                    </Text>
                    {renderDropDown()}
                </Block>

                {user.role === 'employee' &&
                    <Block style={styles.wrapper}>
                        <CheckBox
                            value={isNewCheers}
                            onChange={()=> changeHandler(isNewCheers, 'new_cheer')}
                        />
                        <Text style={styles.text}>
                            New cheers
                        </Text>
                    </Block>
                }

            </Block>
        )
    }
    function renderAccount () {
        return (
            <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />
                    }
                >
                    {refreshing &&
                        <Spinner
                            visible={refreshing}
                            textContent={'Please Wait...'}
                            textStyle={styles.spinnerTextStyle}
                        />
                    }
                    <Block flex style={styles.accountContainer}>
                        <Block flex>
                            <Block style={styles.accountDetails}>
                                <AccountLinks title={route.name} role={user.role}/>
                            </Block>
                        </Block>
                    </Block>

                    <Block flex style={styles.listContainer}>
                        <Block style={{ marginLeft: 6, marginBottom: 8 }}>
                            <Text>Receive an email notification about</Text>
                        </Block>

                        {user.role === 'admin' ? renderAdminNotification() : renderEmployeeNotification()}

                    </Block>
                </ScrollView>
            </Block>
        )
    }

    return (
        <Block flex>
            <Block flex>
                { renderAccount() }
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    dropdownsRow: {flexDirection: 'row', width: '50%', paddingLeft: '2%'},
    dropdown1BtnStyle: {
        flex: 1,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 8,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 12},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left'},
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        // paddingVertical: 15,
    },
    profile: {
        marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
        // marginBottom: -HeaderHeight * 2,
        flex: 1
    },
    profileContainer: {
        width: width,
        height: height,
        padding: 0,
        zIndex: 1
    },
    profileBackground: {
        width: width,
        height: height / 2
    },
    accountContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    info: {
        paddingHorizontal: 40
    },
    avatarContainer: {
        position: "relative",
        marginTop: -80
    },
    avatar: {
        width: 124,
        height: 124,
        borderRadius: 62,
        borderWidth: 0
    },
    accountDetails: {
        // marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    divider: {
        width: "90%",
        borderWidth: 1,
        borderColor: "#E9ECEF"
    },
    thumb: {
        borderRadius: 4,
        marginVertical: 4,
        alignSelf: "center",
        width: thumbMeasure,
        height: thumbMeasure
    }
});
