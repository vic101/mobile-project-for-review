import {argonTheme} from "../../../constants";
import {View, Dimensions, RefreshControl, ScrollView, StyleSheet, TextInput, Alert} from "react-native";
import React, {useContext, useEffect, useState} from "react";
import {Block, Text, theme} from "galio-framework";
import SendCheers from "../../../components/surveys/Employee/SendCheers";
import QuestionAndAnswer from "../../../components/surveys/Employee/QandA";
import Streak from "../../../components/surveys/Employee/Streak";
import axiosConfig from "../../../helpers/axiosConfig";
import {AuthContext} from "../../../context/AuthProvider";
import Spinner from "react-native-loading-spinner-overlay";

export default function CurrentSurveyEmployee() {
    const { user } = useContext(AuthContext);
    const [loading, setLoading] = useState(true);
    const [sending, setSending] = useState(false);
    const [next, setNext] = useState(false);
    const [total, setTotal] = useState(null);
    const [lastAnswer, setLastAnswer] = useState(false);
    const [last, setLast] = useState(false);
    const [lastSave, setLastSave] = useState(false);
    const [answerCount, setAnswerCount] = useState('');
    const [position, setPosition] = useState('');
    const [firstSurvey, setFirstSurvey] = useState('');
    const [surveyId, setSurveyId] = useState('');
    const [questionText, setQuestionText] = useState('');
    const [currentQuestionId, setCurrentQuestionId] = useState('');
    const [nextQuestionId, setNextQuestionId] = useState('');
    const [isCustom, setIsCustom] = useState('');
    const [hasLast, setHasLast] = useState('');
    const [answers, setAnswers] = useState([]);
    const [currentQuestion, setCurrentQuestion] = useState([]);
    const [nextSurveyMessage, setNextSurveyMessage] = useState([]);
    const [streaks, setStreaks] = useState([]);
    const [cheerCount, setCheerCount] = useState('');

    const [radioSelected, setRadioSelected] = useState(false);
    const [circleSelected, setCircleSelected] = useState(false);

    const [answerId, setAnswerId] = useState('');
    const [questionId, setQuestionId] = useState('');
    const [skip, setSkip] = useState(false);
    const [message, setMessage] = useState('');
    const [errors, setErrors] = useState([]);
    const [timer, setTimer] = useState('')
    const [userId, setUserId] = useState('');
    const [cheerComment, setCheerComment] = useState('');
    const [cheer, setCheer] = useState('');


    useEffect(()=> {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        const fetchData = async () => {
            await fetchCurrentSurvey()
            await getSurveyStreaks()
            await fetchCheerStreak()
        }

        fetchData().catch(console.error);
    },[])

    function selectAnswer (answer,iWhich,index) {

        // this.answerIndex = index
        if (iWhich != 'radio') {
            setCircleSelected(true)
        } else {
            setRadioSelected(true)
        }

        setAnswerId(answer.id)
        setQuestionId(answer.question_id)
        setSkip(false)
    }
    const fetchCurrentSurvey = async ()=> {
        try {
            const response = await axiosConfig.get('/current-survey-employee-api').catch(error => {
                console.log('Error: ', error.response)
            })

            setAnswerCount(response.data.usersAnswer);

            if (response.data.total !== null && response.data.total !== 0) {
                setLast(response.data.last)

                if (!response.data.last) {
                    setTotal(response.data.total)
                    setPosition(response.data.position)
                    setFirstSurvey(response.data.firstSurvey)
                    setSurveyId(response.data.surveyId)
                    setQuestionText(response.data.question.text)
                    setCurrentQuestionId(response.data.question.id)
                    setNextQuestionId(response.data.nextQuestionId)
                    setIsCustom(response.data.question.custom)

                    response.data.position == response.data.total ? (setLastSave(true)) : (setLastSave(false));

                    if (! response.data.has_last && response.data.firstSurvey) {
                        setAnswers(response.data.question.answers.reverse())
                    } else {
                        setCurrentQuestion(response.data.question.answers)
                    }

                }
            } else {
                setTotal(null)
                setNextSurveyMessage(response.data.nextSurveyMessage)
            }

            setLoading(false)
        } catch (error) {
            console.error(error)
            setLoading(false)
        }
    }
    const saveAnswer = async (skip) => {
        try {
            setNext(true)

            let attributes = {
                questionId: currentQuestionId,
                answerId: skip ? '' : answerId,
                surveyId: surveyId,
                nextQuestionIndex: position,
                skip: skip,
                last: lastSave,
                message: message
            }

            // console.log('attributes: ', attributes)

            const response = await axiosConfig.post('/user-answer-api', attributes).catch(error => {
                console.log('Error: ', error.response)
            })

            setLast(response.data.userAnswer.last)
            setMessage(null)
            // this.clear()
            // this.clearError()
        } catch (error) {
            if (error?.response?.status == 422) {
                // this.hasError = true
                // this.errors   = []
                // const errors  = error.response.data.errors

                // Object.entries(errors).map(values => this.errors[values[0]] = values[1][0])
            }
        }

    }
    const nextQuestion = async (skip)=> {
        setSending(true)

        await saveAnswer(skip)
        setCurrentQuestion([])

        let attribute = {
            nextQuestionId: nextQuestionId
        }

        axiosConfig.get('/current-survey-employee-api?', {params: attribute}).then((response) => {
            if (response.data.question !== null) {
                setCurrentQuestion(response.data.question.answers)
                setTotal(response.data.total)
                setPosition(response.data.position)
                setQuestionText(response.data.question.text)
                setNextQuestionId(response.data.nextQuestionId)
                setCurrentQuestionId(response.data.question.id)
                setIsCustom(response.data.question.custom)

                if (response.data.position == response.data.total) {
                    setLastSave(true)
                } else {
                    setLastSave(false)
                }

                setRadioSelected(false)
                setCircleSelected(false)
            }

            setNext(false)
            setSending(false)
        }).catch(error => {
            console.log('error: ', error.response)
            setSending(false)
        });

        await getSurveyStreaks();
    }
    const getSurveyStreaks = async ()=> {
        try {
            const response = await axiosConfig.get(`/survey-streaks-api`)
            setStreaks(response.data)
        } catch (error) {
            console.error(error)
        }
    }
    const fetchCheerStreak = async ()=> {
        try {
            const response = await axiosConfig.get('/cheer-and-streak-api')
            setCheerCount(response.data.cheerCount)
        } catch (error) {
            console.log('Error: ', error)
        }
    }
    const sendCheers = async () => {
        if (userId !== '' && cheerComment !== '') {
            setSending(true)

            let attributes = {
                userId: userId,
                status: cheer,
                comment: cheerComment
            }

            axiosConfig.post('/cheer-api', attributes).then((response) => {
                Alert.alert('Success', 'Your cheers has been sent.')
                fetchCheerStreak()

                setSending(false)
            })
        } else {
            Alert.alert('Error', 'Please check all fields')
        }
    }

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block flex>
                <ScrollView>
                    {sending ?
                        <Spinner
                            visible={sending}
                            textContent={'Please Wait...'}
                            textStyle={styles.spinnerTextStyle}
                        />:null
                    }

                    {loading ?
                        <Spinner
                            visible={loading}
                            textContent={'Please Wait...'}
                            textStyle={styles.spinnerTextStyle}
                        />
                        :
                        <Block>
                            {(total==null && !last) ?
                                <Block style={styles.container}>
                                    <Text muted center style={{lineHeight: 20}}>
                                        <Text>You have no surveys at the moment {'\n'}</Text>
                                        <Text>The previous survey has expired {'\n'}</Text>
                                        <Text>Your new survey will arrive in</Text>
                                    </Text>
                                </Block>:null
                            }

                            {(total !=null && !last) ?
                                <QuestionAndAnswer
                                    isCustom={isCustom}
                                    answers={answers}
                                    firstSurvey={firstSurvey}
                                    position={position}
                                    total={total}
                                    questionText={questionText}
                                    currentQuestion={currentQuestion}
                                    selectAnswer={selectAnswer}
                                    radioSelected={radioSelected}
                                    circleSelected={circleSelected}
                                    next={nextQuestion}
                                    setMessage={setMessage}
                                    message={message}
                                />:null
                            }

                            {last > 0 ?
                                <SendCheers
                                    setCheer={setCheer}
                                    setCheerComment={setCheerComment}
                                    setUserId={setUserId}
                                    sendCheers={sendCheers}
                                />:null
                            }

                            {(total!=null||answerCount > 0) ?
                                <Streak
                                    streaks={streaks}
                                    cheerCount={cheerCount}
                                />:null
                            }
                        </Block>
                    }
                </ScrollView>
            </Block>
        </Block>
    )
}
const styles = StyleSheet.create({
    container: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    spinnerTextStyle: {
        color: '#FFF'
    }
});
