import { StyleSheet } from "react-native";
import {theme} from "galio-framework";

export default StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    container: {
        flex: 1
    },
    title: {
        fontSize: 25,
        fontWeight: '700',
        marginVertical: 15,
        marginHorizontal: 10
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerText: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
        paddingBottom: 10
    },
    emptyText: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    dropDownContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
    dropdownsRowEmployee: {flexDirection: 'row',width: '35%', paddingTop: '2%', paddingBottom: '2%'},
    dropdownsRow: {flexDirection: 'row', width: '100%', paddingTop: '2%', paddingBottom: '2%'},
    dropdownsBottom: {flexDirection: 'row', width: '100%', paddingTop: '2%', paddingBottom: '2%'},
    divider: {width: 12},
    dropdown1BtnStyle: {
        flex: 1,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 5,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 12},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 12}
})
