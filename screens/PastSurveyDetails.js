import React, {useContext, useEffect, useRef, useState} from "react";
import {argonTheme} from "../constants";
import {ActivityIndicator, RefreshControl, ScrollView, StyleSheet, TouchableOpacity} from "react-native";
import {Block, Text, theme} from "galio-framework";
import SurveyDetails from "../components/surveys/CurrentPast/SurveyDetails";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import Spinner from "react-native-loading-spinner-overlay";
import BarGraph from "../components/surveys/CurrentPast/BarGraph";
import ResponseFilter from "../components/surveys/CurrentPast/ResponseFilter";
import Responses from "../components/surveys/CurrentPast/Responses";
import ResponseActions from "../components/surveys/CurrentPast/ResponseActions";
import OutsideView from "react-native-detect-press-outside";
import RenderQuestions from "../components/surveys/CurrentPast/Questions";

export default function PastSurveyDetails ({ route, navigation }) {
    const childRef = useRef();
    const { user } = useContext(AuthContext);
    const [loading, setLoading] = useState(true);
    const [surveyId, setSurveyId] = useState('');
    const [selectedLocation, setSelectedLocation] = useState(null);
    const [selectedTeam, setSelectedTeam] = useState(null);
    const [selectedCategory, setSelectedCategory] = useState(null);
    const [surveyDetails, setSurveyDetails] = useState([]);
    const [questions, setQuestions] = useState([]);
    const [messageCount, setMessageCount] = useState('');
    const [questionIndex, setQuestionIndex] = useState(undefined);
    const [showNew, setShowNew] = useState(true);
    const [showReplied, setShowReplied] = useState(true);
    const [showResolved, setShowResolved] = useState(true);
    const [hasCheckBoxAction, setHasCheckBoxAction] = useState(false);
    const [isOpen, setIsOpen] = useState(false)
    const [individual, setIndividual] = useState(false)
    const [anyId, setId] = useState('')
    const [anyType, setType] = useState('')
    const [showActions, setShowActions] = useState(false);
    const [ansIndex, setAnsIndex] = useState(undefined);
    const [blur, setBlur] = useState(false);
    const [token, setToken] = useState('');
    const [answerPercentage, setAnswerPercentage] = useState('')
    const [allAnswerStatusCount, setAllAnswerStatusCount] = useState([])
    const [filter, setFilter] = useState('desc');
    const [answersStatusCount, setAnswersStatusCount] = useState([])
    const [loadResponse, setLoadResponse] = useState(false)
    const [answers, setAnswers] = useState([]);
    const [locations, setLocations] = useState([])
    const [teams, setTeams] = useState([])
    const [firstSurvey, setFirstSurvey] = useState('')
    const [surveyTotal, setSurveyTotal] = useState('')
    const [customQuestions, setCustomQuestions] = useState([])
    const [graphPercentage, setGraphPercentage] = useState('')
    const [categories, setCategories] = useState([])
    const [survey, setSurvey] = useState([])
    const [userAnswerCount, setUserAnswerCount] = useState([])

    useEffect(()=> {
        if (route?.params?.id) {
            setSurveyId(route?.params?.id)
        }
    },[route?.params?.id])

    useEffect(()=> {
        if (hasCheckBoxAction) {
            loadResponses()
        }
    },[showNew, showResolved, showReplied, hasCheckBoxAction, loadResponse])

    useEffect(()=> {
        if (surveyId) {
            getPastSurveyDetails(surveyId)
            setToken(user.token)
        }
    },[surveyId, token])
    useEffect(()=> {
        if (isOpen) {
            loadResponses()
        }
    },[isOpen, questionIndex, filter, anyId])
    const loadResponses = async () => {
        await fetchResponses();

        setHasCheckBoxAction(false)
        setIndividual(false)
    }

    const getPastSurveyDetails = async (surveyId) => {
        try {
            let attributes = {
                surveyId: surveyId,
                location: selectedLocation,
                team: selectedTeam,
                category: selectedCategory,
            }

            axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;

            const response = await axiosConfig.get(`/past-survey-details-api`, {params: attributes}).catch(error => {
                console.log('error: ', error.response)
            })

            // this.preloader = false;
            setLocations(response.data?.locations)
            setTeams(response.data?.teams)

            setSurveyDetails(response.data?.surveyDetails)
            setFirstSurvey(response.data?.firstSurvey)
            setSurveyTotal(response.data?.surveyTotal)
            setQuestions(response.data?.questions)
            setCustomQuestions(response.data?.customQuestions)
            setAnswersStatusCount(response.data?.answersStatusCount)
            setAllAnswerStatusCount(response.data?.allAnswerStatusCount)
            setMessageCount(response.data?.messageCount)
            setAnswerPercentage(response.data?.answerPercentage)
            // this.graphPercentage        = response.data?.graphPercentage
            setGraphPercentage(response.data?.graphPercentage)

            // this.userAnswerCount        = response.data?.userAnswerCount
            setUserAnswerCount(response.data?.userAnswerCount)

            setCategories(response.data?.categories)
            setSurvey(response.data?.survey)

            // this.searching = false
            // console.log('searching: ', this.searching)

            setLoading(false)
        } catch (error) {
            console.log(error)
        }
    }

    const showHide = (index, id, type) => {
        if (questionIndex !== index) {
            setQuestionIndex(index)
            setIsOpen(true)
        } else {
            setQuestionIndex(undefined)
            setIsOpen(false)
        }

        setId(id)
        setType(type)
    }
    const fetchIndividualAnswers = (id, type, index) => {
        if (questionIndex !== index) {
            setQuestionIndex(index)
            setIsOpen(true)
        }

        setId(id)
        setType(type)
        setIndividual(true)
    }
    const checkBoxAction =  async (status, index, checkBoxStatus) => {
        setHasCheckBoxAction(true)

        if(status === 'new') {
            setShowNew(checkBoxStatus)
        }
        else if (status === 'replied') {
            setShowReplied(checkBoxStatus)
        }
        else {
            setShowResolved(checkBoxStatus)
        }
    }
    function category(question) {
        if (question?.sub_category_id != null && question.company_id == null) {
            return <Text>Category: {question.sub_category?.category?.name}</Text>
        }

        if (question?.sub_category_id == null && question.company_id == null) {
            return <Text>Category: Happiness</Text>
        }
        if (question.custom) {
            return <Text>Category: Custom</Text>
        }
    }
    function goToMessaging (answer) {
        setQuestionIndex(undefined)
        setIsOpen(false)
        navigation.navigate('Messaging', {answer, token})
    }
    const fetchResponses = async () => {
        setLoadResponse(true)

        let attribute = {
            surveyId: surveyId,
            answerId: anyType == 'answer' ? anyId : null,
            questionId: anyType == 'question' ? anyId : null,
            showReplied: showReplied?1:0,
            showResolved: showResolved?1:0,
            showNew: showNew?1:0,
            filter: filter,
            // location: selectedLocation,
            // team: selectedTeam
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.get('/past-survey/user-answer-api?', { params: attribute}).then(response => {
            setAnswers(response.data.answer)
            setAnswersStatusCount(response.data.answersStatusCount)
            setLoadResponse(false)

            setBlur(false)
            setAnsIndex(undefined)
        }).catch(Error => {
            setLoadResponse(false)
            setBlur(false)
            setAnsIndex(undefined)
            console.log('Error: ', Error.response.data)
        })
    }
    function renderQuestions () {
        return (
            questions.map((question, index) => {
                return (
                    <Block style={styles.surveyDetailsContainer} key={index}>
                        <Block style={{marginBottom: 20}}>
                            <Text muted>{ category(question) }</Text>
                            <Text size={13}>{question.text + ' (' + allAnswerStatusCount[question.id]?.messageCount} replies)</Text>
                        </Block>
                        {
                            question.answers.map((answer, answerIndex) => {
                                return (
                                    <BarGraph
                                        iWhich={'past'}
                                        userAnswerCount={userAnswerCount[answer?.id]}
                                        key={answerIndex}
                                        answer={answer}
                                        answerText={answer.text}
                                        answerPercentage={answerPercentage[answer.id]}
                                        fetchIndividualAnswers={fetchIndividualAnswers}
                                        index={index}
                                    />
                                )
                            })
                        }

                        <Block style={{flexDirection: 'row'}}>
                            <Block style={{backgroundColor: '#edeff2', height: 1, flex: 1, alignSelf: 'center'}} />
                            <TouchableOpacity
                                onPress={() => showHide(index, question.id, 'question')}
                            >
                                <Text size={10} muted style={{textDecorationLine: "underline"}}>
                                    <Text style={{ textDecorationLine: 'underline', alignSelf:'center', paddingHorizontal:5, fontSize: 12, color: '#8898aa' }}>
                                        {questionIndex === index ? 'Hide' : 'Show'} messages
                                    </Text>
                                </Text>
                            </TouchableOpacity>
                            <Block style={{backgroundColor: '#edeff2', height: 1, flex: 1, alignSelf: 'center'}} />
                        </Block>

                        {(questionIndex === index || (hasCheckBoxAction && questionIndex === index)) && (
                            <Block>
                                <ResponseFilter
                                    index={index}
                                    questionIndex={questionIndex}
                                    showNew={showNew}
                                    showReplied={showReplied}
                                    showResolved={showResolved}
                                    checkBoxAction={checkBoxAction}
                                    setFilter={setFilter}
                                    questionId={question.id}
                                    allAnswerStatusCount={allAnswerStatusCount}
                                    answersStatusCount={answersStatusCount}
                                    answers={question?.answers}
                                    setId={setId}
                                    setType={setType}
                                />

                                {loadResponse ? (
                                    <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                                ) : (
                                    answers.map((answer, aIndex) => {
                                        return (
                                            <Block key={aIndex}>
                                                {answer.status != 'new' ? (
                                                    <TouchableOpacity
                                                        onPress={()=>goToMessaging(answer)}
                                                    >
                                                        <Responses
                                                            answer={answer}
                                                            setActions={setShowActions}
                                                            showActions={showActions}
                                                            aIndex={aIndex}
                                                            setIndex={setAnsIndex}
                                                            ansIndex={ansIndex}
                                                            setBlur={setBlur}
                                                            blur={blur}
                                                        />
                                                    </TouchableOpacity>
                                                ) : (
                                                    <Responses
                                                        answer={answer}
                                                        setActions={setShowActions}
                                                        showActions={showActions}
                                                        aIndex={aIndex}
                                                        setIndex={setAnsIndex}
                                                        ansIndex={ansIndex}
                                                        setBlur={setBlur}
                                                        blur={blur}
                                                    />
                                                )
                                                }
                                                {
                                                    (aIndex == ansIndex) &&
                                                    <ResponseActions
                                                        isMessaging={false}
                                                        answer={answer}
                                                        token={user.token}
                                                        state={'fixed-question'}
                                                        questionConversationId={answer.question_conversation_id}
                                                        userAnswerId={answer.id}
                                                        userId={answer?.user_id}
                                                        questionId={question?.id}
                                                        answerId={answer?.answer_id}
                                                        handleResponseReload={fetchResponses}
                                                    />
                                                }
                                            </Block>
                                        )
                                    })
                                )
                                }
                            </Block>
                        )
                        }
                    </Block>
                )
            })
        )
    }

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block flex>
                <ScrollView>
                    {loading ?
                        <Spinner
                            visible={loading}
                            textContent={'Please Wait...'}
                            textStyle={styles.spinnerTextStyle}
                        />
                        :
                        <Block>
                            <Block flex style={styles.surveyDetailsContainer}>
                                <Block flex>
                                    <Block style={styles.surveyDetails}>
                                        {!loading &&
                                            <SurveyDetails
                                                surveyDetails={surveyDetails}
                                                messageCount={messageCount}
                                            />
                                        }
                                    </Block>
                                </Block>
                            </Block>
                            <OutsideView
                                childRef={childRef}
                                onPressOutside={() => {
                                    setBlur(false)
                                    setAnsIndex(undefined)
                                }}
                            >
                                {renderQuestions()}
                            </OutsideView>
                        </Block>
                    }
                </ScrollView>
            </Block>
        </Block>
        )
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    surveyDetailsContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        maxWidth: 500
    },
    noSurvey: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
    },
    surveyDetails: {
        marginTop: 10
    },
    text: {
        lineHeight: 30
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    }
});
