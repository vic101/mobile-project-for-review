import React, {useContext, useEffect, useState} from "react";
import {
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
  Platform, View
} from "react-native";
import { Block, Text, theme } from "galio-framework";

import {Button, Input, Switch} from "../components";
import { Images, argonTheme } from "../constants";
import { HeaderHeight } from "../constants/utils";
import TrialBanner from "../components/TrialBanner";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";

const { width, height } = Dimensions.get("screen");

const thumbMeasure = (width - 48 - 32) / 3;

export default function PastSurvey({ navigation }) {
  const { user } = useContext(AuthContext);
  const [pastSurveys, setPastSurveys] = useState([]);
  const [searching, setSearching] = useState(false);

  useEffect(()=> {
    getPastSurveys()
  },[]);
  function getPastSurveys(sort = 'desc') {
    setSearching(true)
    axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
    axiosConfig.get(`/past-survey-api`, {params: {sort}}).then(response => {
      setPastSurveys(response.data?.pastSurveys ?? [])
      setSearching(false)
    }).catch(error => {
      console.log('Error: ', error.response.data)
      alert('Error: '+ error.response.data)
    });
  }
  function onChangeHandler(event) {
    getPastSurveys(event.target.value)
  }

  function goToDetails(id) {
    navigation.navigate("Survey Details", {id: id})
  }

  function renderPastSurvey () {
    return (
        <Block flex style={styles.pastSurveyContainer}>
          {
            pastSurveys.map((survey, index) => {
              return(
                  <TouchableOpacity
                      key={index}
                      onPress={()=> goToDetails(survey?.pivot?.id)}
                  >
                    <Block style={{ flexDirection: 'row' }}>
                      <Block style={{ flex: 0.6, marginRight: 2 }}>
                        <Text>{ '#'+ survey.survey_number} </Text>
                      </Block>
                      <Block style={{ flex: 2.5 }}>
                        <Text>{ survey.pivot.title} </Text>
                      </Block>

                      <Block style={{ marginLeft: 5, marginRight: 5}}>
                        <Text muted>|</Text>
                      </Block>
                      <Block style={{ flex: 2 }}>
                        <Text muted>{ survey.responsesCount } responses </Text>
                      </Block>
                      <Block style={{ marginLeft: 5, marginRight: 5}}>
                        <Text muted>|</Text>
                      </Block>
                      <Block style={{ flex: 2 }}>
                        <Text muted>{ survey.messagesCount } messages</Text>
                      </Block>
                    </Block>
                    <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                      <Block style={styles.divider} />
                    </Block>
                  </TouchableOpacity>
              )
            })
          }
        </Block>
    )
  }
  return (
      <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <TrialBanner navigation={navigation}/>
            {pastSurveys.length < 1 ?
                <Text>You have no past surveys at the moment</Text>
                :
                renderPastSurvey()
            }
          </ScrollView>
        </ScrollView>
      </Block>
  );
}

const styles = StyleSheet.create({
  divider: {
    width: "90%",
    borderWidth: 0.8,
    borderColor: "#E9ECEF"
  },
  profile: {
    marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    // marginBottom: -HeaderHeight * 2,
    flex: 1
  },
  textCustom: {
    backgroundColor: argonTheme.COLORS.BACKGROUND
  },
  profileContainer: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1
  },
  profileBackground: {
    width: width,
    height: height / 2
  },
  pastSurveyContainer: {
    // position: "relative",
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: 10,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2
  },
  info: {
    paddingHorizontal: 40
  },
  avatarContainer: {
    position: "relative",
    marginTop: -80
  },
  avatar: {
    width: 124,
    height: 124,
    borderRadius: 62,
    borderWidth: 0
  },
  upcomingDetails: {
    marginTop: 10
  },
  text: {
    lineHeight: 30
  },
  questionText: {
    lineHeight: 20,
    // flexWrap: 'wrap',
    // flex: 1
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: "center",
    width: thumbMeasure,
    height: thumbMeasure
  }
});
