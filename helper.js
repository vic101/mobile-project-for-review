import {API_URL} from './Config'
import axios from "axios";
import axiosConfig from "./helpers/axiosConfig";

export async function fetchPublishableKey() {
    try {
        const response = await axiosConfig.get(`/stripe-config`).catch(err => {
            console.log('Something went wrong', err.response.data)
        })

        return response.data;
    } catch (e) {
        console.log(e)
        console.warn('Unable to fetch publishable key. Is your server running?')
        alert('Error: Unable to fetch publishable key. Is your server running?')
    }
}
