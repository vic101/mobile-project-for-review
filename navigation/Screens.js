import React, {useContext, useEffect, useState} from "react";
import {Easing, Animated, Dimensions, ActivityIndicator} from "react-native";

import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { Block } from "galio-framework";

// screens
import Home from "../screens/Home";
import 'react-native-gesture-handler';
import CurrentSurvey from "../screens/CurentSurvey";
import CurrentSurveyEmployee from "../screens/Surveys/Employee/CurrentSurveyEmployee";
import Messaging from "../components/surveys/CurrentPast/Messaging"
import UpcomingSurvey from "../screens/UpcomingSurvey";
import PastSurvey from "../screens/PastSurvey";
import PastSurveyDetails from "../screens/PastSurveyDetails";
import VibeMonitor from "../screens/VibeMonitor";

import Dashboard from "../screens/SuperAdmin/Dashboard";
import Customers from "../screens/SuperAdmin/Customers";
import CustomerSuccess from "../screens/SuperAdmin/CustomerSuccess";
import ActivityLog from "../screens/SuperAdmin/ActivityLog";
import SuperVibeMonitor from "../screens/SuperAdmin/VibeMonitor";
import SurveyEditor from "../screens/SuperAdmin/SurveyEditor";
import SurveyTranslations from "../screens/SuperAdmin/SurveyTranslations";
import UiTranslations from "../screens/SuperAdmin/UiTranslations";
import DataPurge from "../screens/SuperAdmin/DataPurge";

import Messages from "../screens/Messages";

import ManageEmployees from "../screens/ManageEmployees";
import FormUser from "../screens/FormUser";
import Managers from "../screens/ManageManagers";
import Locations from "../screens/ManageLocations";
import Teams from "../screens/ManageTeams";
import FormTeamLocation from "../screens/FormTeamLocation";

import Account from "../screens/Account";
import AccountLoginSecurity from "../screens/AccountLoginSecurity";
import AccountLanguageTime from "../screens/AccountLanguageTime";
import AccountNotifications from "../screens/AccountNotifications";
import AccountBilling from "../screens/AccountBilling";
import CancelBilling from "../screens/Accounts/CancelBilling";

import LoginScreen from "../screens/Auth/LoginScreen";
import PasswordResetScreen from "../screens/Auth/PasswordResetScreen";
import RegisterScreen from "../screens/Auth/RegisterScreen"
import TermsScreen from "../screens/Auth/Terms"
import PrivacyPolicyScreen from "../screens/Auth/PrivacyPolicy"
import SuccessScreen from "../screens/Auth/SuccessScreen";
import YouMadeItScreen from "../screens/Auth/YouMadeIt";
import CreatePasswordScreen from "../screens/Auth/CreatePassword";
import SetupLocationsScreen from "../screens/Auth/SetupLocations";
import SetupTeamsScreen from "../screens/Auth/SetupTeams";
import ReadySetGoScreen from "../screens/Auth/ReadySetGo";

import Settings from "../screens/SettingsScreen";
import Cheers from "../screens/Cheers";

import Pro from "../screens/Pro";

import Profile from "../screens/Profile";
import Elements from "../screens/Elements";
import Articles from "../screens/Articles";
// drawer
import CustomDrawerContent from "./Menu";

// header for screens
import { Icon, Header } from "../components";
import { argonTheme, tabs } from "../constants";
import {NavigationContainer} from "@react-navigation/native";
import {AuthContext} from "../context/AuthProvider";
import * as SecureStore from "expo-secure-store";
import WaitingReply from "../components/messages/WaitingReply";

import Resolved from "../components/messages/Resolved";

const { width } = Dimensions.get("screen");

const Stack  = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab    = createBottomTabNavigator();

const AuthStackNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{ headerShown: false, headerBackTitleVisible: false }}
        >
            <Stack.Screen
                name="Login Screen"
                component={LoginScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Register Screen"
                component={RegisterScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Password Reset"
                component={PasswordResetScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Terms Screen"
                component={TermsScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Privacy Policy Screen"
                component={PrivacyPolicyScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Success Screen"
                component={SuccessScreen}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};
function SettingsStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Settings"
                component={Settings}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Settings"
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}

function ElementsStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Elements"
        component={Elements}
        options={{
          header: ({ navigation, scene }) => (
            <Header
                bgColor={argonTheme.COLORS.THEME_COLOR}
                white
                title="Elements"
                navigation={navigation}
                scene={scene}
            />
          ),
          cardStyle: { backgroundColor: "#F8F9FE" }
        }}
      />
    </Stack.Navigator>
  );
}
function ArticlesStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Articles"
        component={Articles}
        options={{
          header: ({ navigation, scene }) => (
            <Header
                bgColor={argonTheme.COLORS.THEME_COLOR}
                white
                title="Articles"
                navigation={navigation}
                scene={scene}
            />
          ),
          cardStyle: { backgroundColor: "#F8F9FE" }
        }}
      />
    </Stack.Navigator>
  );
}
function ProfileStack(props) {
  return (
    <Stack.Navigator initialRouteName="Profile" mode="card" headerMode="screen">
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{
          header: ({ navigation, scene }) => (
            <Header
                bgColor={argonTheme.COLORS.THEME_COLOR}
                transparent
                white
                title="Profile"
                navigation={navigation}
                scene={scene}
            />
          ),
          cardStyle: { backgroundColor: "#FFFFFF" },
          headerTransparent: true
        }}
      />
    </Stack.Navigator>
  );
}
function HomeStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          header: ({ navigation, scene }) => (
            <Header
                bgColor={argonTheme.COLORS.THEME_COLOR}
                white
                title="Home"
                // search
                // options
                navigation={navigation}
                scene={scene}
            />
          ),
          cardStyle: { backgroundColor: "#F8F9FE" }
        }}
      />
    </Stack.Navigator>
  );
}

function CurrentSurveyStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Current Survey"
                component={CurrentSurvey}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Current Survey"
                            navigation={navigation}
                            trial={false}
                            // options
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
            <Stack.Screen
                name="Messaging"
                component={Messaging}
                options={{ title: 'Current Survey Responses' }}
            />
            <Stack.Screen
                name="FormUser"
                component={FormUser}
                options={{ title: '' }}
            />
        </Stack.Navigator>
    );
}

function DashboardStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Dashboard"
                component={Dashboard}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Dashboard"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function CustomersStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Customers"
                component={Customers}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Customers"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function CustomerSuccessStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Customer Success"
                component={CustomerSuccess}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Customer Success"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function ActivityLogStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Activity Log"
                component={ActivityLog}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Activity Log"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function SuperVibeMonitorStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Super VM"
                component={SuperVibeMonitorStack}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Vibe Monitor"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}

function SurveyEditorStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Survey editor"
                component={SurveyEditor}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Survey Editor"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function SurveyTranslationsStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Survey translations"
                component={SurveyTranslations}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Survey translations"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function UiTranslationsStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="UI translations"
                component={UiTranslations}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="UI translations"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function DataPurgeStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Data Purge"
                component={DataPurge}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Data Purge"
                            navigation={navigation}
                            trial={false}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}

function CurrentSurveyEmployeeStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Current Survey Employee"
                component={CurrentSurveyEmployee}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Current Survey"
                            navigation={navigation}
                            trial={false}
                            // options
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
function UpcomingSurveyStack(props) {
    const { user } = useContext(AuthContext);
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Upcoming Survey"
                component={UpcomingSurvey}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Upcoming Survey"
                            trial={false}
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
            <Stack.Screen
                name="FormUser"
                component={FormUser}
                options={{ title: '' }}
            />
        </Stack.Navigator>
    );
}
function PastSurveyStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Past Survey"
                component={PastSurvey}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Past Surveys"
                            // search
                            trial={false}
                            navigation={navigation}
                            scene={scene}
                            transparent
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
            <Stack.Screen
                name="Survey Details"
                component={PastSurveyDetails}
                options={{ title: 'Past Survey Details' }}
            />
            <Stack.Screen
                name="Messaging"
                component={Messaging}
                options={{ title: 'Past Survey Responses' }}
            />
            <Stack.Screen
                name="FormUser"
                component={FormUser}
                options={{ title: '' }}
            />
        </Stack.Navigator>
    );
}
function VibeMonitorStack(props) {
    const { user } = useContext(AuthContext);
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Vibe Monitor"
                component={VibeMonitor}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Vibe Monitor"
                            trial={false}
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
            <Stack.Screen
                name="FormUser"
                component={FormUser}
                options={{ title: '' }}
            />
        </Stack.Navigator>
    );
}
function MessagesStack(props) {
    const { user } = useContext(AuthContext);
    switch (user.role) {
        case 'admin':
            return (
                <Stack.Navigator mode="card" headerMode="screen">
                    <Stack.Screen
                        name="my_reply"
                        component={Messages}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Messages"
                                    // search
                                    trial={false}
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                            cardStyle: { backgroundColor: "#F8F9FE" }
                        }}
                    />

                    <Stack.Screen
                        name="employee_reply"
                        component={WaitingReply}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Waiting for employee's reply"
                                    back
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                        }}
                    />

                    <Stack.Screen
                        name="resolved"
                        component={Resolved}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Resolved"
                                    back
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                        }}
                    />
                    <Stack.Screen
                        name="FormUser"
                        component={FormUser}
                        options={{ title: '' }}
                    />
                </Stack.Navigator>
            );
            break;
        case 'manager':
            return (
                <Stack.Navigator mode="card" headerMode="screen">
                    <Stack.Screen
                        name="my_reply"
                        component={Messages}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Messages"
                                    // search
                                    trial={false}
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                            cardStyle: { backgroundColor: "#F8F9FE" }
                        }}
                    />

                    <Stack.Screen
                        name="employee_reply"
                        component={WaitingReply}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Waiting for employee's reply"
                                    back
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                        }}
                    />

                    <Stack.Screen
                        name="resolved"
                        component={Resolved}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Resolved"
                                    back
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                        }}
                    />
                </Stack.Navigator>
            );
            break;
        case 'employee':
            return (
                <Stack.Navigator mode="card" headerMode="screen">
                    <Stack.Screen
                        name="my_reply"
                        component={Messages}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Messages"
                                    // search
                                    trial={false}
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                            cardStyle: { backgroundColor: "#F8F9FE" }
                        }}
                    />

                    <Stack.Screen
                        name="organization_reply"
                        component={WaitingReply}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Waiting for organization's reply"
                                    back
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                        }}
                    />

                    <Stack.Screen
                        name="archive"
                        component={Resolved}
                        options={{
                            header: ({ navigation, scene }) => (
                                <Header
                                    bgColor={argonTheme.COLORS.THEME_COLOR}
                                    white
                                    title="Archive"
                                    back
                                    navigation={navigation}
                                    scene={scene}
                                />
                            ),
                        }}
                    />
                </Stack.Navigator>
            );
            break;
    }
}
function ManageEmployeeStack(props) {
    const { user } = useContext(AuthContext);
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Manage Employees"
                component={ManageEmployees}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Manage Employees"
                            // search
                            trial={false}
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
            <Stack.Screen
                name="FormUser"
                component={FormUser}
                options={{ title: '' }}
            />
            <Stack.Screen
                name="Managers"
                component={Managers}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Manage Managers"
                            back
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
            <Stack.Screen
                name="Locations"
                component={Locations}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Manage Locations"
                            back
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
            <Stack.Screen
                name="Teams"
                component={Teams}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Manage Teams"
                            back
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
            <Stack.Screen
                name="FormTeamLocation"
                component={FormTeamLocation}
                options={{ title: '' }}
            />
        </Stack.Navigator>
    );
}
function AccountStack(props) {
    const { user } = useContext(AuthContext);
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Account"
                component={Account}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Account"
                            trial={false}
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />

            {user.role === 'admin' &&
                <Stack.Screen
                    name="FormUser"
                    component={FormUser}
                    options={{ title: '' }}
                />
            }

            <Stack.Screen
                name="LoginSecurity"
                component={AccountLoginSecurity}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="LoginScreen & security"
                            back
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
            <Stack.Screen
                name="LanguageTime"
                component={AccountLanguageTime}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Language & time"
                            back
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
            <Stack.Screen
                name="Notifications"
                component={AccountNotifications}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Notifications"
                            back
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                }}
            />
            {user.role === 'admin' &&
                <Stack.Screen
                    name="Billing"
                    component={AccountBilling}
                    options={{
                        header: ({ navigation, scene }) => (
                            <Header
                                bgColor={argonTheme.COLORS.THEME_COLOR}
                                white
                                title="Billing"
                                back
                                navigation={navigation}
                                scene={scene}
                            />
                        ),
                    }}
                />
            }
            {user.role === 'admin' &&
                <Stack.Screen
                    name="CancelBilling"
                    component={CancelBilling}
                    options={{
                        header: ({ navigation, scene }) => (
                            <Header
                                bgColor={argonTheme.COLORS.THEME_COLOR}
                                white
                                title="Cancel account"
                                back
                                navigation={navigation}
                                scene={scene}
                            />
                        ),
                    }}
                />
            }
        </Stack.Navigator>
    );
}
function CheersStack(props) {
    return (
        <Stack.Navigator mode="card" headerMode="screen">
            <Stack.Screen
                name="Cheers"
                component={Cheers}
                options={{
                    header: ({ navigation, scene }) => (
                        <Header
                            bgColor={argonTheme.COLORS.THEME_COLOR}
                            white
                            title="Cheers"
                            trial={false}
                            navigation={navigation}
                            scene={scene}
                        />
                    ),
                    cardStyle: { backgroundColor: "#F8F9FE" }
                }}
            />
        </Stack.Navigator>
    );
}
export default function App(props) {
    const [isLoading, setIsLoading] = useState(true);
    const { user, setUser, setStep, setBanner } = useContext(AuthContext);

    useEffect(() => {
        // check if user is logged in or not.
        // Check SecureStore for the user object/token

        SecureStore.getItemAsync('user')
            .then(userString => {
                if (userString) {
                    setUser(JSON.parse(userString));
                }
                setIsLoading(false);
            })
            .catch(err => {
                console.log(err);
                setIsLoading(false);
            });

        SecureStore.getItemAsync("step").then(stepString => {
            if (stepString) {
                setStep(stepString);
            }
        })

        SecureStore.getItemAsync("banner").then(dateString => {
            let currentDate = new Date().getDate();

            if(dateString) {
                if (dateString < currentDate) {
                    setBanner(true)

                } else {
                    setBanner(false)
                }
            } else {
                setBanner(true)
            }
        })
    }, []);

    if (isLoading) {
        return (
            <Block style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator size="large" color="gray" />
            </Block>
        );
    }

    return (
        <>
            {user ? (
                <NavigationContainer>
                    <Stack.Navigator mode="card" headerMode="none">
                        <Stack.Screen name="App" component={AppStack} />
                    </Stack.Navigator>
                </NavigationContainer>
            ) : (
                <NavigationContainer>
                    <AuthStackNavigator />
                </NavigationContainer>
            )}
        </>
  );
}
function SuperAdminStack() {
    return (
        <Drawer.Navigator
            style={{ flex: 1 }}
            drawerContent={props => <CustomDrawerContent {...props} />}
            drawerStyle={{
                backgroundColor: "white",
                width: width * 0.8
            }}
            drawerContentOptions={{
                activeTintcolor: "white",
                inactiveTintColor: "#000",
                activeBackgroundColor: "transparent",
                itemStyle: {
                    width: width * 0.75,
                    backgroundColor: "transparent",
                    paddingVertical: 16,
                    paddingHorizonal: 12,
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center",
                    overflow: "hidden"
                },
                labelStyle: {
                    fontSize: 18,
                    marginLeft: 12,
                    fontWeight: "normal"
                }
            }}
        >

            <Drawer.Screen name="Dashboard" component={DashboardStack} />
            <Drawer.Screen name="Customers" component={CustomersStack} />
            <Drawer.Screen name="Customer Success" component={CustomerSuccessStack} />
            <Drawer.Screen name="Activity Log" component={ActivityLogStack} />

            <Drawer.Screen name="Vibe Monitor" component={VibeMonitorStack} />

            <Drawer.Screen name="Survey editor" component={SurveyEditorStack} />
            <Drawer.Screen name="Survey translations" component={SurveyTranslationsStack} />
            <Drawer.Screen name="UI translations" component={UiTranslationsStack} />
            <Drawer.Screen name="Data Purge" component={DataPurgeStack} />
        </Drawer.Navigator>
    );
}
function CsmStack() {
    return (
        <Drawer.Navigator
            style={{ flex: 1 }}
            drawerContent={props => <CustomDrawerContent {...props} />}
            drawerStyle={{
                backgroundColor: "white",
                width: width * 0.8
            }}
            drawerContentOptions={{
                activeTintcolor: "white",
                inactiveTintColor: "#000",
                activeBackgroundColor: "transparent",
                itemStyle: {
                    width: width * 0.75,
                    backgroundColor: "transparent",
                    paddingVertical: 16,
                    paddingHorizonal: 12,
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center",
                    overflow: "hidden"
                },
                labelStyle: {
                    fontSize: 18,
                    marginLeft: 12,
                    fontWeight: "normal"
                }
            }}
        >

            <Drawer.Screen name="Dashboard" component={DashboardStack} />
            <Drawer.Screen name="Customers" component={CustomersStack} />

            <Drawer.Screen name="Activity Log" component={ActivityLogStack} />
            <Drawer.Screen name="Vibe Monitor" component={VibeMonitorStack} />

            <Drawer.Screen name="Account" component={AccountStack} />
        </Drawer.Navigator>
    );
}
function AccountSetupToAdminStack () {
    return (
        <Drawer.Navigator
            style={{ flex: 1 }}
            drawerContent={props => <CustomDrawerContent {...props} />}
            drawerStyle={{
                backgroundColor: "white",
                width: width * 0.8
            }}
            drawerContentOptions={{
                activeTintcolor: "white",
                inactiveTintColor: "#000",
                activeBackgroundColor: "transparent",
                itemStyle: {
                    width: width * 0.75,
                    backgroundColor: "transparent",
                    paddingVertical: 16,
                    paddingHorizonal: 12,
                    justifyContent: "center",
                    alignContent: "center",
                    alignItems: "center",
                    overflow: "hidden"
                },
                labelStyle: {
                    fontSize: 18,
                    marginLeft: 12,
                    fontWeight: "normal"
                }
            }}
            initialRouteName="Current Survey"
        >
            <Drawer.Screen name="Current Survey" component={CurrentSurveyStack} />
            <Drawer.Screen name="Upcoming Survey" component={UpcomingSurveyStack} />
            <Drawer.Screen name="Past Surveys" component={PastSurveyStack} />
            <Drawer.Screen name="Vibe Monitor" component={VibeMonitorStack} />
            <Drawer.Screen name="Messages" component={MessagesStack} />
            <Drawer.Screen name="Manage Employees" component={ManageEmployeeStack} />
            <Drawer.Screen name="Account" component={AccountStack} />
        </Drawer.Navigator>
    );
}
function AppStack(props) {
    const { user } = useContext(AuthContext);
    switch (user.role) {
        case 'super':
            return (
                <Drawer.Navigator
                    style={{ flex: 1 }}
                    drawerContent={props => <CustomDrawerContent {...props} />}
                    drawerStyle={{
                        backgroundColor: "white",
                        width: width * 0.8
                    }}
                    drawerContentOptions={{
                        activeTintcolor: "white",
                        inactiveTintColor: "#000",
                        activeBackgroundColor: "transparent",
                        itemStyle: {
                            width: width * 0.75,
                            backgroundColor: "transparent",
                            paddingVertical: 16,
                            paddingHorizonal: 12,
                            justifyContent: "center",
                            alignContent: "center",
                            alignItems: "center",
                            overflow: "hidden"
                        },
                        labelStyle: {
                            fontSize: 18,
                            marginLeft: 12,
                            fontWeight: "normal"
                        }
                    }}
                    initialRouteName="Current Survey"
                >
                    <Drawer.Screen name="SuperAdminStack" component={SuperAdminStack} />
                </Drawer.Navigator>
            );
            break;
        case 'csm':
            return (
                <Drawer.Navigator
                    style={{ flex: 1 }}
                    drawerContent={props => <CustomDrawerContent {...props} />}
                    drawerStyle={{
                        backgroundColor: "white",
                        width: width * 0.8
                    }}
                    drawerContentOptions={{
                        activeTintcolor: "white",
                        inactiveTintColor: "#000",
                        activeBackgroundColor: "transparent",
                        itemStyle: {
                            width: width * 0.75,
                            backgroundColor: "transparent",
                            paddingVertical: 16,
                            paddingHorizonal: 12,
                            justifyContent: "center",
                            alignContent: "center",
                            alignItems: "center",
                            overflow: "hidden"
                        },
                        labelStyle: {
                            fontSize: 18,
                            marginLeft: 12,
                            fontWeight: "normal"
                        }
                    }}
                    initialRouteName="Current Survey"
                >
                    <Drawer.Screen name="CsmStack" component={CsmStack} />
                </Drawer.Navigator>
            );
            break;
        case 'admin':
            if (user.verified !== null) {
                return (
                    <Drawer.Navigator
                        style={{ flex: 1 }}
                        drawerContent={props => <CustomDrawerContent {...props} />}
                        drawerStyle={{
                            backgroundColor: "white",
                            width: width * 0.8
                        }}
                        drawerContentOptions={{
                            activeTintcolor: "white",
                            inactiveTintColor: "#000",
                            activeBackgroundColor: "transparent",
                            itemStyle: {
                                width: width * 0.75,
                                backgroundColor: "transparent",
                                paddingVertical: 16,
                                paddingHorizonal: 12,
                                justifyContent: "center",
                                alignContent: "center",
                                alignItems: "center",
                                overflow: "hidden"
                            },
                            labelStyle: {
                                fontSize: 18,
                                marginLeft: 12,
                                fontWeight: "normal"
                            }
                        }}
                        initialRouteName="Current Survey"
                    >
                        <Drawer.Screen name="Current Survey" component={CurrentSurveyStack} />
                        <Drawer.Screen name="Upcoming Survey" component={UpcomingSurveyStack} />
                        <Drawer.Screen name="Past Surveys" component={PastSurveyStack} />
                        <Drawer.Screen name="Vibe Monitor" component={VibeMonitorStack} />
                        <Drawer.Screen name="Messages" component={MessagesStack} />
                        <Drawer.Screen name="Manage Employees" component={ManageEmployeeStack} />
                        <Drawer.Screen name="Account" component={AccountStack} />

                        <Drawer.Screen name="Settings" component={SettingsStack} />
                        <Drawer.Screen name="Home" component={HomeStack} />
                        <Drawer.Screen name="Profile" component={ProfileStack} />
                        <Drawer.Screen name="Elements" component={ElementsStack} />
                        <Drawer.Screen name="Articles" component={ArticlesStack} />
                    </Drawer.Navigator>
                );
            } else {
                return (
                        <Stack.Navigator
                            screenOptions={{ headerShown: false, headerBackTitleVisible: false }}
                            initialRouteName={"YouMadeIt"}
                        >
                            <Stack.Screen
                                name="YouMadeIt"
                                component={YouMadeItScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="CreatePassword"
                                component={CreatePasswordScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="SetupLocations"
                                component={SetupLocationsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="SetupTeams"
                                component={SetupTeamsScreen}
                                options={{ headerShown: false }}
                            />
                            <Stack.Screen
                                name="ReadySetGo"
                                component={ReadySetGoScreen}
                                options={{ headerShown: false }}
                            />

                            <Stack.Screen name="AdminHomeStack" component={AccountSetupToAdminStack} />
                        </Stack.Navigator>
                )
            }
            break;
        case 'manager':
            return (
                <Drawer.Navigator
                    style={{ flex: 1 }}
                    drawerContent={props => <CustomDrawerContent {...props} />}
                    drawerStyle={{
                        backgroundColor: "white",
                        width: width * 0.8
                    }}
                    drawerContentOptions={{
                        activeTintcolor: "white",
                        inactiveTintColor: "#000",
                        activeBackgroundColor: "transparent",
                        itemStyle: {
                            width: width * 0.75,
                            backgroundColor: "transparent",
                            paddingVertical: 16,
                            paddingHorizonal: 12,
                            justifyContent: "center",
                            alignContent: "center",
                            alignItems: "center",
                            overflow: "hidden"
                        },
                        labelStyle: {
                            fontSize: 18,
                            marginLeft: 12,
                            fontWeight: "normal"
                        }
                    }}
                    initialRouteName="Current Survey"
                >
                    <Drawer.Screen name="Current Survey" component={CurrentSurveyStack} />
                    <Drawer.Screen name="Past Surveys" component={PastSurveyStack} />
                    <Drawer.Screen name="Vibe Monitor" component={VibeMonitorStack} />
                    <Drawer.Screen name="Messages" component={MessagesStack} />
                    <Drawer.Screen name="Account" component={AccountStack} />
                </Drawer.Navigator>
            );
        case 'employee':
            return (
                    <Drawer.Navigator
                        style={{ flex: 1 }}
                        drawerContent={props => <CustomDrawerContent {...props} />}
                        drawerStyle={{
                            backgroundColor: "white",
                            width: width * 0.8
                        }}
                        drawerContentOptions={{
                            activeTintcolor: "white",
                            inactiveTintColor: "#000",
                            activeBackgroundColor: "transparent",
                            itemStyle: {
                                width: width * 0.75,
                                backgroundColor: "transparent",
                                paddingVertical: 16,
                                paddingHorizonal: 12,
                                justifyContent: "center",
                                alignContent: "center",
                                alignItems: "center",
                                overflow: "hidden"
                            },
                            labelStyle: {
                                fontSize: 18,
                                marginLeft: 12,
                                fontWeight: "normal"
                            }
                        }}
                        initialRouteName="Current Survey Employee"
                    >
                        <Drawer.Screen name="Current Survey" component={CurrentSurveyEmployeeStack} />
                        <Drawer.Screen name="Messages" component={MessagesStack} />
                        <Drawer.Screen name="Cheers" component={CheersStack} />
                        <Drawer.Screen name="Account" component={AccountStack} />
                        <Drawer.Screen name="Elements" component={ElementsStack} />
                    </Drawer.Navigator>
                );
            break;
    }
}

