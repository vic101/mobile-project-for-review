import React, {useContext} from "react";
import {
  ScrollView,
  StyleSheet,
  Image
} from "react-native";
import { Block, Text, theme } from "galio-framework";

import Images from "../constants/Images";
import { DrawerItem as DrawerCustomItem } from '../components';
import {AuthContext} from "../context/AuthProvider";

function CustomDrawerContent({ drawerPosition, navigation, profile, focused, state, ...rest }) {
  const { user } = useContext(AuthContext);
  const role = user.role;

  let screens = [];

  switch (user.role) {
      case 'super':
          screens = [
              "Dashboard",
              "Customers",
              "Customer Success",
              "Activity Log",
              "Vibe Monitor",
              "Survey editor",
              "Survey translations",
              "UI translations",
              "Data Purge",
              "Logout"
          ];
          break;
      case 'csm':
          screens = [
              "Dashboard",
              "Customers",
              "Activity Log",
              "Vibe Monitor",
              "Account",
              "Logout"
          ];
          break;

    case 'admin':
       screens = [
        "Current Survey",
        "Upcoming Survey",
        "Past Surveys",
        "Vibe Monitor",
        "Messages",
        "Manage Employees",
        "Account",
        // "Elements"
      ];
      break;

      case 'manager':
          screens = [
              "Current Survey",
              "Past Surveys",
              "Vibe Monitor",
              "Messages",
              "Account"
          ];
          break;

      case 'employee':
          screens = [
              "Current Survey",
              "Messages",
              "Cheers",
              "Account",
              // "Elements",
          ]
          break;
  }

    const renderHairLine = (index) => {
      switch (role) {
          case 'super':
              if (!(index === 0 || index === 1 || index === 3 || index === 5 || index === 6  || index === 9)) {
                  return <Block style={styles.hairLine}/>
              }
              break;
          case 'csm':
              if (!(index === 2 || index === 4 || index === 5)) {
                  return <Block style={styles.hairLine}/>
              }
              break;
          case 'admin':
              if (!(index === 0 || index === 1 || index === 6)) {
                  return <Block style={styles.hairLine}/>
              }
              break;
          case 'manager':
              if ((index === 1 || index === 2 || index === 3)) {
                  return <Block style={styles.hairLine}/>
              }
              break;
          case 'employee':
              if (index == 2) {
                  return <Block style={styles.hairLine}/>
              }
              break;
      }
    }
  return (
    <Block
      style={styles.container}
      forceInset={{ top: 'always', horizontal: 'never' }}
    >
      <Block flex={0.06} style={styles.header}>
        <Image styles={styles.logo} source={Images.Logo} />
      </Block>

      <Block flex style={{ paddingLeft: 8, paddingRight: 14 }}>
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>

          {screens.map((item, index) => {
              return (
                  <Block flex key={item}>
                      <DrawerCustomItem
                          title={item}
                          key={index}
                          navigation={navigation}
                          focused={state.index == index ? true : false}
                      />

                      {renderHairLine(index)}
                  </Block>
              )
            })
          }
        </ScrollView>
      </Block>
    </Block>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    paddingHorizontal: 28,
    paddingBottom: theme.SIZES.BASE,
    paddingTop: theme.SIZES.BASE * 3,
    justifyContent: 'center'
  },
    hairLine: {
        borderColor: "rgba(0,0,0,0.2)",
        width: '100%',
        borderWidth: StyleSheet.hairlineWidth
    }
});

export default CustomDrawerContent;
