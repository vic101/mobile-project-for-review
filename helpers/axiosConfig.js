import axios from 'axios';

const instance = axios.create({
   baseURL: 'http://vibestrive.local/api'
   // baseURL: 'https://app-stg.vibestrive.com/api'
});

export default instance;
