import React, {useEffect, useState} from 'react'
import { connect } from 'react-redux'
import { fetchUsers } from '../redux'
import {Block, Text} from "galio-framework";
import {ActivityIndicator, FlatList} from "react-native";
import UsersCard from "./usersCard";

function UsersContainer ({ userData, fetchUsers }) {
    const [page, setPage] = useState(1);

    useEffect(() => {
        fetchUsers()
    }, [])

    useEffect(()=> {

    },[])

    function fetchMoreData () {

    }

    function renderUsers (item) {
        console.log('item: ', item.name)

        return (
            // userData.users.map(user => <Text>{user.name}</Text>)
            <Text>{item.name}</Text>
        )
    }

    return userData.loading ? (
        <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
    ) : userData.error ? (
        <Text>{userData.error}</Text>
    ) : (
        <Block>
            <Text>Users List</Text>

            <Block>
                {
                    userData &&
                    userData.users &&
                    userData.users.map((user, index) => <Text key={index}>{user.name}</Text>)
                }
            </Block>
        </Block>
    )
}

const mapStateToProps = state => {
    return {
        userData: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: () => dispatch(fetchUsers())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UsersContainer)
