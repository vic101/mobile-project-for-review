import React, {useContext, useEffect, useState} from "react";
import {AuthContext} from "../context/AuthProvider";
import {Block, Text, theme} from "galio-framework";
import {TouchableOpacity} from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import * as SecureStore from "expo-secure-store";


export default function TrialBanner ({ trial, route, navigation}) {
    const { user, banner, hideBanner, showBanner } = useContext(AuthContext);

    function renderTrialBar() {
        return (
            <Block>
                {banner &&
                    <Text style={{ padding: theme.SIZES.BASE, backgroundColor: '#36d79a' }}>
                        <Text style={{ marginRight: '10%'}}><Text style={{ fontWeight: 'bold' }}>{ user.trialDays + (user.trialDays > 1 ? ' days' : ' day') } left in your trial</Text>. If you wish to continue using vibestrive without interruption, </Text>
                        <Text style={{ color:'#32325D' }}>
                            <TouchableOpacity
                                onPress={()=> navigation.navigate('Billing')}
                            >
                                <Text style={{ color:'#32325D', textDecorationLine: 'underline' }}>enter your billing details</Text>
                            </TouchableOpacity>
                             before your trial ends.
                            <Text style={{ color: '#ffffff' }}> Get invaluable insights about your company's health. Increase your employee's happiness, make them more loyal to your organization, and keep them longer.</Text>
                            <Text>You can enter your billing details already <Text style={{ color: '#ffffff' }}> – we won't charge you before your trial has ended, and you can still cancel your account if you are unhappy.</Text>{'\n'}{'\n'}</Text>
                        </Text>

                        <Block>
                            <TouchableOpacity
                                onPress={()=> hideBanner()}
                            >
                                <Text size={14} style={{textDecorationLine: 'underline',color: '#32325D'}}>Hide this message until tomorrow</Text>
                            </TouchableOpacity>
                        </Block>
                    </Text>
                }

                <Block>
                    <Text style={{ padding: theme.SIZES.BASE, backgroundColor: '#32325D', color: '#ffffff' }}>
                        <Text bold>Add 5 more employees</Text> to send your first survey, and get the most out of your trial period
                        <TouchableOpacity onPress={()=> navigation.navigate('FormUser', {form: 'user', item: null, type: 'employee', action: 'create'})}>
                            <Text style={{ marginLeft: 5, color: '#ffffff', backgroundColor:'#36d79a',paddingHorizontal: 20,borderRadius: 5 }}>Add employees</Text>
                        </TouchableOpacity>
                    </Text>
                </Block>
            </Block>
        );
    }

    return (
        <Block>
            {user.onTrial ? renderTrialBar() : null}
        </Block>
    )
}
