import React from 'react';
import { StatusBar } from 'react-native';
import {Block} from "galio-framework";
import {useSafeAreaInsets} from "react-native-safe-area-context";

const AppStatusBar = ({backgroundColor, ...props}) => {
    const insets = useSafeAreaInsets();
    return (
        <Block style={{ height: insets.top, backgroundColor }}>
            <StatusBar
                animated={true}
                backgroundColor={backgroundColor}
                barStyle="light-content"
            />
        </Block>
    );
};

export default AppStatusBar;
