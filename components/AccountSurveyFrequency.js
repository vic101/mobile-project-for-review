import React, {useContext, useEffect, useState} from "react";
import {
    StyleSheet,
    Dimensions, ActivityIndicator,
} from "react-native";

import {Block, Radio, Text, theme} from "galio-framework";
import { Images, argonTheme } from "../constants";
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";
import {func} from "prop-types";
const { width, height } = Dimensions.get("screen");

export default function Locations() {
    const { user } = useContext(AuthContext);
    const [isLoading, setIsLoading] = useState(true);
    const [everyWeek, setEveryWeek] = useState(false);
    const [everyOtherWeek, setEveryOtherWeek] = useState(false);

    useEffect(() => {
        fetchSurveyFrequencySetting()
    }, []);

    function fetchSurveyFrequencySetting() {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            type: 'surveyFrequency'
        }

        axiosConfig.get('/account-api?', { params: attribute}).then(function (response) {
            let status = response.data.account;

            if (status == 'every_week_wednesday') {
                setEveryWeek(true)
                setEveryOtherWeek(false)
            }
            if (status == "every_other_week_wednesday") {
                setEveryWeek(false)
                setEveryOtherWeek(true)
            }

            setIsLoading(false);
        }).catch(error => {
            console.log(error);
            setIsLoading(false);
        });
    }
    function updateSurveyFrequencySetting(status) {
        let surveyStatus = null
        setIsLoading(true);
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        console.log('status: ', status)

        surveyStatus = status == 'ew' ? 'every_week_wednesday' : 'every_other_week_wednesday'

        let attributes = {
            type: 'survey_frequency',
            value: surveyStatus
        }
        console.log('attributes: ', attributes)
        axiosConfig.post('/account-api', attributes).then(response => {
            alert("Successfully changed.")
            setIsLoading(false);
        })
            .catch(error => {
                console.log(error);
                setIsLoading(false);
            });
    }
    function select (status) {
        if (status == 'eow') {
            setEveryWeek(false)
            setEveryOtherWeek(true)
        }

        if (status == 'ew') {
            setEveryWeek(true)
            setEveryOtherWeek(false)
        }

        updateSurveyFrequencySetting(status)
    }

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block flex>
                <Block flex style={styles.accountContainer}>
                    <Block flex>
                        <Block style={styles.accountDetails}>
                            <Text bold size={15} color="#32325D" style={styles.text}>
                                How often do you wish to send surveys?
                            </Text>
                            <Text size={14} muted style={styles.text}>
                                Each survey takes only ~2 minutes to complete.
                            </Text>
                        </Block>
                    </Block>
                    {isLoading ? (
                        <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
                    ) : (
                        <Block>
                            <Block style={{ marginTop: 5}}>
                                <Radio label="every week on Wednesday" onChange={()=>select('ew')} color="default" initialValue={everyWeek} radioInnerStyle={{ backgroundColor: "#32325d" }} />
                            </Block>
                            <Block style={{ marginTop: 5}}>
                                <Radio label="every other week on Wednesday" onChange={()=>select('eow')} color="default" initialValue={everyOtherWeek} radioInnerStyle={{ backgroundColor: "#32325d" }} />
                            </Block>
                        </Block>
                    )}
                </Block>
            </Block>
        </Block>
    );
}

const styles = StyleSheet.create({
    accountContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
});
