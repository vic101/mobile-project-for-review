import React, {useContext} from "react";
import {Block, Text} from "galio-framework";
import {StyleSheet} from "react-native";
import {withNavigation} from "@react-navigation/compat";
import {AuthContext} from "../../context/AuthProvider";

class AccountLinks extends React.Component {

    render() {
        const { navigation, title, role } = this.props;

        return (
            <Block>
                {role === 'admin' &&
                    (
                        <Block>
                            <Text bold={title=='Account'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("Account")}>
                                Survey frequency
                            </Text>
                            <Text bold={title==='LoginSecurity'}  size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("LoginSecurity")}>
                                Login & security
                            </Text>
                        </Block>
                    )
                }

                {(role==='employee' || role==='manager') &&
                    <Text bold={title=='Account'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("Account")}>
                        Login & security
                    </Text>
                }

                <Text bold={title=='LanguageTime'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("LanguageTime")}>
                    Language & time
                </Text>
                <Text bold={title=='Notifications'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("Notifications")}>
                    Notifications
                </Text>

                {role==='admin' &&
                    <Text bold={title=='Billing'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("Billing")}>
                        Billing
                    </Text>
                }
            </Block>
        )
    }
}
const styles = StyleSheet.create({
    text: {
        lineHeight: 30
    },
    aboutText: {
        lineHeight: 20,
        marginBottom: 15
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    }
});
export default withNavigation(AccountLinks);
