import React from "react";
import {Block, Text} from "galio-framework";
import {StyleSheet} from "react-native";
import {withNavigation} from "@react-navigation/compat";

class ManageEmployeesLinks extends React.Component {
    render() {
        const { navigation, route } = this.props;
        return (
            <Block>
                <Text bold={route.name=='Manage Employees'} size={13} color="#32325D" onPress={()=> {
                    navigation.navigate('Manage Employees')
                }}>
                    Employees <Text muted size={12}>can reply to surveys</Text>
                </Text>
                <Text bold={route.name=='Managers'} size={13} color="#32325D" style={styles.text} onPress={()=> {
                    navigation.navigate('Managers')
                }}>
                    Managers <Text muted size={12}>can view & manage survey details</Text>
                </Text>
                <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                    <Block style={styles.divider} />
                </Block>
                <Text bold={route.name=='Locations'} size={13} color="#32325D" style={styles.text} onPress={()=> {
                    navigation.navigate('Locations')
                }}>
                    Locations <Text muted size={12}>segment by offices & workplaces</Text>
                </Text>
                <Text bold={route.name=='Teams'} size={13} color="#32325D" style={styles.text} onPress={()=> {
                    navigation.navigate('Teams')
                }}>
                    Teams <Text muted size={12}>segment by departments & roles</Text>
                </Text>
            </Block>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        lineHeight: 30
    },
    aboutText: {
        lineHeight: 20,
        marginBottom: 15
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    }
});

export default withNavigation(ManageEmployeesLinks);
