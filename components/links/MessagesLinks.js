import React, {useState} from "react";
import {Block, Text, theme} from "galio-framework";
import {StyleSheet} from "react-native";
import {useNavigation} from "@react-navigation/native";

export default function MessagesLinks({role, route, conversations}) {
    const [revealText, setRevealText] = useState(false);
    const navigation = useNavigation();

    function renderLinks() {
        return (
            <Block style={{ marginTop: 10}}>
                <Text bold={route.name==='my_reply'} size={15} color="#32325D" style={styles.text} onPress={()=>navigation.navigate("my_reply", {refresh: true})}>
                    Waiting for my reply
                </Text>

                {(role === 'admin' || role === 'manager') ?
                    <Block>
                        <Text bold={route.name==='employee_reply'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("employee_reply", {params: conversations})}>
                            Waiting for employee's reply
                        </Text>
                        <Text bold={route.name==='resolved'} size={15} color="#32325D" style={styles.text} onPress={
                            () => navigation.navigate("resolved")}
                        >
                            Resolved
                        </Text>
                    </Block>
                    :
                    <Block>
                        <Text bold={route.name==='organization_reply'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("organization_reply", {params: conversations})}>
                            Waiting for organization's reply
                        </Text>
                        <Text bold={route.name==='archive'} size={15} color="#32325D" style={styles.text} onPress={() => navigation.navigate("archive")}>
                            Archive
                        </Text>
                    </Block>
                }
            </Block>
        )
    }

    return (
        <Block>
            <Block flex style={styles.MessageDetailsContainer}>
                { renderLinks() }
            </Block>

            <Block flex style={styles.MessageDetailsContainer}>
                <Block style={{ marginTop: 10}}>
                    <Text bold size={15} color="#36d79a" style={styles.text}>
                        Messages are 100% anonymous.
                    </Text>

                    <Text size={15} color="#36d79a" style={styles.text}>
                        This is a safe space for honest feedback.{'\n'}
                    </Text>

                    {(role === 'admin' || role === 'manager') ?
                        <Block>
                            <Text size={15} color="#32325D" style={styles.text}>
                                By excluding employees' names, you get the most value out of their feedback.{'\n'}
                            </Text>
                            <Text size={15} color="#32325D" style={styles.text}>
                                We inform your employees about vibestrive's conversation anonymity as well.
                            </Text>
                        </Block>
                        :
                        <Block>
                            <Text style={{marginBottom: 5}}>
                                Your organization can never see your name,
                                <Text muted> or other personal details.</Text>
                            </Text>

                            <Text onPress={()=> revealText ? setRevealText(false) : setRevealText(true)} style={{textDecorationLine: 'underline'}}>More about anonymity on vibestrive</Text>
                            {revealText &&
                                <Block>
                                    <Text style={styles.anonymityText}>
                                        What if I reveal my identity in a conversation? {'\n'}

                                        <Text style={styles.anonymitySubText}>Sure, no problem! Your other conversations will stay 100% anonymous.</Text>
                                    </Text>
                                    <Text style={styles.anonymityText}>
                                        What information can my organization see? {'\n'}

                                        <Text style={styles.anonymitySubText}>We show the following information to let your organization know where to work on improvements:</Text>
                                    </Text>
                                </Block>
                            }
                        </Block>
                    }
                </Block>
            </Block>
        </Block>
    )
}
const styles = StyleSheet.create({
    anonymityText: {
        fontSize: 12,
        fontWeight: 'bold',
        marginTop: 20,
        color: '#32325d'
    },
    anonymitySubText: {
        fontWeight: '100',
        marginTop: 10,
        color: '#8898aa'
    },
    MessageDetailsContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    text: {
        lineHeight: 30
    },
    aboutText: {
        lineHeight: 20,
        marginBottom: 15
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    }
});
