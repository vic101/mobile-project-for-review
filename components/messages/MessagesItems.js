import {Block, Text, theme} from "galio-framework";
import LocationTeamAgo from "./LocationTeamAgo";
import QuestionAndAnswer from "./QuestionAndAnswer";
import Conversations from "./Conversations";
import Actions from "./Actions";
import React from "react";

import {StyleSheet} from "react-native";


export default function MessagesItems(
    {
        route,
        role,
        conversation,
        index,
        handleUnResolve,
        resolved,
        handleResolve,
        handleRefresh,
        showActionButton
    }) {

    return (
        <Block flex style={styles.convDetailsContainer} key={index}>
            <Block flex>
                <Block>
                    <Text bold muted style={{marginBottom: 10}}>Category: {conversation.question?.sub_category?.category?.name ?? 'Happiness / Custom'}</Text>
                    <LocationTeamAgo
                        route={route}
                        role={role}
                        conversation={conversation}
                        parentIndex={index}
                        locations={conversation.conversations[0].user?.location_employees}
                        teams={conversation.conversations[0].user?.team_employees}
                        ago={resolved?null:conversation.conversations[conversation.conversations.length-1].created_at_ago}
                        unresolved={handleUnResolve}
                    />
                    <QuestionAndAnswer conversation={conversation}/>
                    <Conversations
                        role={role}
                        conversations={conversation.conversations}
                    />
                    {
                        showActionButton && (
                            <Actions
                                resolve={handleResolve}
                                refresh={handleRefresh}
                                conversation={conversation}
                                index={index}
                            />
                        )
                    }
                </Block>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    MessageDetailsContainer: {
        // position: "relative",
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    convDetailsContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
})
