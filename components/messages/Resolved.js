import React from "react";
import Details from "./Details";
export default function Resolved({ route, navigation, handleRefresh}) {
    return (
        <Details
            navigation={navigation}
            route={route}
        />
    )
}
