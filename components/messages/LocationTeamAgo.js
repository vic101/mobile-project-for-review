import {Block, Text} from "galio-framework";
import React, {useState} from "react";
import Ionicons from "@expo/vector-icons/Ionicons";
import {Menu, MenuItem} from "react-native-material-menu";

export default function LocationTeamAgo(
    {
        route,
        role,
        conversation,
        parentIndex,
        locations,
        teams,
        ago,
        unresolved
    }) {
    const [activeIndex, setActiveIndex] = useState(undefined);

    function handleUnresolved() {
        unresolved(conversation);
        setActiveIndex(undefined)
    }

    return (
        <Block style={role==='admin' && {marginBottom: 20}}>
            <Block style={{ flexDirection: 'row'}}>
                <Block style={{ flex: 6}}>
                    <Text>
                        {
                            locations?.map((location, index) => {
                                return (
                                    <Block key={index}>
                                        <Text muted style={{ fontSize: 12 }}>{index==0? 'Location:' :''} <Text bold>{(index ? ', ' : '') + location?.location?.name}</Text></Text>
                                    </Block>
                                )
                            })
                        }
                    </Text>
                </Block>
                <Block>
                    {ago ? (
                        <Text muted style={{ fontSize: 12 }}>
                            Last message: <Text>{ago}</Text>
                        </Text>
                    ):(
                        <Block>
                            <Ionicons
                                onPress={() => activeIndex !== parentIndex ? setActiveIndex(parentIndex) : setActiveIndex(undefined)}
                                name="ellipsis-vertical" size={14} color="#8898aa"
                            />
                            <Menu
                                visible={activeIndex === parentIndex} onRequestClose={() => setActiveIndex(undefined)}
                                row middle space="between"
                            >
                                <MenuItem
                                    onPress={ () => handleUnresolved() }
                                >un-resolve</MenuItem>
                            </Menu>
                        </Block>
                    )}
                </Block>
            </Block>
            <Block style={{ flexDirection: 'row'}}>
                <Block style={{ flex: 12}}>
                    <Text>
                        {
                            teams?.map((team, teamIndex) => {
                                return (
                                    <Block key={teamIndex}>
                                        <Text muted style={{ fontSize: 12 }}>{teamIndex==0? 'Team:' :''} <Text bold>{(teamIndex ? ', ' : '') + team?.team?.name}</Text></Text>
                                    </Block>
                                )
                            })
                        }
                    </Text>
                </Block>
            </Block>
            {(role === 'admin' && route === 'resolved') &&
                <Block style={{ flexDirection: 'row', marginTop: 10}}>
                    <Block style={{ flex: 12}}>
                        <Text muted style={{ fontSize: 13 }}>
                            First message: {conversation.conversations[0].created_at_readable} {'\n'}
                            Resolved: {conversation.updated_at_readable} {'\n'}
                            by {conversation.resolved_by}
                        </Text>
                    </Block>
                </Block>
            }
        </Block>
    )
}
