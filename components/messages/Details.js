import {Block, Text, theme} from "galio-framework";
import {Dimensions, ActivityIndicator, RefreshControl, FlatList} from "react-native";
import React, {useContext, useEffect, useRef, useState} from "react";
import {argonTheme} from "../../constants";
import TrialBanner from "../TrialBanner";
import MessagesLinks from "../links/MessagesLinks";
import axiosConfig from "../../helpers/axiosConfig";
import {AuthContext} from "../../context/AuthProvider";
import MessagesItems from "./MessagesItems";
import styles from '../../screens/Styles/MessagesScreen.style';
import Filters from "./Filters";
import Spinner from "react-native-loading-spinner-overlay";

const { width, height } = Dimensions.get("screen");

export default function Details({ route, navigation }) {
    const flatListRef = useRef();
    const { user } = useContext(AuthContext);
    const [conversations, setConversations] = useState([])
    const [locations, setLocations] = useState([])
    const [teams, setTeams] = useState([])
    const [categories, setCategories] = useState([])

    const [category, setCategory] = useState(null)
    const [selectedCategory, setSelectedCategory] = useState(null)

    const [location, setLocation] = useState(null)
    const [selectedLocation, setSelectedLocation] = useState(null)

    const [team, setTeam] = useState(null)
    const [selectedTeam, setSelectedTeam] = useState(null)

    const [dateSort, setSelectedDate] = useState(null)

    const [isLoading, setIsLoading] = useState(true);
    const [loading, setLoading] = useState(true);

    const [isRefreshing, setIsRefreshing] = useState(false);
    const [isAtEndOfScrolling, setIsAtEndOfScrolling] = useState(false);
    const [page, setPage] = useState(1);
    const filterOption = [
        {
            key: 'desc',
            name: 'Newest first'
        },
        {
            key: 'asc',
            name: 'Oldest first'
        }
    ]
    useEffect(()=> {
        fetchConversations()
    }, [page,category, team, location, dateSort])

    function fetchConversations() {
        setLoading(true)

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        let attributes = null;

        if (route.name === 'resolved') {
            attributes = {
                order: 'asc',
                status: 'resolved',
                questionType: 'fixed',
                // category: category,
                // location: location,
                // team: team,
                paginate: 10
            }
        }
        else if (route.name === 'archive') {
            attributes = {
                order: 'asc',
                conversationType: 'employeeToOrganization',
                status: 'resolved',
                questionType: 'fixed',
                paginate: 10
            }
        }
        else {
            if (user.role === 'admin' || user.role === 'manager') {
                attributes = {
                    status: 'unresolved',
                    questionType: 'fixed',
                    waitingReply: route.name,
                    order: dateSort,
                    category: category ?? null,
                    location: location ?? null,
                    team: team ?? null,
                    keyword: '',
                    paginate: 5
                }
            }
            if (user.role === 'employee') {
                attributes = {
                    order: 'asc',
                    status: 'unresolved',
                    questionType: 'fixed',
                    waitingReply: route.name,
                    conversationType: 'employeeToOrganization',
                    keyword: '',
                    paginate: 10
                }
            }
        }

        axiosConfig.get(`/conversation-api?page=${page}`, { params: attributes }).then((response) => {
            setNewDropDownData(response.data.categories, 'category')
            setNewDropDownData(response.data.locations, 'location')
            setNewDropDownData(response.data.teams, 'team')

            if (page === 1) {
                setConversations(response.data.conversation.data)
            } else {
                setConversations([...conversations, ...response.data.conversation.data])
            }

            if (! response.data.conversation?.links?.next) {
                setIsAtEndOfScrolling(true);
            }

            setIsLoading(false)
            setIsRefreshing(false);
            setLoading(false)
        }).catch(error => {
            console.log('error: ', error.response)
            setIsLoading(false)
            setIsRefreshing(false);
        })
    }
    function setNewDropDownData (data, iWhich) {
        let items = [];
        switch (iWhich) {
            case 'category':
                items.push(
                    {
                        id: null,
                        name: 'All topics'
                    },
                    {
                        id: 'happiness',
                        name: 'Happiness'
                    },
                    {
                        id: 'custom',
                        name: 'Custom'
                    }
                );
                break;
            case 'location':
                items.push({id: null,name: 'All locations'});
                break;
            case 'team':
                items.push({id: null,name: 'All teams'});
                break;
        }

        data.map(item => {
            items.push({
                id: item.id,
                name: item.name
            })
        })

        iWhich === 'category' && setCategories(items)
        iWhich === 'location' && setLocations(items)
        iWhich === 'team' && setTeams(items)
    }
    function handleEnd() {
        setPage(page + 1);
        console.log('handle end executed...')
    }
    function handleRefresh() {
        setPage(1);
        setIsAtEndOfScrolling(false);
        setIsRefreshing(true);
        fetchConversations()
    }

    function unResolve (conversation) {
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;
        let notification = 'Message has been moved to waiting reply.'

        let attribute = {
            status: 'unresolved',
            messaging: true,
            custom: conversation.question_type=='custom'?true:false
        }

        axiosConfig.patch('/question-conversation-api/' + conversation.id, attribute).then(function (response) {
            handleRefresh()
            console.log('handleRefresh...')
            alert(notification)

        }).catch(error => {
            console.log(error);
        });
    }
    const renderItems = (item, index) => (
        <MessagesItems
            route={route.name}
            role={user.role}
            conversation={item}
            index={index}
            handleUnResolve={unResolve}
            resolved={route.name=='resolved'?true:false}
            // handleResolve={handleResolve}
            handleRefresh={handleRefresh}
            showActionButton={route.name=='my_reply'?true:false}

        />
    )
    const renderHeader = () => (
        <Block>
            <MessagesLinks
                role={user.role}
                route={route}
            />
            <Block style={styles.dropDownContainer}>
                <Filters
                    role={user.role}
                    categories={categories}
                    locations={locations}
                    teams={teams}

                    setCategory={setCategory}
                    setSelectedCategory={setSelectedCategory}
                    selectedCategory={selectedCategory}

                    setLocation={setLocation}
                    setSelectedLocation={setSelectedLocation}
                    selectedLocation={selectedLocation}

                    setTeam={setTeam}
                    setSelectedTeam={setSelectedTeam}
                    selectedTeam={selectedTeam}

                    filterOption={filterOption}
                    setSelectedDate={setSelectedDate}
                />
            </Block>
        </Block>
    )

    const renderFooter = () => (
        <Block style={styles.footerText}>
            {!isAtEndOfScrolling && <ActivityIndicator size="large" color="gray"/>}
            {isAtEndOfScrolling && <Text>No more messages at the moment</Text>}
        </Block>
    )

    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <Block flex>
                {user.role === 'admin' && <TrialBanner navigation={navigation}/>}
                {loading &&
                    <Spinner
                        visible={loading}
                        textContent={'Please Wait...'}
                        textStyle={styles.spinnerTextStyle}
                    />
                }
                {isLoading ? (
                    <Spinner
                        visible={loading}
                        textContent={'Please Wait...'}
                        textStyle={styles.spinnerTextStyle}
                    />
                ) : (
                    <FlatList
                        ref={flatListRef}
                        keyExtractor={item => item.id.toString()}
                        data={conversations}
                        renderItem={({ item, index }) => renderItems(item, index)}
                        refreshing={isRefreshing}
                        onRefresh={handleRefresh}
                        onEndReached={handleEnd}
                        onEndReachedThreshold={0.2}
                        ListHeaderComponent={renderHeader}
                        ListFooterComponent={renderFooter}
                    />
                ) }
            </Block>
        </Block>
    );
}
