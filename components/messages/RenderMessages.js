import {Block, Text, theme} from "galio-framework";
import LocationTeamAgo from "./LocationTeamAgo";
import QuestionAndAnswer from "./QuestionAndAnswer";
import Conversations from "./Conversations";
import Actions from "./Actions";
import React, {useRef, useState} from "react";
import {Dimensions, FlatList, StyleSheet, ActivityIndicator} from "react-native";
import {Button} from "../index";
import {argonTheme} from "../../constants";
import MessagesItems from "./MessagesItems";
import MessagesLinks from "../links/MessagesLinks";

const { width, height } = Dimensions.get("screen");
export default function renderMessages(
    {
        role,
        route,
        conversations,
        showActionButton,
        resolved,
        handleUnResolve,
        handleResolve,
        handleRefresh,
        isRefreshing,
        handleEnd,
        isAtEndOfScrolling
    }) {

    const flatListRef = useRef();

    return (
        <Block>
            <FlatList
                ref={flatListRef}
                keyExtractor={item => item.id.toString()}
                data={conversations}
                refreshing={isRefreshing}
                onRefresh={handleRefresh}
                onEndReached={handleEnd}
                onEndReachedThreshold={0}
                renderItem={({ item, index }) => (
                    <MessagesItems
                        role={role}
                        conversation={item}
                        index={index}
                        handleUnResolve={handleUnResolve}
                        resolved={resolved}
                        handleResolve={handleResolve}
                        handleRefresh={handleRefresh}
                        showActionButton={showActionButton}

                    />
                )}
                ListHeaderComponent={
                    <MessagesLinks
                        role={role}
                        route={route}
                    />
                }
                ListFooterComponent={() =>
                    !isAtEndOfScrolling && (
                        <ActivityIndicator size="large" color="gray" />
                    )
                }
            />
        </Block>
    )
}

const styles = StyleSheet.create({
    MessageDetailsContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 10,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    }
})
