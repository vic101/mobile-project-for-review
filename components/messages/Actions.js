import {Block, theme} from "galio-framework";
import {TextInput, StyleSheet, Dimensions, ActivityIndicator} from "react-native";
import {Button} from "../index";
import React, {useContext, useEffect, useState} from "react";
import {argonTheme} from "../../constants";
import axiosConfig from "../../helpers/axiosConfig";
import {AuthContext} from "../../context/AuthProvider";

const { width, height } = Dimensions.get("screen");

export default function Actions({conversation, resolve, refresh}) {

    const [messages, setMessages] = useState('');
    const [status, setStatus]     = useState('replied');
    const [isLoading, setIsLoading] = useState(false);
    const [sending, setSending] = useState(false);

    const { user } = useContext(AuthContext);

    function send () {
        setIsLoading(true)
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attributes = {
            state                 : 'fixed-question',
            messaging             : true,
            questionConversationId: conversation.id,
            questionId            : conversation.question_id,
            answerId              : conversation.answer_id,
            message               : messages,
            status                : status,
            owner                 : conversation.owner
        }

        if (user.role === 'employee') {
            attributes.companyId = conversation.company_id;
        }

        axiosConfig.post('/conversation-api', attributes).then(function (response) {
            setIsLoading(false)
            refresh()
        }).catch(error => {
            // const key = Object.keys(error.response.data.errors)[0];
            // setError(error.response.data.errors[key][0]);
            // setIsLoading(false);
        });
    }

    function resolve () {
        setIsLoading(true)
        axiosConfig.defaults.headers.common[
            'Authorization'
            ] = `Bearer ${user.token}`;

        let attribute = {
            status: 'resolved',
            messaging: true
        }

        axiosConfig.patch('/question-conversation-api/' + conversation.id, attribute).then(function (response) {
            setIsLoading(false)
            refresh()
        })
            .catch(error => {
                console.log('Error: ', error)
                setIsLoading(false);
        });
    }
    return (
        <Block>
            {isLoading ? (
                <ActivityIndicator
                    size="small"
                    color="gray"
                    style={{ marginRight: 8, marginTop: 10 }}
                />
            ):(
                <Block>
                    <Block style={{ flexDirection: 'row' }}>
                        <Block style={{ flex: 12, marginBottom: 5 }}>
                            <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                                <TextInput
                                    onChangeText={setMessages}
                                    multiline
                                    placeholder="Type a message"
                                    style={styles.input} />
                            </Block>
                        </Block>
                    </Block>
                    <Block style={{ flexDirection: 'row' }}>
                        <Block style={{ flex: 12 }} center>
                            <Button color="default" style={styles.button} onPress={()=> send()}>
                                Send
                            </Button>
                        </Block>
                    </Block>
                    {user.role === 'admin' &&
                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 12 }} center>
                                <Button color="success" style={styles.button} onPress={() => resolve()}>
                                    Mark as resolved
                                </Button>
                            </Block>
                        </Block>
                    }
                </Block>
            )}
        </Block>
    )
}

const styles = StyleSheet.create({
    input: {
        maxHeight: 100,
        minHeight: 45,
        marginTop: 10,
        borderRadius: 4,
        backgroundColor: '#edeff2',
        shadowColor: argonTheme.COLORS.BLACK,
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 2,
        shadowOpacity: 0.05,
        elevation: 2,
        paddingLeft: 8,
        paddingRight: 8,
    },
    button: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 6,
        borderRadius: 6
    },
})
