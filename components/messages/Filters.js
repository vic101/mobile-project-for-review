import {Block, Text} from "galio-framework";
import styles from "../../screens/Styles/MessagesScreen.style";
import SelectDropdownMenu from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";
import React, {useEffect, useState} from "react";


export default function Filters (
    {
        role,
        categories,
        locations,
        teams,

        setCategory,
        setSelectedCategory,
        selectedCategory,

        setSelectedLocation,
        selectedLocation,
        setLocation,

        setTeam,
        setSelectedTeam,
        selectedTeam,

        filterOption,
        setSelectedDate
    }
    ) {

        return (
            <Block>
                {role === 'employee' ?
                    <Block row center space="between">
                        <Block flex middle right>
                            <Block style={styles.dropdownsRowEmployee}>
                                 <SelectDropdownMenu
                                    data={filterOption}
                                    onSelect={(selectedItem, index) => {
                                        setSelectedDate(selectedItem.key)
                                    }}
                                    defaultValueByIndex={1}
                                    buttonTextAfterSelection={(selectedItem, index) => {
                                        return selectedItem.name;
                                    }}
                                    rowTextForSelection={(item, index) => {
                                        return item.name;
                                    }}
                                    buttonStyle={styles.dropdown1BtnStyle}
                                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                    renderDropdownIcon={isOpened => {
                                        return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                                    }}
                                    dropdownIconPosition={'right'}
                                    dropdownStyle={styles.dropdown1DropdownStyle}
                                    rowStyle={styles.dropdown1RowStyle}
                                    rowTextStyle={styles.dropdown1RowTxtStyle}
                                />
                            </Block>
                        </Block>
                    </Block>
                    :
                    <Block>
                        <Block style={styles.dropdownsRow}>
                            <SelectDropdownMenu
                                data={categories}
                                onSelect={(selectedItem, index) => {
                                    setCategory(selectedItem.id)
                                    setSelectedCategory(selectedItem.name)
                                }}
                                defaultButtonText={selectedCategory === null ? categories[0]?.name : selectedCategory}
                                rowTextForSelection={(item, index) => {
                                    return item.name;
                                }}
                                buttonStyle={styles.dropdown1BtnStyle}
                                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                renderDropdownIcon={isOpened => {
                                    return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                                }}
                                dropdownIconPosition={'right'}
                                dropdownStyle={styles.dropdown1DropdownStyle}
                                rowStyle={styles.dropdown1RowStyle}
                                rowTextStyle={styles.dropdown1RowTxtStyle}
                            />
                        </Block>
                        <Block style={styles.dropdownsBottom}>
                            <SelectDropdownMenu
                                data={locations}
                                onSelect={(selectedItem, index) => {
                                    setSelectedLocation(selectedItem.name)
                                    setLocation(selectedItem.id)
                                }}
                                defaultButtonText={selectedLocation === null ? locations[0]?.name : selectedLocation}
                                buttonTextAfterSelection={(selectedItem, index) => {
                                    return selectedItem.name;
                                }}
                                rowTextForSelection={(item, index) => {
                                    return item.name;
                                }}
                                buttonStyle={styles.dropdown1BtnStyle}
                                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                renderDropdownIcon={isOpened => {
                                    return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                                }}
                                dropdownIconPosition={'right'}
                                dropdownStyle={styles.dropdown1DropdownStyle}
                                rowStyle={styles.dropdown1RowStyle}
                                rowTextStyle={styles.dropdown1RowTxtStyle}
                            />
                            <Block style={styles.divider}/>
                            <SelectDropdownMenu
                                data={teams}
                                onSelect={(selectedItem, index) => {
                                    setTeam(selectedItem.id)
                                    setSelectedTeam(selectedItem.name)
                                }}
                                defaultButtonText={selectedTeam === null ? teams[0]?.name : selectedTeam}
                                rowTextForSelection={(item, index) => {
                                    return item.name;
                                }}
                                buttonStyle={styles.dropdown1BtnStyle}
                                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                renderDropdownIcon={isOpened => {
                                    return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                                }}
                                dropdownIconPosition={'right'}
                                dropdownStyle={styles.dropdown1DropdownStyle}
                                rowStyle={styles.dropdown1RowStyle}
                                rowTextStyle={styles.dropdown1RowTxtStyle}
                            />
                            <Block style={styles.divider}/>
                            <SelectDropdownMenu
                                data={filterOption}
                                onSelect={(selectedItem, index) => {
                                    setSelectedDate(selectedItem.key)
                                }}
                                defaultValueByIndex={1}
                                buttonTextAfterSelection={(selectedItem, index) => {
                                    return selectedItem.name;
                                }}
                                rowTextForSelection={(item, index) => {
                                    return item.name;
                                }}
                                buttonStyle={styles.dropdown1BtnStyle}
                                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                renderDropdownIcon={isOpened => {
                                    return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                                }}
                                dropdownIconPosition={'right'}
                                dropdownStyle={styles.dropdown1DropdownStyle}
                                rowStyle={styles.dropdown1RowStyle}
                                rowTextStyle={styles.dropdown1RowTxtStyle}
                            />
                        </Block>
                    </Block>
                }
            </Block>
        )
}
