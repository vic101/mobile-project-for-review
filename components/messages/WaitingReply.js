import React from "react";
import Details from "./Details";
export default function WaitingReply({ route, navigation}) {
    return (
        <Details
            navigation={navigation}
            route={route}
        />
    )
}
