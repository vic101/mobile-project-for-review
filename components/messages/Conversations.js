import {Block, Text, theme} from "galio-framework";
import React from "react";
import {Dimensions, StyleSheet} from "react-native";


const { width, height } = Dimensions.get("screen");
export default function Conversations ({ conversations, role }) {

    return (
        conversations.map((message, messageIndex) => {
            return (
                <Block key={messageIndex}>
                    {(role === 'admin' || role === 'manager') ?
                        <Block >
                            {message.user?.role_name !== 'employee' ? (
                                <Block right>
                                    <Block style={styles.messagesContainer}>
                                        <Block style={styles.outgoingMessage}>
                                            <Text style={styles.outGoingText}>{ message.message }</Text>

                                            <Block style={{ alignSelf: 'flex-end', marginTop: 10, marginBottom: -10}}>
                                                <Text size={12} style={styles.outGoingText}>{message.created_at_readable}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            ) : (
                                <Block left>
                                    <Block style={styles.messagesContainer}>
                                        <Block style={styles.incomingMessage}>
                                            <Text style={styles.incomingText}>{ message.message }</Text>

                                            <Block style={{ alignSelf: 'flex-end', marginTop: 10, marginBottom: -10}}>
                                                <Text size={12} muted>{message.created_at_readable}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            )}
                        </Block>
                        :
                        <Block >
                            {message.user?.role_name === 'employee' ? (
                                <Block right>
                                    <Block style={styles.messagesContainer}>
                                        <Block style={styles.outgoingMessage}>
                                            <Text size={15} style={styles.outGoingText}>{ message.message }</Text>

                                            <Block style={{ alignSelf: 'flex-end', marginTop: 10, marginBottom: -10}}>
                                                <Text size={12} style={styles.outGoingText}>{message.created_at_readable}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            ) : (
                                <Block  left>
                                    <Block style={styles.messagesContainer}>
                                        <Block style={styles.incomingMessage}>
                                            <Text size={15} style={styles.incomingText}>{ message.message }</Text>

                                            <Block style={{ alignSelf: 'flex-end', marginTop: 10, marginBottom: -10}}>
                                                <Text size={12} muted>{message.created_at_readable}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            )}
                        </Block>
                    }

                </Block>
            )
        })
    )
}

const styles = StyleSheet.create({
    messagesContainer: {
        width: width - theme.SIZES.BASE * 10
    },
    incomingMessage: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderRadius: 6,
        backgroundColor: '#edeff2',
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    outgoingMessage: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderRadius: 6,
        backgroundColor: '#36d79a',
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    outGoingText: {
        color: '#ffffff'
    },
    incomingText: {
        color: '#32325d'
    },
})
