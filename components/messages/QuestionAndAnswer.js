import {Block, Text} from "galio-framework";
import React from "react";


export default function questionAndAnswer({ conversation }) {
    return (
        <Block>
            <Block style={{ flexDirection: 'row'}}>
                <Block style={{ flex: 3}}>
                    <Text muted style={{ fontSize: 13 }}>Question:</Text>
                </Block>
                <Block style={{ flex: 9}}>
                    <Text muted style={{ fontSize: 13 }}>
                        {conversation.question.text}
                    </Text>
                </Block>
            </Block>
            <Block style={{ flexDirection: 'row'}}>
                <Block style={{ flex: 3}}>
                    <Text style={{ fontSize: 13 }} muted>Answer:</Text>
                </Block>
                <Block style={{ flex: 9}}>
                    <Text muted style={{ fontSize: 13 }}>
                        {conversation?.answer?.text}
                    </Text>
                </Block>
            </Block>
        </Block>
    )
}
