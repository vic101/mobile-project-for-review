import React, {useContext, useEffect, useState} from "react";
import {AuthContext} from "../../context/AuthProvider";
import {Block, Text, theme} from "galio-framework";
import {ActivityIndicator, Alert, Dimensions, ScrollView, StyleSheet, TouchableOpacity} from "react-native";
import Ionicons from "@expo/vector-icons/Ionicons";
import {argonTheme} from "../../constants";
import {Button, Icon, Input} from "../index";
import {FontAwesome} from "@expo/vector-icons";
import {StripeProvider, CardField, useStripe, useConfirmSetupIntent} from '@stripe/stripe-react-native';
import axiosConfig from "../../helpers/axiosConfig";
import axios from "axios";
import {API_URL} from "../../Config";
import SelectDropdown from "react-native-select-dropdown";

const { width, height } = Dimensions.get("screen");
export default function AccountNoBilling ({ route, navigation, reload, countries}) {
    const { user } = useContext(AuthContext);
    const { confirmSetupIntent, loading } = useConfirmSetupIntent();
    const [streetNo, setStreetNo]  = useState('');
    const [addressTwo, setAddressTwo]  = useState('');
    const [countryId, setCountryId]  = useState('');
    const [city, setCity]  = useState('');
    const [vatNo, setVatNo]  = useState('');
    const [isValidVat, setIsValidVat]  = useState('');
    const [postalCode, setPostalCode]  = useState('');
    const [internalRef, setInternalRef]  = useState('');
    const [updatingCard, setUpdatingCard] = useState(false);
    const [company, setCompany] = useState('');
    const [cardComplete, setCardComplete] = useState(false);

    useEffect(() => {
        setCompany(user.company);
    }, [])

    const fetchPaymentIntentClientSecret = async () => {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        const response = await axiosConfig.post(`/payment-sheet`)
            .catch(error => {
                console.log('Error: ', error.response.data)
                setUpdatingCard(false)
                alert('Error: '+ error.response.data)
            });

        if (typeof (response.data) == 'undefined') {
            alert('Hello')
        } else {
            return response.data;
        }
    };

    const handlePayPress = async () => {
        if (cardComplete) {
            try {
                setUpdatingCard(true)
                // Gather the customer's billing information (for example, email)
                const billingDetails: BillingDetails = {
                    email: 'test.card@example.com',
                };

                // Create a setup intent on the backend
                const clientSecret = await fetchPaymentIntentClientSecret();
                console.log('clientSecret: ', clientSecret)

                const { setupIntent, error } = await confirmSetupIntent(clientSecret, {
                    type: 'Card'
                });

                if (error) {
                    Alert.alert(`Error code: ${error.code}`, error.message);
                } else {
                    handleSaveBillingDetails(setupIntent.paymentMethodId);
                }
            } catch (e) {
                console.log(e)
            }
        } else {
            Alert.alert('Error', 'Please fill out credit card details completely.');
        }
    };
    function handleSaveBillingDetails (paymentMethodId) {
        let attributes = {
            paymentMethod: paymentMethodId,
            streetNo: streetNo,
            address2: addressTwo,
            country_id: countryId,
            city: city,
            vatNo: vatNo,
            postalCode: postalCode,
            internal_reference: internalRef
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.post(`${API_URL}/save-billing-details`, attributes).then(response => {
            setUpdatingCard(false)
            Alert.alert('Success',response.data);
            reload()
        })
            .catch(error => {
                console.log('Error: ', error)
                alert('Error: '+ error.response.data)
                setUpdatingCard(false)
            });
    }

    function renderDescription() {
        return (
            <Block>
                <Block flex style={styles.listContainer}>
                    <Block>
                        <Text bold size={13} color="#32325D" style={styles.billingDetails}>
                            All features, more happiness & loyalty for only <Text color="#36d79a">5€ monthly</Text> per employee.
                        </Text>
                        <Text bold size={13} color="#32325D" style={{ marginBottom: 10}}>
                            Enter your billing details now:
                        </Text>
                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 1}}>
                                <Ionicons
                                    name="checkmark-sharp" size={18} color="#36d79a"
                                />
                            </Block>
                            <Block style= {{ flex: 14 }}>
                                <Text size={13} style={{ marginBottom: 10}}>
                                    <Text color="#36d79a">Your 30 day trial will continue</Text> even after you entered your payment details
                                </Text>
                            </Block>
                        </Block>

                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 1}}>
                                <Ionicons
                                    name="checkmark-sharp" size={18} color="#36d79a"
                                />
                            </Block>
                            <Block style= {{ flex: 14 }}>
                                <Text size={13} style={{ marginBottom: 10}}>
                                    <Text color="#36d79a">Smooth transition:</Text> After your trial ends, you and your employees can continue using vibestrive without interruption.
                                </Text>
                            </Block>
                        </Block>

                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 1}}>
                                <Ionicons
                                    name="checkmark-sharp" size={18} color="#36d79a"
                                />
                            </Block>
                            <Block style= {{ flex: 14 }}>
                                <Text size={13} style={{ marginBottom: 10}}>
                                    <Text color="#36d79a">No costs if you are unhappy:</Text> Cancel before the end of your 30 day trial and we will not charge you anything.
                                </Text>
                            </Block>
                        </Block>
                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 1}}>
                                <Ionicons
                                    name="checkmark-sharp" size={18} color="#36d79a"
                                />
                            </Block>
                            <Block style= {{ flex: 14 }}>
                                <Text size={13} style={{ marginBottom: 10}}>
                                    <Text color="#36d79a">100% fair:</Text> No minimum or hidden fees, no setup costs.
                                </Text>
                            </Block>
                        </Block>
                        <Block style={{ flexDirection: 'row' }}>
                            <Block style={{ flex: 1}}>
                                <Ionicons
                                    name="checkmark-sharp" size={18} color="#36d79a"
                                />
                            </Block>
                            <Block style= {{ flex: 14 }}>
                                <Text size={13} color="#36d79a" style={{ marginBottom: 10}}>
                                    Cancel anytime.
                                </Text>
                            </Block>
                        </Block>
                    </Block>
                </Block>
            </Block>
        )
    }
    function renderBillingAddress() {
        return (
            <Block style={{ marginBottom: 20}}>
                <Block flex style={styles.listContainer}>
                    <Block>
                        <Block style={{ flexDirection: 'row', marginBottom: 10}}>
                            <Block style={{ flex: 6 }}>
                                <Text bold size={13} color="#32325D">
                                    Billing address
                                </Text>
                            </Block>
                            <Block style={{ flexDirection: 'row'}}>
                                <Block>
                                    <FontAwesome style={{ paddingRight: 5, paddingTop: 2}} name="lock" size={14} color={argonTheme.COLORS.SUCCESS}/>
                                </Block>
                                <Block>
                                    <Text size={12} color={argonTheme.COLORS.SUCCESS}>
                                        secure connection
                                    </Text>
                                </Block>
                            </Block>
                        </Block>

                        <Block>
                            <Input style={styles.input} editable={false}  value={company} placeholder={'Company'} iconContent={<Block />}/>
                        </Block>
                        <Block>
                            <Input style={styles.input} onChangeText={setStreetNo} value={streetNo} placeholder={'Street, No.'} iconContent={<Block />}/>
                        </Block>
                        <Block>
                            <Input style={styles.input} onChangeText={setAddressTwo} value={addressTwo} placeholder={'Address line 2 (optional)'} iconContent={<Block />}/>
                        </Block>
                        <Block style={{ flexDirection: 'row'}}>
                            <Block style={{ flex: 0.5, marginRight: 5}}>
                                <Input style={styles.input} onChangeText={setPostalCode} value={postalCode} placeholder={'Postal code'} iconContent={<Block />}/>
                            </Block>
                            <Block flex>
                                <Input style={styles.input} onChangeText={setCity} value={city} placeholder={'City'} iconContent={<Block />}/>
                            </Block>
                        </Block>
                        <Block style={styles.dropdownsRow}>
                            <SelectDropdown
                                data={countries}
                                onSelect={(selectedItem, index) => {
                                    console.log(selectedItem);
                                    setCountryId(selectedItem.id)
                                }}
                                defaultButtonText={'Countries'}
                                buttonTextAfterSelection={(selectedItem, index) => {
                                    return selectedItem.name;
                                }}
                                rowTextForSelection={(item, index) => {
                                    return item.name;
                                }}
                                buttonStyle={styles.dropdown1BtnStyle}
                                buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                renderDropdownIcon={isOpened => {
                                    return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={12} />;
                                }}
                                dropdownIconPosition={'right'}
                                dropdownStyle={styles.dropdown1DropdownStyle}
                                rowStyle={styles.dropdown1RowStyle}
                                rowTextStyle={styles.dropdown1RowTxtStyle}
                            />
                        </Block>
                        <Block>
                            <Input style={styles.input} onChangeText={setVatNo} value={vatNo} placeholder={'VAT No. (optional)'} iconContent={<Block />}/>
                        </Block>
                        <Block>
                            <Input style={styles.input} onChangeText={setInternalRef} value={internalRef} placeholder={'Internal reference (optional)'} iconContent={<Block />}/>
                        </Block>
                    </Block>
                </Block>
            </Block>
        );
    }
    function renderCreditCard() {
        return (
            <Block style={{ marginBottom: 20}}>
                <Block flex style={styles.listContainer}>
                    <Block>
                        <Block style={{ flexDirection: 'row', marginBottom: 10}}>
                            <Block style={{ flex: 6 }}>
                                <Text bold size={13} color="#32325D">
                                    Credit card
                                </Text>
                            </Block>
                            <Block style={{ flexDirection: 'row'}}>
                                <Block>
                                    <FontAwesome style={{ paddingRight: 5, paddingTop: 2}} name="lock" size={14} color={argonTheme.COLORS.SUCCESS}/>
                                </Block>
                                <Block>
                                    <Text size={12} color={argonTheme.COLORS.SUCCESS}>
                                        secure connection
                                    </Text>
                                </Block>
                            </Block>
                        </Block>

                        <Block>
                            <CardField
                                postalCodeEnabled={true}
                                placeholder={{
                                    number: '4242 4242 4242 4242',
                                }}
                                cardStyle={{
                                    backgroundColor: '#edeff2',
                                    textColor: '#000000',
                                    borderWidth: 1,
                                    borderColor: '#000000',
                                    borderRadius: 8
                                }}
                                style={styles.cardInput}
                                onCardChange={(cardDetails) => {
                                    setCardComplete(cardDetails.complete)
                                }}
                                onFocus={(focusedField) => {
                                    console.log('focusField', focusedField);
                                }}
                            />
                        </Block>
                        <Block>
                            <Text color={argonTheme.COLORS.SUCCESS}>
                                <Text>Secure payments with stripe. {"\n"}{"\n"}</Text>
                                <Text bold>You do not pay now.{"\n"}</Text>
                                <Text>After your trial ends, you will pay 5 € monthly for each active employee on the 1st day of your billing month.{"\n"}{"\n"}</Text>

                                <Text bold>You can cancel your account anytime.{"\n"}</Text>
                                <Text>If you cancel your account during your trial. you do not pay anything.</Text>
                            </Text>
                        </Block>
                    </Block>
                </Block>
            </Block>
        );
    }

    function renderSaveButton() {
        return (
            <Block style={{ marginBottom: 20}}>
                <Block flex style={styles.listContainer}>
                    <Block>
                        <Block>
                            <Block style={{ flexDirection: 'row' }}>
                                <Block style={{ flex: 12 }} center>

                                    <Button
                                        color="success"
                                        style={updatingCard ? styles.buttonDisabled : styles.buttonEnabled}
                                        variant="primary"
                                        disabled={updatingCard}
                                        onPress={handlePayPress}
                                    >
                                        {updatingCard ? 'Saving details please wait...':'Save my billing details'}
                                    </Button>
                                    <Text color={argonTheme.COLORS.SUCCESS}>You do not pay now – cancel anytime</Text>
                                </Block>
                            </Block>
                        </Block>
                    </Block>
                </Block>

                <Block>
                    <Text style={styles.cancelAccount}>Not happy with vibestrive? <Text style={{ textDecorationLine: 'underline'}} onPress={()=> navigation.navigate('CancelBilling')}>Cancel account</Text></Text>
                </Block>
            </Block>
        );
    }
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                { renderDescription() }
                { renderBillingAddress() }
                { renderCreditCard() }
                { renderSaveButton() }
            </ScrollView>
        </Block>
    )
}

const styles = StyleSheet.create({
    dropdownsRow: {flexDirection: 'row', width: '100%', paddingTop: '2%', paddingBottom: '2%'},
    dropdown1BtnStyle: {
        flex: 1,
        height: 40,
        backgroundColor: '#edeff2',
        borderRadius: 8,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 14},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left'},
    buttonEnabled: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 6,
        borderRadius: 6
    },
    buttonDisabled: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 6,
        borderRadius: 6,
        backgroundColor: '#76dfb2'
    },
    input: {
        backgroundColor: '#edeff2',
        borderColor: '#edeff2'
    },
    cardInput: {
        borderColor: '#edeff2',
        width: '100%',
        height: 50,
        marginVertical: 30
    },
    secureConnection: {
        textAlign: "center",
        paddingBottom: 30
    },
    billingDetails: {
        marginBottom: 20
    },
    cancelAccount: {
        padding: theme.SIZES.BASE,
        marginTop: -10,
        textAlign: 'center',
        color: '#8898aa'
    },
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        // marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
});
