import {Block} from "galio-framework";
import SelectDropdownMenu from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";
import React from "react";
import {StyleSheet} from "react-native";


export default function LanguageTime({languages, language, which, timezones, timezone}) {
    return (
        <Block flex left style={{marginTop: 8}}>
            <Block style={styles.dropdownsRow}>
                <SelectDropdownMenu
                    data={which==='language' ? languages : timezones}
                    onSelect={(selectedItem, index) => {
                        console.log(selectedItem, index);
                        // setSelectedLanguage(selectedItem.language)
                    }}
                    defaultButtonText={which==='language' ? language : timezone}
                    buttonTextAfterSelection={(selectedItem, index) => {
                        return selectedItem.name;
                    }}
                    rowTextForSelection={(item, index) => {
                        return item.name;
                    }}
                    buttonStyle={styles.dropdown1BtnStyle}
                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                        return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={20} />;
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={styles.dropdown1DropdownStyle}
                    rowStyle={styles.dropdown1RowStyle}
                    rowTextStyle={styles.dropdown1RowTxtStyle}
                />
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    dropdownsRow: {flexDirection: 'row', width: '100%', paddingTop: '2%', paddingBottom: '2%'},
    dropdown1BtnStyle: {
        flex: 1,
        height: 45,
        backgroundColor: '#FFF',
        borderRadius: 8,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 14},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left'},
});
