import {CardField} from "@stripe/stripe-react-native";
import React from "react";
import {StyleSheet} from "react-native";


export default function Card ({ setCardComplete }) {

    return (
        <CardField
            postalCodeEnabled={true}
            placeholder={{
                number: '4242 4242 4242 4242',
            }}
            cardStyle={{
                backgroundColor: '#edeff2',
                textColor: '#000000',
                borderWidth: 1,
                borderColor: '#000000',
                borderRadius: 8
            }}
            style={styles.cardInput}
            onCardChange={(cardDetails) => {
                setCardComplete(cardDetails.complete)
                console.log(cardDetails.complete)
            }}
            onFocus={(focusedField) => {
                console.log('focusField', focusedField);
            }}
        />
    )
}

const styles = StyleSheet.create({
    cardInput: {
        borderColor: '#edeff2',
        width: '100%',
        height: 50
    }
});
