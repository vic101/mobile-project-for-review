// import {useConfirmSetupIntent, useStripe} from "@stripe/stripe-react-native";
// import React, {useContext, useEffect, useState} from "react";
// import {Block, theme} from "galio-framework";
// import {Button} from "../index";
// import {Alert, Dimensions, StyleSheet} from "react-native";
// import axiosConfig from "../../helpers/axiosConfig";
// import {AuthContext} from "../../context/AuthProvider";
// const { user } = useContext(AuthContext);
//
// const { width, height } = Dimensions.get("screen");
// export default function SaveCard() {
//     const [clientSecret, setClientSecret] = useState('');
//     const { confirmSetupIntent, loading } = useConfirmSetupIntent();
//
//     useEffect(()=> {
//         createSetupIntent()
//     })
//
//     // Create a setup intent on the backend
//     function createSetupIntent() {
//         axiosConfig.defaults.headers.common[
//             'Authorization'
//             ] = `Bearer ${user.token}`;
//
//         axiosConfig.post('/payment-sheet').then(function (response) {
//             setClientSecret(response)
//         })
//             .catch(error => {
//                 console.log('Error: ', error)
//             });
//     }
//
//     const handlePayPress = async () => {
//         // Gather the customer's billing information (for example, email)
//         const billingDetails: BillingDetails = {
//             email: 'test.card@example.com',
//         };
//
//         const { setupIntent, error } = await confirmSetupIntent(clientSecret, {
//             paymentMethodType: 'Card',
//             paymentMethodData: {
//                 billingDetails,
//             }
//         });
//
//         if (error) {
//             Alert.alert(`Error code: ${error.code}`, error.message);
//         } else {
//             Alert.alert('Success', 'Your payment method is successfully set up for future payments!');
//         }
//     };
//
//     return (
//         <Block>
//             <Button
//                 color="success"
//                 style={styles.button}
//                 variant="primary"
//                 disabled={!loading}
//                 title="Set up"
//                 onPress={handlePayPress}
//             >
//                 Save my billing details
//             </Button>
//         </Block>
//     );
// }
//
// const styles = StyleSheet.create({
//     button: {
//         marginBottom: 2,
//         width: width - theme.SIZES.BASE * 6,
//         borderRadius: 6
//     },
// })
