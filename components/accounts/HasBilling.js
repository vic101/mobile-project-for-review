import React, {useContext, useEffect, useState} from "react";
import {AuthContext} from "../../context/AuthProvider";
import {Block, Text, theme} from "galio-framework";
import {ActivityIndicator, Alert, Dimensions, StyleSheet, TouchableOpacity} from "react-native";
import { format } from "date-fns";
import axiosConfig from "../../helpers/axiosConfig";
import {
    Modal,
    ModalContent,
    ModalFooter,
    ModalTitle,
    ModalButton,
    FadeAnimation,
    ScaleAnimation
} from 'react-native-modals';
import {Button, Input} from "../index";
import SelectDropdown from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";
import {confirmSetupIntent} from "@stripe/stripe-react-native";
import Card from "./Card";

import * as FileSystem from 'expo-file-system';
import { StorageAccessFramework } from 'expo-file-system';

const { width, height } = Dimensions.get("screen");

export default function AccountHasBilling ({navigation, intent, billingDetails, payments, countries, reload}) {
    const { user } = useContext(AuthContext);
    const [showModal, setShowModal] = useState(false)
    const [showCard, setShowCard] = useState(false)
    const [showAddEmail, setAddEmail] = useState(false)
    const [email, setEmail] = useState('')
    const [streetNo, setStreetNo]  = useState('');
    const [company, setCompany] = useState('');
    const [countryId, setCountryId]  = useState('');
    const [vat, setVat]  = useState('');
    const [billing, setBilling]  = useState('');
    const [cardComplete, setCardComplete] = useState(false);
    const [updatingCard, setUpdatingCard] = useState(false);
    const [updatingEmail, setUpdatingEmail] = useState(false);

    useEffect(()=> {
        setStreetNo(billingDetails?.street)
        setCompany(billingDetails?.company)
        setVat(billingDetails?.vat)
        setBilling(billingDetails?.card)
    }, [])

    const downloadFile = async (payment) => {
        const pdf         = await grabPdf(payment);
        const permissions = await StorageAccessFramework.requestDirectoryPermissionsAsync();
        if (!permissions.granted) {
            return;
        }

        try {
            await StorageAccessFramework.createFileAsync(permissions.directoryUri, 'invoice-'+payment.invoice_number, 'application/pdf')
                .then(async(uri) => {
                    await FileSystem.writeAsStringAsync(uri, pdf, { encoding: FileSystem.EncodingType.Base64 });
                    Alert.alert('Success', 'Successfully downloaded')
                })
                .catch((e) => {
                    console.log(e.response.data);
                    alert(e)
                });
        } catch (e) {
            throw new Error(e);
            alert(e)
        }
    }
    const grabPdf = async (payment) => {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        const response = await axiosConfig(`/user/invoice/${payment.invoice_number+'/'+payment.user_id}`)
            .catch(error => {
                console.log('Error: ', error.response.data)
                alert('Error: '+ error.response.data)
            });

        return response.data;
    }

    const fetchPaymentIntentClientSecret = async () => {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        const response = await axiosConfig.post(`/updated-intent`)
            .catch(error => {
                console.log('Error: ', error.response.data)
                alert('Error: '+ error.response.data)
            });

        if (typeof (response.data) == 'undefined') {

        } else {
            return response.data?.clientSecret;
        }

        // console.log('response: ', response.data?.clientSecret)
        // return response.data?.clientSecret;
    };
    function updateBillingAddress () {
        setShowModal(false)

        let attributes = {
            company    : company,
            streetNo   : streetNo,
            country    : countryId,
            vatNo      : vat,
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.post('/billing-update-api', attributes).then(response => {
            reload()
            Alert.alert('Success',response.data.message);
        }).catch(error => {
            console.log('Error: ', error.response.data)
            alert('Error: '+ error.response.data)
        });
    }
    function renderBillingDetails() {
        return (
            <Block>
                <Block flex style={styles.listContainer}>
                    <Block>
                        <Text bold size={13} color="#32325D">
                            Billing address
                        </Text>
                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>

                        <Text size={13}>
                            {billingDetails?.company}
                        </Text>
                        <Text size={13}>
                            {billingDetails.street}
                        </Text>
                        <Text size={13}>
                            {billingDetails.country}
                        </Text>
                        {billingDetails.vat != null ?(
                                <Block>
                                    <Text size={13}>
                                        {billingDetails.vat}
                                    </Text>
                                </Block>
                            ) : (
                                <Text>VAT: N/A</Text>
                            )
                        }
                        <Block style={{ flexDirection: 'row'}}>
                            <TouchableOpacity
                                onPress={()=> setShowModal(true)}
                            >
                                <Text muted size={13} style={{ textDecorationLine: 'underline'}}>
                                    edit
                                </Text>
                            </TouchableOpacity>
                        </Block>
                    </Block>
                </Block>
                <Block flex style={styles.listContainer}>
                    <Block>
                        <Text bold size={13} color="#32325D" style={styles.text}>
                            Payment details
                        </Text>
                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>

                        {updatingCard ? (
                            <Block middle>
                                <Block flex style={{ flexDirection: 'row', marginTop: 8}}>
                                    <Block>
                                        <ActivityIndicator
                                            style={{ marginRight: 18 }}
                                            size="small"
                                            color="gray"
                                        />
                                    </Block>
                                    <Block>
                                        <Text bold size={10} color="gray">
                                            Updating payment details...
                                        </Text>
                                    </Block>
                                </Block>
                            </Block>
                        ) : (
                            <Block>
                                <Block>
                                    <Text size={13}>
                                        {billing}
                                    </Text>
                                </Block>
                                <Block style={{ flexDirection: 'row'}}>
                                    <TouchableOpacity
                                        onPress={()=> setShowCard(true)}
                                    >
                                        <Text muted size={13} style={{ textDecorationLine: 'underline'}}>
                                            edit
                                        </Text>
                                    </TouchableOpacity>
                                </Block>
                            </Block>
                        )}

                    </Block>
                </Block>
                <Block flex style={styles.listContainer}>
                    <Text size={13}>Send new invoices to {user.email}</Text>
                    <>
                        {billingDetails.billingEmail != null ?
                        (
                            <Block>
                            <Text muted size={11}>A copy of your future invoices is sent to {billingDetails.billingEmail}</Text>
                                <TouchableOpacity
                                    onPress={()=> SaveEmail('remove')}
                                >
                                    <Text muted size={11} style={{ textDecorationLine: 'underline'}}>
                                        remove
                                    </Text>
                                </TouchableOpacity>
                        </Block>
                        ) : (
                            <Block style={{ flexDirection: 'row'}}>
                            <TouchableOpacity
                                onPress={()=> setAddEmail(true)}
                            >
                                <Text muted size={13} style={{ textDecorationLine: 'underline'}}>
                                    add billing email address
                                </Text>
                            </TouchableOpacity>
                        </Block>
                        )}
                    </>
                </Block>
            </Block>
        );
    }
    const handleSaveCard = async () => {
        if (cardComplete) {
            try {
                setUpdatingCard(true)
                setShowCard(false)

                // const clientSecret = await fetchPaymentIntentClientSecret();
                const { setupIntent, error } = await confirmSetupIntent(intent.client_secret, {
                    type: 'Card'
                });

                if (error) {
                    Alert.alert(`Error code: ${error.code}`, error.message);
                    setUpdatingCard(false)
                    setCardComplete(false)
                } else {
                    let paymentMethod = setupIntent.paymentMethodId
                    let attributes = {
                        paymentMethod: paymentMethod,
                        id: user.id
                    }

                    handleUpdate(attributes)
                }
            } catch (e) {
                console.log(e)
            }
        } else {
            Alert.alert('Error', 'Please fill out credit card details completely.');
            console.log('Please fill out credit card details completely.')
        }
    };

    const SaveEmail = async (status) => {
        if (status == 'add') {
            if (email != '' ) {
                await handleSaveEmail(status)
            } else {
                Alert.alert('Error', 'Please fill out email field.');
                console.log('Please fill out email field.')
            }
        } else {
            await handleSaveEmail(status)
        }
    }
    const handleSaveEmail = async (status) => {
        try {
            setUpdatingEmail(true)
            setAddEmail(false)
            let attributes = {
                status: status,
                email: status == 'add' ? email : billingDetails.billingEmail,
                id: 247 // subscriber id here
            }

            console.log('attributes: ', attributes)

            axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
            axiosConfig.post('/add-billing-email-api', attributes).then(response => {
                Alert.alert('Success',response.data.message);
                setUpdatingEmail(false)
                reload()
            }).catch(error => {
                setUpdatingEmail(false)
                console.log('Error: ', error.response.data)
                alert('Error: '+ error.response.data)
            });
        } catch (e) {
            setUpdatingEmail(false)
            console.log(e)
        }
    };
    function handleUpdate(attributes) {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        axiosConfig.post('/payment-method-update', attributes).then(response => {

            setBilling(response.data.card)
            Alert.alert('Success',response.data.message);
            setUpdatingCard(false)
            setCardComplete(false)
        }).catch(error => {
            setUpdatingCard(false)
            setCardComplete(false)
            console.log('Error: ', error.response.data)
            alert('Error: '+ error.response.data)
        });
    }
    function renderCard () {
        return (
            <Block>
            <Modal
                width={(width - theme.SIZES.BASE * 2)}
                modalAnimation={
                    new ScaleAnimation({
                        initialValue: 0, // optional
                        useNativeDriver: true, // optional
                    })
                }
                visible={showCard}
                modalTitle={<ModalTitle textStyle={{fontSize: 15}} title="Update payment details" />}
                footer={
                    <ModalFooter>
                        <ModalButton
                            text="CANCEL"
                            onPress={() => {setShowCard(false)}}
                            textStyle={{fontSize: 15}}
                        />
                        <ModalButton
                            text="OK"
                            onPress={() => handleSaveCard() }
                            textStyle={{fontSize: 15}}
                        />
                    </ModalFooter>
                }
            >
                <ModalContent style={{ marginTop: 10}}>
                    <Card
                        setCardComplete={setCardComplete}
                    />
                </ModalContent>
            </Modal>
            </Block>
        )
    }
    function renderAddBillingEmail () {
        return (
            <Block>
                <Modal
                    width={(width - theme.SIZES.BASE * 2)}
                    modalAnimation={
                        new ScaleAnimation({
                            initialValue: 0,
                            useNativeDriver: true,
                        })
                    }
                    visible={showAddEmail}
                    modalTitle={<ModalTitle textStyle={{fontSize: 15}} title="Add billing email" />}
                    footer={
                        <ModalFooter>
                            <ModalButton
                                text="CANCEL"
                                onPress={() => {setAddEmail(false)}}
                                textStyle={{fontSize: 15}}
                            />
                            <ModalButton
                                text="OK"
                                onPress={() => SaveEmail('add') }
                                textStyle={{fontSize: 15}}
                            />
                        </ModalFooter>
                    }
                >
                    <ModalContent style={{ marginTop: 10}}>
                        <Block>
                            <Input style={styles.input} onChangeText={setEmail} value={email} placeholder={'Email'} iconContent={<Block />}/>
                        </Block>
                    </ModalContent>
                </Modal>
            </Block>
        )
    }
    function renderBillingDetailsModal() {
        return (
            <Modal
                modalAnimation={
                    new ScaleAnimation({
                        initialValue: 0, // optional
                        useNativeDriver: true, // optional
                    })
                }
                visible={showModal}
                onTouchOutside={() => {
                    setShowModal(false)
                }}
                modalTitle={<ModalTitle textStyle={{fontSize: 15}} title="Update billing details" />}
                footer={
                    <ModalFooter>
                        <ModalButton
                            text="CANCEL"
                            onPress={() => {setShowModal(false)}}
                            textStyle={{fontSize: 15}}
                        />
                        <ModalButton
                            text="OK"
                            onPress={() => updateBillingAddress() }
                            textStyle={{fontSize: 15}}
                        />
                    </ModalFooter>
                }
            >
                <ModalContent style={{ marginTop: 10}}>
                    <Block>
                        <Input style={styles.input} onChangeText={setCompany} value={company} placeholder={'Company'} iconContent={<Block />}/>
                    </Block>
                    <Block>
                        <Input style={styles.input} onChangeText={setStreetNo} value={streetNo} placeholder={'Street, No.'} iconContent={<Block />}/>
                    </Block>
                    <Block style={styles.dropdownsRow}>
                        <SelectDropdown
                            data={countries}
                            onSelect={(selectedItem, index) => {
                                setCountryId(selectedItem.id)
                            }}
                            defaultButtonText={billingDetails.country}
                            buttonTextAfterSelection={(selectedItem, index) => {
                                return selectedItem.name;
                            }}
                            rowTextForSelection={(item, index) => {
                                return item.name;
                            }}
                            buttonStyle={styles.dropdown1BtnStyle}
                            buttonTextStyle={styles.dropdown1BtnTxtStyle}
                            renderDropdownIcon={isOpened => {
                                return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={12} />;
                            }}
                            dropdownIconPosition={'right'}
                            dropdownStyle={styles.dropdown1DropdownStyle}
                            rowStyle={styles.dropdown1RowStyle}
                            rowTextStyle={styles.dropdown1RowTxtStyle}
                        />
                    </Block>
                    {/*<Block> needs validation */}
                    {/*    <Input style={styles.input} onChangeText={setVat} value={billingDetails.vat} placeholder={'VAT No.'} iconContent={<Block />}/>*/}
                    {/*</Block>*/}
                </ModalContent>
            </Modal>
        )
    }
    function renderPayments() {
        return (
                <Block flex style={styles.listContainer}>
                    <Text size={13}>Billing history</Text>
                    <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                        <Block style={styles.divider} />
                    </Block>
                    {
                        payments.map((payment, index) => {
                            return (
                                <Block key={index}>
                                    <Block style={{ flexDirection: 'row' }}>
                                        <Block style={{ flex: 12, marginRight: 2 }}>
                                            <Text size={13}>
                                                {'INV'+ payment.invoice_number + ' '}
                                                <Text muted>
                                                    {format(new Date(payment.created_at), 'LLL dd, yyyy') + ' | €' + payment.total/100}
                                                </Text>
                                            </Text>
                                        </Block>
                                        <Block>
                                            <Text onPress={ ()=> downloadFile(payment)} style={{ textDecorationLine: 'underline' }}>Download PDF</Text>
                                        </Block>
                                    </Block>

                                    <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                                        <Block style={styles.divider} />
                                    </Block>
                                </Block>
                            )
                        })
                    }
                </Block>
            )
    }
    return (
        <Block>
            { renderBillingDetailsModal() }
            { renderCard() }
            { renderAddBillingEmail() }
            { renderBillingDetails() }
            { renderPayments() }

            <Block style={{ marginBottom: 20}}>
                <Block flex style={styles.listContainer}>
                    <TouchableOpacity
                        onPress={()=> navigation.navigate('CancelBilling')}
                    >
                        <Text color="#f5365c" style={{textDecorationLine: 'underline'}}>Cancel account</Text>
                    </TouchableOpacity>
                </Block>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    card: {
        width: 0.75
    },
    buttonEnabled: {
        marginBottom: 2,
        height: 35,
        width: width - theme.SIZES.BASE * 16,
        borderRadius: 6
    },
    buttonDisabled: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 4,
        borderRadius: 6,
        backgroundColor: '#68758b'
    },
    cardInput: {
        borderColor: '#edeff2',
        width: '100%',
        height: 50,
        marginVertical: 30
    },
    dropdownsRow: {flexDirection: 'row', width: '100%', paddingTop: '2%', paddingBottom: '2%'},
    dropdown1BtnStyle: {
        flex: 1,
        height: 40,
        backgroundColor: '#edeff2',
        borderRadius: 8,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 14},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left'},
    input: {
        backgroundColor: '#edeff2',
        borderColor: '#edeff2'
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    },
    listContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        // marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE
    },
});
