import {ActivityIndicator, Alert, Dimensions, StyleSheet, TextInput} from "react-native";
import React, {useContext, useEffect, useState} from "react";
import {argonTheme} from "../../../constants";
import {Block, theme} from "galio-framework";
import {Button} from "../../index";
import axiosConfig from "../../../helpers/axiosConfig";
const { width, height } = Dimensions.get("screen");

export default function CustomResponseActions(
    {
        isMessaging,
        token,
        userAnswerCustomId,
        questionConversationId,
        userId,
        questionId,
        handleResponseReload,
        setStatus
    }
) {

    const [isLoading, setIsLoading] = useState(false);
    const [message, setMessage] = useState('');

    function action(status) {
        if (message.length === 0 && status === 'replied') {
            Alert.alert('Error', 'Please enter your message');
            return;
        }

        let attributes = {
            state: 'custom-question',
            userAnswerCustomId: userAnswerCustomId,
            questionConversationId: questionConversationId,
            userId: userId,
            questionId: questionId,
            message : message,
            status : status
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        axiosConfig.post('/conversation-api', attributes).then((response) =>  {
            if (!isMessaging) {
                handleResponseReload()
            } else {
                Alert.alert('Success', response.data.message)
                status == 'replied' ? handleResponseReload() : setStatus('resolved')
            }
        }).catch(error => {
            console.log('custom res actions error: ', error)
        })
    }

    return (
        <Block>
            {isLoading ? (
                <ActivityIndicator
                    size="small"
                    color="gray"
                    style={{ marginRight: 8, marginTop: 10 }}
                />
            ):(
                <Block style={styles.container}>
                    <Block style={{ flexDirection: 'row' }}>
                        <Block style={{ flex: 12, marginBottom: 5 }}>
                            <TextInput
                                multiline
                                placeholder="Type a message"
                                style={styles.input}
                                onChangeText={setMessage}
                            />
                        </Block>
                    </Block>
                    <Block style={{ flexDirection: 'row' }}>
                        <Block style={{ flex: 12 }} center>
                            <Button color="default" style={styles.default} onPress={()=> action('replied')}>
                                Send
                            </Button>
                        </Block>
                    </Block>
                    <Block style={{ flexDirection: 'row' }}>
                        <Block style={{ flex: 12 }} center>
                            <Button style={styles.success} onPress={() => action('resolved')}>
                                Mark as resolved
                            </Button>
                        </Block>
                    </Block>
                </Block>
            )}
        </Block>
    )
}

const styles = StyleSheet.create({
    container: {
        maxWidth: 500
    },
    input: {
        maxHeight: 100,
        minHeight: 45,
        minWidth: 50,
        marginTop: 10,
        borderRadius: 4,
        backgroundColor: '#edeff2',
        shadowColor: argonTheme.COLORS.BLACK,
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 2,
        shadowOpacity: 0.05,
        elevation: 2,
        paddingLeft: 8,
        paddingRight: 8
    },
    default: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 4,
        borderRadius: 6,
        backgroundColor: '#32325d'
    },
    success: {
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 4,
        borderRadius: 6,
        backgroundColor: '#36d79a'
    }
})
