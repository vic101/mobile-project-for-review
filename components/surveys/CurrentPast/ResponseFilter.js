import {Alert, Dimensions, StyleSheet} from "react-native";
import {Block, Text, theme} from "galio-framework";
import React from "react";
import SelectDropdown from "react-native-select-dropdown";
import options from "../../manage/sortOptions";
import {FontAwesome} from "@expo/vector-icons";
import CheckBox from "@react-native-community/checkbox";

const { width, height } = Dimensions.get("screen");
export default function ResponseFilter (
    {
        index,
        showNew,
        showReplied,
        showResolved,
        checkBoxAction,
        setFilter,
        allAnswerStatusCount,
        answersStatusCount,
        questionId,
        answers,
        setId,
        setType
    }) {
    const filterOption = [
        {
            key: 'desc',
            name: 'Newest first'
        },
        {
            key: 'asc',
            name: 'Oldest first'
        }
    ]

    const checkBoxColors = { true: '#32325d', false: 'black' }

    return (
        <Block >
            <Block style={styles.container}>
                <Block style={styles.showMessagesForLabel}>
                    <Block style={{ flex: 6 }}>
                        <Text style={styles.black}>
                            Show messages for
                        </Text>
                    </Block>
                </Block>
                <Block style={[styles.dropdownContainer, styles.dropdownsRow]}>
                    <SelectDropdown
                        data={answers}
                        onSelect={(selectedItem, index) => {
                            // console.log('status: ', selectedItem.key)
                            // filter(selectedItem.key, fetchType)
                            console.log('anyId: ', selectedItem.id)
                            setId(selectedItem.id)
                            setType('answer')
                        }}
                        defaultButtonText={'All answers' +allAnswerStatusCount[questionId]?.statuses}
                        buttonTextAfterSelection={(selectedItem, index) => {
                            return selectedItem?.text + ' ' + answersStatusCount[selectedItem.id];
                        }}
                        rowTextForSelection={(item, index) => {
                            return item?.text + ' ' + answersStatusCount[item.id]
                        }}
                        buttonStyle={styles.dropdown1BtnStyle}
                        buttonTextStyle={styles.dropdown1BtnTxtStyle}
                        renderDropdownIcon={isOpened => {
                            return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                        }}
                        dropdownIconPosition={'right'}
                        dropdownStyle={styles.dropdown1DropdownStyle}
                        rowStyle={styles.dropdown1RowStyle}
                        rowTextStyle={styles.dropdown1RowTxtStyle}
                        search
                        searchInputStyle={styles.dropdown1searchInputStyleStyle}
                        searchPlaceHolder={'Search here'}
                        searchPlaceHolderColor={'darkgrey'}
                        renderSearchInputLeftIcon={() => {
                            return <FontAwesome name={'search'} color={'#444'} size={18} />;
                        }}
                    />
                </Block>
            </Block>
            <Block style={styles.container}>
                <Block style={{ flex: 4}}>
                    <Block style={styles.wrapper}>
                        <CheckBox
                            tintColors={checkBoxColors}
                            value={showNew}
                            onChange={()=> checkBoxAction('new', index, showNew?false:true)}
                        />
                        <Text style={styles.text}>
                            Show new
                        </Text>
                    </Block>
                </Block>
                <Block style={{ flex: 4}}>
                    <Block style={styles.wrapper}>
                        <CheckBox
                            tintColors={checkBoxColors}
                            value={showReplied}
                            onChange={()=> checkBoxAction('replied', index, showReplied?false:true)}
                        />
                        <Text style={styles.text}>
                            Show replied
                        </Text>
                    </Block>
                </Block>
                <Block style={{ flex: 4}}>
                    <Block style={styles.wrapper}>
                        <CheckBox
                            tintColors={checkBoxColors}
                            value={showResolved}
                            onChange={()=> checkBoxAction('resolved', index, showResolved?false:true)}
                        />
                        <Text style={styles.text}>
                            Show resolved
                        </Text>
                    </Block>
                </Block>
            </Block>
            <Block>
                <Block style={[styles.dropdownsRow2]}>
                    <SelectDropdown
                        data={filterOption}
                        onSelect={(selectedItem, index) => {
                            setFilter(selectedItem.key)
                        }}
                        defaultButtonText={filterOption[0].name}
                        buttonTextAfterSelection={(selectedItem, index) => {
                            return selectedItem.name;
                        }}
                        rowTextForSelection={(item, index) => {
                            return item.name;
                        }}
                        buttonStyle={styles.dropdown1BtnStyle}
                        buttonTextStyle={styles.dropdown1BtnTxtStyle}
                        renderDropdownIcon={isOpened => {
                            return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={11} />;
                        }}
                        dropdownIconPosition={'right'}
                        dropdownStyle={styles.dropdown1DropdownStyle}
                        rowStyle={styles.dropdown1RowStyle}
                        rowTextStyle={styles.dropdown1RowTxtStyle}
                        search
                        searchInputStyle={styles.dropdown1searchInputStyleStyle}
                        searchPlaceHolder={'Search here'}
                        searchPlaceHolderColor={'darkgrey'}
                        renderSearchInputLeftIcon={() => {
                            return <FontAwesome name={'search'} color={'#444'} size={18} />;
                        }}
                    />
                </Block>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    filterContainer: {
        width: width - theme.SIZES.BASE * 5
    },
    text: {
        lineHeight: 30,
        fontSize: 12
    },
    dropdownsRow: {
        flexDirection: 'row',
        width: '100%',
        maxWidth: '55%',
        alignSelf: 'flex-end',
        marginRight: -5
    },
    dropdownsRow2: {
        flexDirection: 'row',
        width: '100%',
        alignSelf: 'flex-end'
    },
    dropdown1BtnStyle: {
        flex: 1,
        height: 30,
        backgroundColor: '#FFF',
        borderRadius: 4,
        borderWidth: 0.75,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 12},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 12},
    container: {
        flex: 1,
        flexDirection: "row",
        alignContent: "space-between",
        maxWidth: 500,
        marginBottom: 10,
        marginTop: 10
    },
    showMessagesForLabel: {
        paddingTop: 10
    },
    dropdownContainer: {
        minWidth: 5,
        justifyContent: 'center',
        marginLeft: 10,
        paddingTop:5
    },
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        // paddingVertical: 15,
    },
    textLeft: {
        fontSize: 10,
        textAlign: 'left'
    },
    textRight: {
        fontSize: 10,
        textAlign: 'right'
    },
    white: {
        color: 'white'
    },
    black: {
        color: 'black'
    },
    responseContainer: {
        flexDirection: 'row',
        padding: 10,
        borderRadius: 5,
        maxWidth: 400,
    },
    gray: {
        backgroundColor: '#edeff2'
    },
})
