import {Block, Input, Text, theme} from "galio-framework";
import {Alert, Dimensions, StyleSheet, TouchableOpacity} from "react-native";
import React from "react";

const { width, height } = Dimensions.get("screen");
export default function BarGraph(
    {
        answer,
        answerText,
        answerPercentage,
        fetchIndividualAnswers,
        index,
        iWhich,
        userAnswerCount}
) {

    // graph percent values: 20min, 10max
    let textColor = null;
    function graphPercentWitch () {
        let answerPercentageSwitch = parseInt(answerPercentage);

        if (answerPercentageSwitch >= 90) {
            return (answerPercentageSwitch/10) + (2*1);
        } else if (answerPercentageSwitch >= 80 && answerPercentageSwitch < 90) {
            return (answerPercentageSwitch/10) + (2*2);
        } else if (answerPercentageSwitch >= 70 && answerPercentageSwitch < 80) {
            return (answerPercentageSwitch/10) + (2*3);
        } else if (answerPercentageSwitch >= 60 && answerPercentageSwitch < 70) {
            return (answerPercentageSwitch/10) + (2*4);
        } else if (answerPercentageSwitch >= 50 && answerPercentageSwitch < 60) {
            return (answerPercentageSwitch/10) + (2*5);
        } else if (answerPercentageSwitch >= 40 && answerPercentageSwitch < 50) {
            return (answerPercentageSwitch/10) + (2*6);
        } else if (answerPercentageSwitch >= 30 && answerPercentageSwitch < 40) {
            return (answerPercentageSwitch/10) + (2*7);
        } else if (answerPercentageSwitch >= 20 && answerPercentageSwitch < 30) {
            return (answerPercentageSwitch/10) + (2*8);
        } else if (answerPercentageSwitch >= 10 && answerPercentageSwitch < 20) {
            return (answerPercentageSwitch/10) + (2*9);
        } else if (answerPercentageSwitch >= 0 && answerPercentageSwitch < 10) {
            if (answerPercentageSwitch < 1) {
                return 19;
            } else {
                return (answerPercentageSwitch/10) + (2*10);
            }
        }
    }

    const graphColor = () => {
        if (answer.graph_color === 'red') {
            return styles.red;
        }
        if (answer.graph_color === 'greendeep') {
            return styles.greendeep;
        }
        if (answer.graph_color === 'darkblue') {
            return styles.darkblue;
        }
    }
    const color = () => {
        if (iWhich == 'current') {
            if (answer.user_answers_count > 0) {
                return graphColor()
            } else {
                textColor = 'gray';
                return styles.gray;
            }
        } else {
            if (userAnswerCount > 0) {
                return graphColor()
            } else {
                textColor = 'gray';
                return styles.gray;
            }
        }
    }
    const answersCount = () => {
        return answer.user_answers_count;

        // this has been commented because this is an issue for manager's account
        // if (iWhich == 'current') {
        //     return answer.user_answers_count;
        // } else {
        //     return userAnswerCount;
        // }
    }
    return (
        <Block style={styles.container}>
            <Block style={[styles.barGraph, color() , {
                width: width - theme.SIZES.BASE * graphPercentWitch()
            }]}>
                <Block style={{ flex: 6 }}>
                    <Text maxLength={3} style={[styles.textLeft, textColor !== 'gray' ? styles.white:styles.black]}>{answerText}</Text>
                </Block>
                <Block style={{ flex: 6}}>
                    <Text maxLength={3} style={[styles.textRight, textColor !== 'gray' ? styles.white:styles.black]}>{answersCount()} ({answerPercentage} %)</Text>
                </Block>
            </Block>
            <Block style={styles.message}>
                {
                    answersCount() > 0 &&
                    <TouchableOpacity
                        onPress={() => fetchIndividualAnswers(answer.id, 'answer', index)}
                    >
                        <Text size={10} muted style={{textDecorationLine: "underline"}}>{
                            answersCount() > 1 ? (answersCount() + ' messages') : (answersCount() + ' message')
                        }</Text>
                    </TouchableOpacity>
                }
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",
        alignContent: "space-between",
        maxWidth: 500,
        marginBottom: 10
    },
    barGraph: {
        flexDirection: 'row',
        padding: 10,
        borderRadius: 5,
        maxWidth: 400,
    },
    message: {
        minWidth: 5,
        maxWidth: 80,
        justifyContent: 'center',
        marginLeft: 5,
    },
    textLeft: {
        fontSize: 10,
        textAlign: 'left'
    },
    textRight: {
        fontSize: 10,
        textAlign: 'right'
    },
    white: {
        color: 'white'
    },
    black: {
        color: 'black'
    },
    greendeep: {
        backgroundColor: '#36d79a'
    },
    darkblue: {
        backgroundColor: '#32325d'
    },
    red: {
        backgroundColor: '#f5365c'
    },
    gray: {
        backgroundColor: '#edeff2'
    },
})
