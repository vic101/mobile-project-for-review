import {Dimensions, StyleSheet, TouchableOpacity} from "react-native";
import {Block, Text, theme} from "galio-framework";
import React from "react";

const { width, height } = Dimensions.get("screen");
export default function Responses (
    {
        answer,
        aIndex,
        ansIndex,
        setIndex,
        setBlur,
        blur
    }
    ) {
    const textColor = (status) => {
        if (status == 'resolved') {
            return styles.white
        } else if (status == 'replied') {
            return styles.gray
        }
    }

    const actions = () => {
        if (aIndex !== ansIndex) {
            setIndex(aIndex)
            setBlur(true)
        } else {
            setBlur(false)
            setIndex(undefined)
        }
    }
    return (
        <Block style={[styles.container, (blur && (aIndex !== ansIndex)) && styles.blur]}>
            <Block style={[styles.responseContainer, (answer.status == 'resolved' && styles.messageResolved)]}>
            <Block style={{ flex: 6 }}>
                <Text size={12} style={textColor(answer.status)}>
                    {answer.message}
                </Text>
            </Block>
            <Block style={{ flex: 1, flexDirection: 'row', marginTop: 10, marginBottom: -10}}>
                <Block style={{ flex: 1}}>
                    <Text size={10} muted style={textColor(answer.status)}>
                        Location:
                        {
                            answer?.user?.location_employee?.map((location, index) => {
                                return (
                                    (index ? ', ' : '') + location?.location?.name
                                )
                        })
                    }
                    </Text>
                </Block>
                <Block style={{ flex: 1}}>
                    <Text size={10} muted style={[textColor(answer.status),{textAlign: 'right'}]}>
                        {answer?.user?.team_employee?.length > 0 && <Text>Team: </Text>}
                        {
                            answer?.user?.team_employee?.map((team, index) => {
                                return (
                                    (index ? ', ' : '') + team?.team?.name
                                )
                            })
                        }
                    </Text>
                    {
                        answer.status != 'new' ? (
                            <Text bold size={10} style={[answer.status == 'replied' ? ({color: '#36d79a'}):({color: 'white'}), {
                                textAlign: 'right'
                            }]}>{answer.status}</Text>
                        ) : (
                            <TouchableOpacity>
                                <Text
                                    onPress={()=> actions()}
                                    size={10} muted bold style={{textAlign: 'right', color: '#32325d'}}
                                >click to reply/resolve</Text>
                            </TouchableOpacity>
                        )
                    }
                </Block>
            </Block>
        </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    blur: {
        opacity: 0.4
    },
    messagesReplied: {
        textAlign: 'right',
        color: '#36d79a'
    },
    messageResolved: {
        textAlign: 'right',
        backgroundColor: '#36d79a'
    },
    container: {
        flex: 1,
        flexDirection: "row",
        alignContent: "space-between",
        maxWidth: 500
    },
    textLeft: {
        fontSize: 10,
        textAlign: 'left'
    },
    textRight: {
        fontSize: 10,
        textAlign: 'right'
    },
    white: {
        color: 'white'
    },
    black: {
        color: '#32325d'
    },
    responseContainer: {
        padding: theme.SIZES.BASE,
        marginTop: 15,
        backgroundColor: '#edeff2',
        borderRadius: 5,
        maxWidth: 400,
        width: width - theme.SIZES.BASE * 4
    },
    gray: {
        color: '#8898aa'
    }
})
