import {Block, Text} from "galio-framework";
import {StyleSheet} from "react-native";
import React from "react";

export default function SurveyDetails({ surveyDetails , messageCount}) {

    return (
        <Block>
            <Text bold size={15} color="#32325D" style={styles.text}>
                Survey details
            </Text>
            <Text size={15} color="#32325D" style={styles.text}>
                Start: {surveyDetails?.start}
            </Text>
            <Text size={15} color="#32325D" style={styles.text}>
                End: {surveyDetails?.end} <Text muted size={12}>{surveyDetails?.status}</Text>
            </Text>
            <Text size={15} color="#32325D" style={styles.text}>
                { surveyDetails?.surveyCompleted + ' / '+ surveyDetails?.employeeCount } completed the survey <Text muted size={12}>( {surveyDetails?.completionPercentage} ) %</Text>
            </Text>
            <Text size={15} color="#32325D" style={styles.text}>
                {messageCount}
            </Text>
        </Block>
    )
}

const styles = StyleSheet.create({
    text: {
        lineHeight: 30
    },
})
