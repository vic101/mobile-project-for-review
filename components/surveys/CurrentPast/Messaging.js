import React, {useContext, useEffect, useState} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView, Dimensions
} from 'react-native';

import {Block, theme, Text} from "galio-framework";
import axiosConfig from "../../../helpers/axiosConfig";
import {argonTheme} from "../../../constants";
import Ionicons from "@expo/vector-icons/Ionicons";
import {Menu, MenuItem} from "react-native-material-menu";
import ResponseActions from "./ResponseActions";
import CustomResponseActions from "./CustomResponseActions";

const { width, height } = Dimensions.get("screen");
export default function Messaging ({route})
{
    const [question, setQuestion] = useState('');
    const [messages, setMessages] = useState([]);
    const [category, setCategory] = useState('');
    const [subCategory, setSubCategory] = useState('');

    const [isLoading, setIsLoading] = useState(false);
    const [loadConversation, setLoadConversation] = useState(true);

    const [answer, setAnswer] = useState([]);
    const [response, setResponse] = useState([]);

    const [status, setStatus] = useState(undefined);
    const [questionType, setQuestionType] = useState(route?.params?.type);

    const [token, setToken] = useState('');
    const [lastMessage, setLastMessage] = useState('');
    const [resAns, setResAns] = useState([]);
    const [visible, setVisible] = useState(false);


    useEffect(()=> {
        setAnswer(route?.params?.answer)
        setResponse(route?.params?.response)
        setToken(route?.params?.token)
    },[answer])

    useEffect(()=> {
        if (token) {
            fetchSingleConversation()
        }
    },[token])

    function fetchSingleConversation () {
        let attributes = {}
        if (questionType == 'custom') {
            attributes = {
                questionConversationId: response.question_conversation_id,
                questionId: response.question_id,
                userId: response.user_id
            }
        } else {
            attributes = {
                questionConversationId: answer.question_conversation_id,
                questionId: answer.question_id,
                answerId: answer.answer_id
            }
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        axiosConfig.get('/single-conversation-api?', { params: attributes}).then(response => {
            let conversations = response.data.conversation.messages.conversations;
            setQuestion(response.data.conversation.question)
            setMessages(conversations)

            if (questionType !== 'custom') {
                setResAns(response.data.conversation.answer)
                setCategory(response.data.conversation.category)
                setSubCategory(response.data.conversation.subCategory)

                // _this.limitedConvo(_this.singleConversation)
            } else {
                // _this.limitedConvo(_this.singleConversation)
            }

            setLastMessage(conversations[conversations.length-1].created_at_ago)

            setLoadConversation(false)
        }).catch(error => {
            console.log('custom error: ', error.response)
            setLoadConversation(false)
        });
    }
    function unresolved (whichQuestion, data) {
        setIsLoading(true)

        let attribute = {
            status: 'unresolved',
            messaging: true,
            custom: whichQuestion === 'custom' ? true : false,
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        axiosConfig.patch('/question-conversation-api/' + data.question_conversation_id, attribute).then(response => {
            setStatus(undefined)
            setVisible(false)
            setIsLoading(false)
        }).catch(error => {
            setIsLoading(false)
            console.log('Error: ', error.response)
        })
    }
    function renderConversation () {
        return (
                messages.reverse().map((message, index) => {
                    return (
                        <Block key={index}>
                            {message.user?.role_name != 'employee' ? (
                                <Block right>
                                    <Block style={styles.messagesContainer}>
                                        <Block style={styles.outgoingMessage}>
                                            <Text style={styles.outGoingText}>
                                                {message.message}
                                            </Text>

                                            <Block style={{ alignSelf: 'flex-end', marginTop: 10, marginBottom: -10}}>
                                                <Text size={12} style={styles.outGoingText}>{message.created_at_readable}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            ) : (
                                <Block left>
                                    <Block style={styles.messagesContainer}>
                                        <Block style={styles.incomingMessage}>
                                            <Text style={styles.incomingText}>
                                                {message.message}
                                            </Text>

                                            <Block style={{ alignSelf: 'flex-end', marginTop: 10, marginBottom: -10}}>
                                                <Text size={12} style={styles.incomingText}>{message.created_at_readable}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            )}
                        </Block>
                    )
                })
            )
    }
    function customQuestionDropdown() {
        return (
            <Block>
                {(response.status !== 'resolved' && status === undefined) ? (
                    <Text size={9} muted style={{ textAlign: 'right'}}>Last message: <Text bold>{lastMessage}</Text></Text>
                ):(
                    <Block style={{ alignSelf: 'flex-end'}}>
                        <Ionicons
                            onPress={() => visible ? setVisible(false) : setVisible(true)}
                            name="ellipsis-vertical" size={14} color="#8898aa"
                        />
                        <Menu
                            visible={visible}
                            onRequestClose={() => setVisible(false)}
                            row middle space="between"
                        >
                            <MenuItem onPress={()=> unresolved('custom', response)}>Unresolved</MenuItem>
                        </Menu>
                    </Block>
                )}
            </Block>
        )
    }
    function fixedQuestionDropdown() {
        return (
            <Block>
                {(answer.status !== 'resolved' && status === undefined) ?
                    <Text size={9} muted style={{ textAlign: 'right'}}>Last message: <Text bold>{lastMessage}</Text></Text>
                    :
                    <Block style={{ alignSelf: 'flex-end'}}>
                        <Ionicons
                            onPress={() => visible ? setVisible(false) : setVisible(true)}
                            name="ellipsis-vertical" size={14} color="#8898aa"
                        />
                        <Menu
                            visible={visible}
                            onRequestClose={() => setVisible(false)}
                            row middle space="between"
                        >
                            <MenuItem onPress={()=> unresolved('fixed', answer)}>Unresolved</MenuItem>
                        </Menu>
                    </Block>
                }
            </Block>
        )
    }
    function renderHeader () {
        return (
            <Block>
                <Block style={{flexDirection: 'row'}}>
                    <Block style={{ flex: 3}}>
                        <Text size={10} muted bold>{questionType === 'custom' ? 'Question created by organization' : (category != null ? subCategory : 'Un-categorize')}</Text>
                    </Block>
                    <Block style={{ flex: 3}}>
                        <Text size={9} muted>
                            Location:
                            { questionType === 'custom' ?
                                response?.user?.location_employee?.map((location, index) => {
                                    return (
                                        (index ? ', ' : '') + location?.location?.name
                                    )
                                })
                                :
                                answer?.user?.location_employee?.map((location, index) => {
                                    return (
                                        (index ? ', ' : '') + location?.location?.name
                                    )
                                })
                            }
                        </Text>
                    </Block>
                    <Block style={{ flex: 3}}>
                        {questionType === 'custom' ? customQuestionDropdown() : fixedQuestionDropdown()}
                    </Block>
                </Block>

                <Block style={{flexDirection: 'row'}}>
                    <Block style={{ flex: 3}}>
                        <Text size={10} muted>Question</Text>
                    </Block>
                    <Block style={{ flex: 6.2}}>
                        <Text size={9} muted> {question}</Text>
                    </Block>
                </Block>

                <Block style={{flexDirection: 'row'}}>
                    <Block style={{ flex: 3}}>
                        <Text size={10} muted>Answer</Text>
                    </Block>
                    <Block style={{ flex: 6.2}}>
                        <Text size={9} muted>{resAns}</Text>
                    </Block>
                </Block>
            </Block>
        )
    }
    return (
        <Block flex style={{ backgroundColor: argonTheme.COLORS.BACKGROUND}}>
            {isLoading || loadConversation ? (
                <ActivityIndicator style={{ marginTop: 8 }} size="large" color="gray" />
            ):(
                <ScrollView>
                <Block flex>
                    <Text style={{ textAlign: 'left', padding: theme.SIZES.BASE}}>
                        {category === 'custom' ? 'Open question' : (category != null ? category : 'Happiness')}
                    </Text>
                    <Block style={styles.container}>
                        {renderHeader()}

                        <Block middle style={{ marginTop: 14, marginBottom: 16 }}>
                            <Block style={styles.divider} />
                        </Block>

                        { renderConversation() }

                        {
                            ((answer?.status !== 'resolved' && status !== 'resolved') && questionType !== 'custom') &&
                            <ResponseActions
                                isMessaging={true}
                                answer={answer}
                                token={token}
                                state={'fixed-question'}
                                questionConversationId={answer.question_conversation_id}
                                userAnswerId={answer.id}
                                userId={answer?.user_id}
                                questionId={answer?.question_id}
                                answerId={answer?.answer_id}
                                setStatus={setStatus}
                                fetchSingleConversation={fetchSingleConversation}
                            />
                        }
                        {
                            ((response?.status !== 'resolved' && status !== 'resolved') && questionType === 'custom') &&
                            <CustomResponseActions
                                isMessaging={true}
                                token={token}
                                userAnswerCustomId={response.id}
                                questionConversationId={response.question_conversation_id}
                                userId={response.user_id}
                                questionId={response.question_id}
                                handleResponseReload={fetchSingleConversation}
                                setStatus={setStatus}
                            />
                        }
                    </Block>
                </Block>
                </ScrollView>
            )}
        </Block>
    );
}

const styles = StyleSheet.create({
    messagesContainer: {
        width: width - theme.SIZES.BASE * 10
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    },
    container: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
    },
    incomingMessage: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderRadius: 6,
        backgroundColor: '#edeff2',
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    outgoingMessage: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        borderRadius: 6,
        backgroundColor: '#36d79a',
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2
    },
    outGoingText: {
        color: '#ffffff'
    },
    incomingText: {
        color: '#32325d'
    },
})
