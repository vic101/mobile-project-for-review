import {Block, theme} from "galio-framework";
import {Dimensions, StyleSheet, Text} from "react-native";
import React, {useEffect, useState} from "react";
import {argonTheme} from "../../../constants";


const { width, height } = Dimensions.get("screen");
export default function Streak({streaks,cheerCount}) {
    const green = argonTheme.COLORS.BRAND_GREEN;

    return (
        <Block row center space="between" style={{marginBottom: 20}}>
            <Block flex middle left>
                <Block style={styles.container}>
                    <Text style={{color: green, fontSize: 9, fontWeight: 'bold'}}>Your best streak</Text>
                    <Text style={{color: green, fontSize: 35, fontWeight: 'bold'}}>{streaks.bestStreak}</Text>
                    <Text style={{color: green, fontSize: 9, fontWeight: 'bold'}}>surveys in a row</Text>
                </Block>
            </Block>
            <Block flex middle center>
                <Block style={[styles.container, {backgroundColor: green}]}>
                    <Text style={{color: 'white', fontSize: 9, fontWeight: 'bold'}}>Your current streak</Text>
                    <Text style={{color: 'white', fontSize: 35, fontWeight: 'bold'}}>{streaks.currentStreak}</Text>
                    <Text style={{color: 'white', fontSize: 9, fontWeight: 'bold'}}>surveys in a row</Text>
                </Block>
            </Block>
            <Block flex middle right>
                <Block style={styles.container}>
                    <Text style={{color: green, fontSize: 9, fontWeight: 'bold'}}>You have sent</Text>
                    <Text style={{color: green, fontSize: 35, fontWeight: 'bold'}}>{cheerCount}</Text>
                    <Text style={{color: green, fontSize: 8, fontWeight: 'bold'}}>Cheers to {cheerCount} coworkers</Text>
                </Block>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        width: 120,
        height: 100,
        // marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
    },
});
