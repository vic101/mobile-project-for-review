import {TouchableHighlight, Dimensions, StyleSheet, TextInput, FlatList} from "react-native";
import {Block, Text, Button as GaButton, theme, Radio} from "galio-framework";
import React, {useEffect, useState} from "react";
import {Button} from "../../index";

const { width, height } = Dimensions.get("screen");

export default function QuestionAndAnswer(
    {
        isCustom,
        answers,
        firstSurvey,
        position,
        total,
        questionText,
        currentQuestion,
        selectAnswer,
        radioSelected,
        next,
        setMessage,
        message
    }
    ) {

    const [selected, setSelected] = useState(undefined);

    function renderCounter() {
        return (
            <Text size={12} color={'#32325d'} bold style={styles.text}>Question {position} of {total}</Text>
        )
    }
    function renderQuestion() {
        return (
            <Block>
                <Text size={15} color={'#32325d'} bold>{questionText}</Text>
                {isCustom ?
                    <Text size={12} muted>This is a custom question created by your organization</Text> : null
                }
            </Block>
        )
    }
    function selectRadio (answer, index) {
        selectAnswer(answer, 'radio', index)
        selected !== index ? setSelected(index) : setSelected(undefined)
    }
    function nextQuestion (skip) {
        next(skip)
        setSelected(undefined)
    }
    const renderOneToTen = (answer, index) => (
        <TouchableHighlight
            key={index}
            style = {[styles.circle, selected === answer?.text ? {backgroundColor: '#32325d'}:null]}
            onPress = {() => setSelected(answer?.text)}
        >
            <Text size={10} bold color={ selected === answer?.text ? 'white' : '#32325d' }>{answer?.text}</Text>
        </TouchableHighlight>
    )
    function renderFirstSurveyChoices() {
        return (
            <Block>
                <Block row center space="between">
                    <Block flex middle center>
                        <FlatList
                            keyExtractor={item => item.id.toString()}
                            data={answers}
                            horizontal={true}
                            renderItem={({ item, index }) => renderOneToTen(item, index)}
                        />
                    </Block>
                </Block>
                <Block row center space="between">
                    <Block flex middle left>
                        <Text>Jumping with joy</Text>
                    </Block>
                    <Block flex middle right>
                        <Text>About to quit</Text>
                    </Block>
                </Block>
            </Block>
    )
    }

    function renderSurveyChoices() {
        return (
            <Block flex column style={{marginTop: 20}}>
                <Block style={{paddingLeft: "35%"}}>

                    {currentQuestion.map((answer, index) => {
                        return (
                            <Block style={{marginTop: 5}} key={index}>
                                {selected === index ?
                                    <Radio
                                        onChange={()=> selectRadio(answer, index)}
                                        label={answer.text}
                                        color="default"
                                        initialValue={true}
                                        radioInnerStyle={{ backgroundColor: "#32325d" }}
                                    />:null
                                }
                                {selected !== index ?
                                    <Radio
                                        onChange={()=> selectRadio(answer, index)}
                                        label={answer.text}
                                        color="default"
                                        initialValue={false}
                                        radioInnerStyle={{ backgroundColor: "#32325d" }}
                                    />:null
                                }
                            </Block>
                        )
                    })
                    }
                </Block>
            </Block>
        )
    }
    return (
        <Block style={styles.container}>
            <Block flex row center>
                {renderCounter()}
            </Block>
            <Block flex row center style={{marginTop: 20}}>
                {renderQuestion()}
            </Block>

            {(firstSurvey && position == 1) ? renderFirstSurveyChoices() : null}
            {(!firstSurvey && position ==1 || position > 1) ? renderSurveyChoices() : null}

            {(radioSelected || selected !== undefined) ?
                <Block flex row center style={{marginTop: 50}}>
                    <TextInput
                        onChangeText={setMessage}
                        placeholder="Why do you feel that way? Leave a message."
                        style={styles.comment}
                    />
                </Block>:null
            }
            {isCustom ?
                <Block flex row center style={{marginTop: 50}}>
                    <TextInput
                        onChangeText={setMessage}
                        multiline
                        placeholder="Write your reply ..."
                        style={styles.commentCustom}
                    />
                </Block>:null
            }
            {((radioSelected || selected !== undefined) || message !== '') ?
                <Block style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
                    <Button onPress={ ()=> nextQuestion(false) } style={styles.button}>
                        <Text bold color="white">{position === total ? 'Finish survey' : 'Next question'}</Text>
                    </Button>
                </Block>:null
            }

            <Block style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
                <Text onPress={ ()=> nextQuestion(true) } size={12} muted>Skip this question</Text>
            </Block>
        </Block>
    )
}

const styles = StyleSheet.create({
    circle: {
        marginTop: 5,
        marginRight: 5,
        marginBottom: 5,
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.06,
        height: Dimensions.get('window').width * 0.06,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.75,
        borderColor: '#32325d',
    },
    comment: {
        borderRadius: 4,
        shadowOpacity: 0.05,
        backgroundColor: '#edeff2',
        maxHeight: 100,
        minHeight: 45,
        paddingLeft: 8,
        paddingRight: 8,
        width: "100%"
    },
    commentCustom: {
        borderRadius: 4,
        shadowOpacity: 0.05,
        backgroundColor: '#edeff2',
        height: 100,
        minHeight: 45,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 10,
        width: "100%",
        textAlignVertical: 'top'
    },
    button: {
        backgroundColor: '#32325d',
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 11,
    },
    container: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
    },
});
