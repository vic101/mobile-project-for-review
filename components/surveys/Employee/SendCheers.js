import React, {useEffect, useState} from "react";
import {Block, Text, theme} from "galio-framework";
import {argonTheme} from "../../../constants";
import SelectDropdownMenu from "react-native-select-dropdown";
import {FontAwesome} from "@expo/vector-icons";
import {
    SafeAreaView,
    ScrollView,
    Dimensions,
    Platform,
    StyleSheet,
    TextInput,
    KeyboardAvoidingView,
    Alert
} from "react-native";
import {Button} from "../../index";
import RemoteDataSetExample from "../../RemoteDataSetExample";

const { width, height } = Dimensions.get("screen");
export default function SendCheers({setCheer, setCheerComment, setUserId, sendCheers}) {
    const cheers = [
        {
            key: 'Cheers',
            name: 'Cheers'
        },
        {
            key: 'High five',
            name: 'High five'
        },
        {
            key: 'Hug',
            name: 'Hug'
        },
        {
            key: 'Smile',
            name: 'Smile'
        },
        {
            key: 'Thumbs up',
            name: 'Thumbs up'
        },
        {
            key: 'First bump',
            name: 'First bump'
        },
    ]
    const [selectedItem, setSelectedItem] = useState(null);

    useEffect(()=> {
        setUserId(selectedItem?.id)
        setCheer(cheers[0].key)

        if (selectedItem?.sent) {
            Alert.alert('Error', 'You already sent cheers with this employee');
        }
    },[selectedItem])

    return (
        <SafeAreaView style={{ flex: 1 }}>
        <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            enabled
        >
        <ScrollView
            nestedScrollEnabled
            keyboardDismissMode="on-drag"
            keyboardShouldPersistTaps="handled"
            contentInsetAdjustmentBehavior="automatic"
        >
            <Block style={styles.container}>
            <Block>
                <Text size={15} style={[styles.text,{color: argonTheme.COLORS.BRAND_GREEN}]}>Thank you for completing the survey!{'\n'}</Text>
                <Text size={20} style={styles.text}>Now, you can send <Text style={{color: argonTheme.COLORS.BRAND_GREEN}}>Cheers</Text> to your coworkers – make their day!{'\n'}</Text>
                <Text size={15} style={styles.text}>What are you thankful for? Who did a great job? Who deserves a high five?</Text>
            </Block>

            <Block flex style={styles.group}>
                <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
                    <Block flex middle left style={{marginBottom: 10}}>
                        <Text bold>Send a:</Text>
                    </Block>

                    <Block row center space="evenly">
                        <Block flex middle left>
                            <Block style={styles.dropdownsRow}>
                                <SelectDropdownMenu
                                    data={cheers}
                                    onSelect={(selectedItem, index) => {
                                        setCheer(selectedItem.key)
                                    }}
                                    defaultButtonText={cheers[0].key}
                                    buttonTextAfterSelection={(selectedItem, index) => {
                                        return selectedItem.name;
                                    }}
                                    rowTextForSelection={(item, index) => {
                                        return item.name;
                                    }}
                                    buttonStyle={styles.dropdown1BtnStyle}
                                    buttonTextStyle={styles.dropdown1BtnTxtStyle}
                                    renderDropdownIcon={isOpened => {
                                        return <FontAwesome name={isOpened ? 'chevron-up' : 'chevron-down'} color={'#8898aa'} size={10} />;
                                    }}
                                    dropdownIconPosition={'right'}
                                    dropdownStyle={styles.dropdown1DropdownStyle}
                                    rowStyle={styles.dropdown1RowStyle}
                                    rowTextStyle={styles.dropdown1RowTxtStyle}
                                />
                            </Block>
                        </Block>
                        <Block flex middle center>
                            <Text>to</Text>
                        </Block>
                        <Block flex middle right>
                            <RemoteDataSetExample
                                setSelectedItem={setSelectedItem}
                            />
                        </Block>
                    </Block>
                </Block>
            </Block>

            <Block style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 30, marginTop: 10}}>
                <TextInput
                    onChangeText={setCheerComment}
                    multiline
                    placeholder="Leave a comment"
                    style={styles.input}
                />
            </Block>

            <Block style={{ justifyContent: 'center', alignItems: 'center'}}>
                <Button style={styles.button}>
                    <Text onPress={sendCheers} color="white">Send <Text style={{color: '#36d79a'}}>Cheers</Text> now</Text>
                </Button>
                <Text size={12} style={{color: '#36d79a'}}>You can send more Cheers after sending</Text>
            </Block>
        </Block>
        </ScrollView>
        </KeyboardAvoidingView>
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
    scrollContainer: {
        flex:1
    },
    section: {
        marginBottom: 40,
    },
    group: {
        paddingTop: theme.SIZES.BASE * 2
    },
    button: {
        backgroundColor: '#32325d',
        marginBottom: 2,
        width: width - theme.SIZES.BASE * 11,
    },
    input: {
        width:width - theme.SIZES.BASE * 8,
        maxHeight: 100,
        minHeight: 45,
        marginTop: 10,
        borderRadius: 4,
        backgroundColor: '#edeff2',
        shadowColor: argonTheme.COLORS.BLACK,
        shadowOffset: { width: 0, height: 1 },
        shadowRadius: 2,
        shadowOpacity: 0.05,
        elevation: 2,
        paddingLeft: 8,
        paddingRight: 8,
    },
    comment: {
        borderRadius: 4,
        shadowOpacity: 0.05,
        backgroundColor: '#edeff2',
        maxHeight: 100,
        minHeight: 45,
        paddingLeft: 8,
        paddingRight: 8,
        width: "130%"
    },
    dropdownsRow: {flexDirection: 'row', width: "130%"},
    dropdown1BtnStyle: {
        flex: 1,
        height: 45,
        backgroundColor: '#edeff2',
        borderRadius: 5,
        borderColor: '#444',
    },
    dropdown1BtnTxtStyle: {color: '#8898aa', textAlign: 'left', fontSize: 14},
    dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
    dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: '#C5C5C5'},
    dropdown1RowTxtStyle: {color: '#444', textAlign: 'left', fontSize: 14},
    spinnerTextStyle: {
        color: '#FFF'
    },
    surveyDetailsContainer: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
        shadowColor: "black",
        shadowOffset: { width: 0, height: 0 },
        shadowRadius: 8,
        shadowOpacity: 0.2,
        zIndex: 2,
        maxWidth: 500
    },
    container: {
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        marginBottom: 20,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
    },
    surveyDetails: {
        marginTop: 10
    },
    text: {
        textAlign: 'center',
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    }
});
