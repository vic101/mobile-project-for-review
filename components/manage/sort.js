export function sort (sortType, fetchType, fetch) {
    let type  = null;
    let order = null;
    switch (sortType) {
        case 'a-z':
            type = 'first_name'
            order = 'asc'
            break;
        case 'z-a':
            type = 'first_name'
            order = 'desc'
            break;
        case 'newest':
            type = 'created_at'
            order = 'desc'
            break;
        case 'oldest':
            type = 'created_at'
            order = 'asc'
            break

        default:
            type  = 'created_at'
            order = 'desc'
    }

    return fetch(type, order, fetchType)
}
