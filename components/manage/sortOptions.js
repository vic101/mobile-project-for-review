const filterOption = [
    {
        key: 'a-z',
        name: 'Alphabetical (0-9,A-Z)'
    },
    {
        key: 'z-a',
        name: 'Alphabetical (9-0,Z-A)'
    },
    {
        key: 'newest',
        name: 'Newest first'
    },
    {
        key: 'oldest',
        name: 'Oldest first'
    }
]

export default filterOption;
