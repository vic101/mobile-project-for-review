import axiosConfig from "../../helpers/axiosConfig";

export async function fetchEmployees(
    user,
    userType,
    userStatus,
    filterType,
    filterOrder,
    paginate,
    team,
    location
    ) {

    try {
        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
        let attributes = {
            userType: userType,
            userStatus: userStatus,
            type: filterType,
            order: filterOrder,
            paginate: paginate,
            team: team,
            location: location
        }

        const response = await axiosConfig.get(`/user-api?`, {params: attributes})
            .catch(error => {
                console.log('Error: ', error.response.data)
            });

        return response.data;
    } catch (e) {
        console.log(e)
        console.warn('Something went wrong.')
        alert('Error: Something went wrong')
    }
}
