import {Block, Text} from "galio-framework";
import React from "react";
import {StyleSheet} from "react-native";

export default function renderEmployeeList({data, index, empIndex, manIndex, setEmpIndex, setManIndex, renderType})
{
    return (
        <Block>
            {renderType == 'employee' ?
                <Text onPress={() => empIndex !== index ? setEmpIndex(index) : setEmpIndex(undefined)} style={
                    styles.underline
                }>Hide list</Text>
                :
                <Text onPress={() => manIndex !== index ? setManIndex(index) : setManIndex(undefined)} style={
                    styles.underline
                }>Hide list</Text>
            }
            <Block>
                {
                    data.map((employee, localIndex) => {
                        return (
                            <Block key={localIndex}>
                                <Text size={13}>{employee?.user?.first_name} <Text bold>{employee?.user?.last_name} </Text>
                                <Text muted>{employee?.user?.email}</Text></Text>
                            </Block>
                        )
                    })
                }
            </Block>
            {renderType == 'employee' ?
                <Text onPress={() => empIndex !== index ? setEmpIndex(index) : setEmpIndex(undefined)} style={
                    styles.underline
                }>Hide list</Text>
                :
                <Text onPress={() => manIndex !== index ? setManIndex(index) : setManIndex(undefined)} style={
                    styles.underline
                }>Hide list</Text>
            }
        </Block>
    )
}

const styles = StyleSheet.create({
    underline: {
        textDecorationLine: 'underline',
        fontSize: 11
    }
})
