import React from "react";
import {Block} from "galio-framework";
import {StyleSheet, Text} from "react-native";

export default function RenderUnassignedEmployeeList({data, visibleEmp, visibleMan, renderType, showHideEmployeeList}) {
    return (
        <Block>
            {renderType == 'employee' ?
                <Text onPress={() => showHideEmployeeList(visibleEmp, 'employee')} style={styles.underline}>Hide list</Text>
                :
                <Text onPress={() => showHideEmployeeList(visibleMan, 'manager')} style={styles.underline}>Hide list</Text>
            }
            <Block>
                {
                    data.map((employee, localIndex) => {
                        return (
                            <Block key={localIndex}>
                                <Text size={13}>{employee?.first_name} <Text bold>{employee?.last_name} </Text>
                                    <Text muted>{employee?.email}</Text></Text>
                            </Block>
                        )
                    })
                }
            </Block>
            {renderType == 'employee' ?
                <Text onPress={() => showHideEmployeeList(visibleEmp, 'employee')} style={styles.underline}>Hide
                    list</Text>
                :
                <Text onPress={() => showHideEmployeeList(visibleMan, 'manager')} style={styles.underline}>Hide
                    list</Text>
            }
        </Block>
    )
}
const styles = StyleSheet.create({
    underline: {
        textDecorationLine: 'underline',
        fontSize: 11
    },
})
