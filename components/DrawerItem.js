import React, {useContext, FunctionComponent} from "react";
import { StyleSheet, TouchableOpacity, Linking } from "react-native";
import { Block, Text, theme } from "galio-framework";

import Icon from "./Icon";
import argonTheme from "../constants/Theme";
import {AuthContext} from "../context/AuthProvider";

type UserChildren = (AuthContext: Array) => any;

interface IUserProps {
  children: UserChildren;
}

export const User: FunctionComponent<IUserProps> =({
 children,
}) => {
const logout: any = AuthContext();

return children(logout)
}

class DrawerItem extends React.Component {

  render() {
    const { focused, title, navigation } = this.props;
    const logout = AuthContext._currentValue.logout

    const containerStyles = [
      styles.defaultStyle,
      focused ? [styles.activeStyle, styles.shadow] : null
    ];

    return (
        <TouchableOpacity
            style={{ height: 60 }}
            onPress={() => title === 'Logout' ? logout() : navigation.navigate(title)}
        >
          <Block flex row style={containerStyles}>
            <Block row center flex={8}>
              <Text
                  size={15}
                  bold={focused ? true : false}
                  color={focused ? "white" : "rgba(0,0,0,0.5)"}
              >
                {title}
              </Text>
            </Block>
          </Block>
        </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  defaultStyle: {
    paddingVertical: 16,
    paddingHorizontal: 16
  },
  activeStyle: {
    backgroundColor: argonTheme.COLORS.THEME_COLOR,
    borderRadius: 4
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.1
  }
});

export default DrawerItem;
