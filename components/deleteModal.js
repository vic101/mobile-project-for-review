import React, {useRef} from "react";
import {Block, Text} from "galio-framework";
import {Alert, StyleSheet} from "react-native";
import {withNavigation} from "@react-navigation/compat";

class Delete extends React.Component {

    render() {
        const { navigation, id } = this.props;
        const modalizeRef = useRef(null);

        function showAlert(id) {
            Alert.alert('Delete this team?', null, [
                {
                    text: 'Cancel',
                    onPress: () => modalizeRef.current?.close(),
                    style: 'cancel',
                },
                {
                    text: 'OK',
                    // onPress: () => deleteTeam(id),
                    style: 'default',
                },
            ]);

            // setActiveIndex(undefined)
        }

        return (
            <Block>
                {/*<Text bold={title=='Manage Employees'} size={13} color="#32325D" onPress={()=> {*/}
                {/*    navigation.navigate('Manage Employees')*/}
                {/*}}>*/}
                {/*    Employees <Text muted size={12}>can reply to surveys</Text>*/}
                {/*</Text>*/}
            </Block>
        )
    }
}
const styles = StyleSheet.create({
    text: {
        lineHeight: 30
    },
    aboutText: {
        lineHeight: 20,
        marginBottom: 15
    },
    divider: {
        width: "100%",
        borderWidth: 0.4,
        borderColor: "#E9ECEF"
    }
});
export default withNavigation(Delete);