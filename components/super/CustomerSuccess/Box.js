import {Block, theme} from "galio-framework";
import {Dimensions, StyleSheet, Text} from "react-native";
import React, {useEffect, useState} from "react";
import {argonTheme} from "../../../constants";


const { width, height } = Dimensions.get("screen");
export default function Box({streaks,cheerCount}) {
    const green = argonTheme.COLORS.BRAND_GREEN;

    return (
        <Block row center space="between" style={{marginBottom: 20}}>
            {/*<Block flex middle left>*/}
            {/*    <Block style={styles.container}>*/}
            {/*        <Text style={{color: green, fontSize: 9, fontWeight: 'bold'}}>Current month commission forecast</Text>*/}
            {/*        <Text style={{color: green, fontSize: 9, fontWeight: 'bold'}}>{'12,234.56 €'}</Text>*/}
            {/*        <Text style={{color: green, fontSize: 9, fontWeight: 'bold'}}>surveys in a row</Text>*/}
            {/*    </Block>*/}
            {/*</Block>*/}
            <Block flex middle center>
                <Block style={[styles.container, {backgroundColor: '#4200c6'}]}>
                    <Text style={{color: 'white', fontSize: 12, fontWeight: 'bold'}}>Current month commission forecast</Text>
                    <Text style={{color: 'white', fontSize: 35, fontWeight: 'bold'}}>{'12,234.56 €'}</Text>
                    <Text style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>-123.45 € overdue payments</Text>
                    <Text style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>Lock-in & commission payment in 17 days</Text>
                </Block>
            </Block>
            {/*<Block flex middle right>*/}
            {/*    <Block style={styles.container}>*/}
            {/*        <Text style={{color: green, fontSize: 9, fontWeight: 'bold'}}>You have sent</Text>*/}
            {/*        <Text style={{color: green, fontSize: 35, fontWeight: 'bold'}}>{8}</Text>*/}
            {/*        <Text style={{color: green, fontSize: 8, fontWeight: 'bold'}}>Cheers to {9} coworkers</Text>*/}
            {/*    </Block>*/}
            {/*</Block>*/}
        </Block>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.SIZES.BASE,
        marginHorizontal: theme.SIZES.BASE,
        marginTop: 10,
        width: width * 0.95,
        height: height * 0.2,
        // marginBottom: 20,
        borderRadius: 6,
        backgroundColor: theme.COLORS.WHITE,
    },
});
