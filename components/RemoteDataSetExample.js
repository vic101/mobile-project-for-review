import React, {memo, useCallback, useContext, useEffect, useState} from 'react'
import { Text } from 'react-native'
import { AutocompleteDropdown } from 'react-native-autocomplete-dropdown'
import axiosConfig from "../helpers/axiosConfig";
import {AuthContext} from "../context/AuthProvider";

export default function RemoteDataSetExample({setSelectedItem}) {
    const { user } = useContext(AuthContext);
    const [loading, setLoading] = useState(false)
    const [remoteDataSet, setRemoteDataSet] = useState([])

    const getSuggestions = useCallback(async q => {
        if (typeof q !== 'string' || q.length < 3) {
            setRemoteDataSet(null)
            return
        }

        setLoading(true)

        let attribute = {
            requestFrom: 'mobile'
        }

        axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;

        const response = await axiosConfig.get('user-employee-api?', {params: attribute})
            .then((data) => new Promise(res => {
            setTimeout(() => res(data), 2000)
        })).catch((error)=> {console.log('error: ', error.response)})

        const items = await response.data
        const suggestions = items.map(item => ({
                id: item.id,
                title: item.text,
                sent: item.sent
            }))
        console.log('suggestions: ', suggestions)
        setRemoteDataSet(suggestions)
        setLoading(false)
    }, [])

    useEffect(()=> {

    },[])

    return (
        <>
            <AutocompleteDropdown
                keyExtractor={item => item.id.toString()}
                inputContainerStyle={{
                    backgroundColor: '#edeff2',
                    fontSize: 10
                }}
                dataSet={remoteDataSet}
                closeOnBlur={false}
                useFilter={false}
                clearOnFocus={false}
                textInputProps={{
                    placeholder: 'type to search...',
                }}
                onSelectItem={setSelectedItem}
                loading={loading}
                onChangeText={getSuggestions}
                suggestionsListTextStyle={{
                    color: '#8898aa',
                    fontSize: 14
                }}
                EmptyResultComponent={<Text style={{ padding: 10, fontSize: 15 }}>Oops ¯\_(ツ)_/¯</Text>}
            />
        </>
    )
}
