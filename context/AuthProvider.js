import React, { createContext, useState } from 'react';
import * as SecureStore from "expo-secure-store";
import axiosConfig from "../helpers/axiosConfig";
import {Alert} from "react-native";
export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const [user, setUser]           = useState(null);
    const [step, setStep]           = useState(null);
    const [banner, setBanner]       = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError]         = useState(null);

    return (
        <AuthContext.Provider
            value={{
                user,
                setUser,
                step,
                setStep,
                banner,
                setBanner,
                error,
                isLoading,
                login: (email, password) => {
                    setIsLoading(true);
                    axiosConfig.post('/login', {
                            email,
                            password,
                            device_name: 'mobile',
                        })
                        .then(response => {
                            const userResponse = {
                                token: response.data.token,
                                id: response.data.user.id,
                                name: response.data.user.name,
                                first_name: response.data.user.first_name,
                                username: response.data.user.username,
                                email: response.data.user.email,
                                trialDays: response.data.trialDays,
                                onTrial: response.data.onTrial,
                                pendingEmail: response.data.pending_email,
                                has_billing: response.data.user.has_billing,
                                company: response.data.company,
                                role: response.data.user.role_name,
                                verified: response.data.user.email_verified_at,
                            };

                            setUser(userResponse);
                            setError(null);
                            SecureStore.setItemAsync('user', JSON.stringify(userResponse));
                            setIsLoading(false);
                        })
                        .catch(error => {
                            console.log(error.response.data)

                            Alert.alert('Error', error.response.data)
                            if (error.response.data.errors) {
                                const key = Object.keys(error.response.data.errors)[0];
                                setError(error.response.data.errors[key][0]);
                            }

                            setIsLoading(false);
                        });
                },
                verify: () => {
                    axiosConfig.defaults.headers.common['Authorization'] = `Bearer ${user.token}`;
                    axiosConfig.post('/verify', {device_name: 'mobile'})
                        .then(response => {
                            const userResponse = {
                                token: response.data.token,
                                id: response.data.user.id,
                                name: response.data.user.name,
                                first_name: response.data.user.first_name,
                                username: response.data.user.username,
                                email: response.data.user.email,
                                trialDays: response.data.trialDays,
                                onTrial: response.data.onTrial,
                                pendingEmail: response.data.pending_email,
                                has_billing: response.data.user.has_billing,
                                company: response.data.company,
                                role: response.data.user.role_name,
                                verified: response.data.user.email_verified_at
                            };

                            setUser(userResponse);
                            setError(null);
                            SecureStore.setItemAsync('user', JSON.stringify(userResponse));
                            setIsLoading(false);
                        }).catch(function (error) {
                        console.log('Error: ', error.response.data)
                    })
                },
                hideBanner: () => {
                    let currentDate = new Date().getDate();

                    setBanner(false)
                    SecureStore.setItemAsync('banner', String(currentDate));
                },
                showBanner: () => {
                    setBanner(true)
                    SecureStore.deleteItemAsync('banner');
                },
                logout: () => {
                    setIsLoading(true);
                    axiosConfig.defaults.headers.common[
                        'Authorization'
                        ] = `Bearer ${user.token}`;
                    axiosConfig
                        .post('/logout')
                        .then(response => {
                            setUser(null);
                            SecureStore.deleteItemAsync('user');

                            setBanner(false)
                            SecureStore.deleteItemAsync('banner');

                            setError(null);
                            setIsLoading(false);
                        })
                        .catch(error => {
                            console.log(error);
                            setUser(null);
                            SecureStore.deleteItemAsync('user');

                            setBanner(false)
                            SecureStore.deleteItemAsync('banner');

                            setError(error.response.data.message);
                            setIsLoading(false);
                        });
                },
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};
