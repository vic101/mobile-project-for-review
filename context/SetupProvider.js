import React, { createContext, useState } from 'react';
import * as SecureStore from "expo-secure-store";
export const SetupContext = createContext();

export const AuthProvider = ({ children }) => {
    const [sequence, setSequence] = useState(null);

    return (
        <SetupContext.Provider
            value={{
                sequence,
                setSequence,
                next: (step) => {
                    SecureStore.setItemAsync('step', step);
                }
            }}
        >
            {children}
        </SetupContext.Provider>
    );
};
